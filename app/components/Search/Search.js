import React, { useState, useEffect } from "react";
import { StyleSheet, View, Keyboard, Animated, Text } from "react-native";
import { Searchbar } from "react-native-paper";
import colores from "../../styles/colores";
import { SearchStyles } from "../../styles/estilos";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";
import { Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native";
import ModalFilter from "../Modals/ModalFilter";

export default function Search(props) {
  const [modalDescription, setModalDescription] = useState(false);

  return (
    <View style={SearchStyles.container}>
      <View style={{ width: "0%", alignSelf: "center", alignItems: "center" }}>
        {/*   <Ionicons name="arrow-back" size={25} color={colores.color_blanco} /> */}
      </View>
      <View style={{ width: "86%", paddingLeft: 15 }}>
        <Searchbar
          placeholder="Buscar en Perchero"
          style={{ borderColor: colores.secundario, borderWidth: 1.2 }}
        ></Searchbar>
      </View>
      <TouchableOpacity
        onPress={() => setModalDescription(!modalDescription)}
        style={{ width: "14%", alignSelf: "center", alignItems: "center" }}
      >
        <MaterialCommunityIcons
          name="filter-menu"
          size={25}
          color={colores.color_blanco}
        />
      </TouchableOpacity>

      <ModalFilter
        modalDescription={modalDescription}
        setModalDescription={setModalDescription}
      />
    </View>
  );
}
