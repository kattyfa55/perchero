import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";
import colores from "../../styles/colores";
import { getSearchHistory } from "../../api/search";

export default function SearchHistory2(props) {
  const { showHistory, containerHeight, onSearch } = props;
  const [history, setHistory] = useState(null);

  useEffect(() => {
    if (showHistory) {
      (async () => {
        const response = await getSearchHistory();
        setHistory(response);
      })();
    }
  }, [showHistory]);

  return <View>{/*  <Text>HISTORIAL DE BUSQUEDA</Text> */}</View>;
}

const styles = StyleSheet.create({
  hidden: {
    display: "none",
  },
  history: {
    position: "absolute",
    backgroundColor: colores.primario,
    width: Dimensions.get("window").width,
    height: 100,
  },
  historyItem: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderWidth: 0.2,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  text: {
    color: "#53005f",
    fontSize: 16,
    fontWeight: "bold",
  },
});
