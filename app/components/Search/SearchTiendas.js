import React, { useState, useEffect } from "react";
import { StyleSheet, View, Keyboard, Animated, Text } from "react-native";
import { Searchbar } from "react-native-paper";
import { useNavigation, useRoute } from "@react-navigation/native";

import colores from "../../styles/colores";
import {
  AnimatedIcon,
  animatedTransition,
  animatedTransitionReset,
  arrowAnimation,
  inputAnimation,
  inputAnimationWidth,
} from "./SearchAnimation";
import SearchHistory2 from "./SearchHistory2";
import { updateSearchHistory } from "../../api/search";

export default function SearchTiendas(props) {
  const [searchQuery, setSearchQuery] = useState("");
  const navigation = useNavigation();

  const openSearch = () => {
    animatedTransition.start();
    /*  
    setShowHistory(!showHistory); */
  };

  const closeSearch = () => {
    animatedTransitionReset.start();
    Keyboard.dismiss();
    //setShowHistory(!showHistory);
  };

  const onChangeSearch = (query) => setSearchQuery(query);

  const onSearch = async () => {
    closeSearch();
    await updateSearchHistory(searchQuery);
    navigation.navigate("search-store", {
      search: searchQuery,
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.containerInput}>
        <AnimatedIcon
          name="arrow-left"
          size={20}
          style={[styles.backArrow, arrowAnimation]}
          onPress={closeSearch}
        />
        <Animated.View style={[inputAnimation, { width: inputAnimationWidth }]}>
          <Searchbar
            placeholder="Buscar tiendas"
            value={searchQuery}
            onFocus={openSearch}
            onChangeText={onChangeSearch}
            onSubmitEditing={onSearch}
            style={{ borderColor: colores.secundario, borderWidth: 1.2 }}
          ></Searchbar>
        </Animated.View>
        {/* <SearchHistory2 /> */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colores.secundario,
    paddingVertical: 5,
    paddingHorizontal: 20,
    zIndex: 1,
  },
  containerInput: {
    position: "relative",
    alignItems: "flex-end",
  },
  backArrow: {
    position: "absolute",
    left: 0,
    top: 15,
    color: colores.color_blanco,
  },
});
