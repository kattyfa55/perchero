import React, { useState } from "react";
import {   StyleSheet,   View,   Image,   Dimensions,   TouchableWithoutFeedback ,Text} from "react-native";
import { data_producto } from "../../utils/data";
import { size } from "lodash"; 

 import Carousel, { Pagination } from "react-native-snap-carousel";/*
import { API_URL } from "../../utils/constants";
 */
import { SERVIDOR } from "../../api/rutas";
const width = Dimensions.get("window").width;
const height = 300;

export default function CarouselImages(props) {
  const { images } = props;
  const [imageActive, setImageActive] = useState(0);
  const renderItem = ({ item }) => {
    return (
      <Image
        style={styles.carousel}
        source = {{ uri: SERVIDOR +item.path_fotos_productos }}
      />
    );
  };

  return (
    <>
       <Carousel
          layout={"default"}
          data={images}
          sliderWidth={width}
          itemWidth={width}
          renderItem={renderItem}
          onSnapToItem={(index) => setImageActive(index)}
      />
      <Pagination
          dotsLength={size(images)}
          activeDotIndex={imageActive}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
      /> 
    </>
  );
}

const styles = StyleSheet.create({
  carousel: {
    width,
    height,
    resizeMode: "contain",
  },
});
