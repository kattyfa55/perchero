import React from "react";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { informacionStyles } from "../styles/estilos";

export default function Informacion() {
  return (
    <View style={informacionStyles.viewInfo}>
      <TouchableOpacity onPress={() => console.log("Condiciones")}>
        <Text style={informacionStyles.infoText}>Condiciones de uso</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => console.log("Privacidad")}>
        <Text style={informacionStyles.infoText}>Privacidad</Text>
      </TouchableOpacity>
    </View>
  );
}
