import React, { useState } from "react";
import { View, Text, ToastAndroid } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";
import { formStyles } from "../../styles/estilos";
import colores from "../../styles/colores";
import { loginApi } from "../../api/user";
import { removeUserApi, setUserApi } from "../../api/storage";
import { EsloganPerchero, LogoPerchero } from "../Comunes/ComunComponents";
import { TouchableOpacity } from "react-native";

export default function LoginForm(props) {
  const {
    setShowLogin,
    setLogin,
    setShowRegisterComprador,
    setShowRegisterVendedor,
  } = props;
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);

  const registerVendedor = () => {
    setShowLogin(true);
    setShowRegisterVendedor(true);
  };

  const registerComprador = () => {
    setShowLogin(true);
    setShowRegisterComprador(true);
  };

  const formik = useFormik({
    initialValues: inicializarForm(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await loginApi(formData);
        if (response.result == false) {
          if (response.tipo == "password") {
            ToastAndroid.show("El usuario o password son incorrectos", 1000);
          }
          if (response.tipo == "correo") {
            ToastAndroid.show("El correo no esta registrado en perchero", 1000);
          }
        } else {
          if (response.tipo == "correo") {
            ToastAndroid.show("El correo aun no ha sido verificado", 1000);
          }
          if (response.tipo == "user") {
            await removeUserApi();
            setUserApi(response.data);
            setLogin(true);

            /*   
           
            await removeTokenApi();
           
            setToken(response.token); */
            ToastAndroid.show("Ingreso exitoso", 1000);
          }
        }
      } catch (error) {
        ToastAndroid.show("Ha ocurrido un error", 1000);
      }
      setLoading(false);
    },
  });

  return (
    <>
      <View>
        <LogoPerchero alto={150} />
        <View style={formStyles.view_titulo}>
          <TextInput
            label="Email"
            style={formStyles.input_form}
            left={<TextInput.Icon name="account" color={colores.terciario} />}
            mode="outlined"
            selectionColor={colores.color_sec}
            onChangeText={(text) => formik.setFieldValue("email", text)}
            value={formik.values.email}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />
          <TextInput
            label="Contraseña"
            style={formStyles.input_form}
            left={<TextInput.Icon name="lock" color={colores.terciario} />}
            right={
              showPassword ? (
                <TextInput.Icon
                  name="eye-off"
                  color={colores.terciario}
                  onPress={() => setShowPassword(!showPassword)}
                />
              ) : (
                <TextInput.Icon
                  name="eye"
                  color={colores.terciario}
                  onPress={() => setShowPassword(!showPassword)}
                />
              )
            }
            mode="outlined"
            selectionColor={colores.color_sec}
            onChangeText={(text) => formik.setFieldValue("password", text)}
            value={formik.values.password}
            secureTextEntry={showPassword ? false : true}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />

          {/*  <View style={{ flexDirection: "row" }}>
            <View
              style={{
                backgroundColor: "#fff",
                width: "30%",
              }}
            >
              <Text style={{ color: colores.terciario }}>Recordármelo </Text>
            </View>

            <View
              style={{
                alignItems: "flex-end",
                width: "70%",
                paddingRight: 10,
              }}
            >
              <Text style={{ color: colores.secundario }}>
                Olvidaste tu contraseña?
              </Text>
            </View>
          </View> */}
          <Button
            mode="contained"
            style={formStyles.btnSucces}
            labelStyle={formStyles.btnTextLabelLogin}
            onPress={formik.handleSubmit}
            loading={loading}
          >
            Iniciar Sesión
          </Button>
          <View style={formStyles.view_border}></View>
          <Text style={formStyles.txt_registrarse}>Registrarse</Text>
          <View style={formStyles.view_tipo_register}>
            <TouchableOpacity onPress={registerVendedor}>
              <Text style={formStyles.btnTextLabel}>Vendedor</Text>
            </TouchableOpacity>
            <Text style={formStyles.txt_separation_register}>|</Text>
            <TouchableOpacity onPress={registerComprador}>
              <Text style={formStyles.btnTextLabel}>Comprador</Text>
            </TouchableOpacity>
          </View>
        </View>
        <EsloganPerchero alto={80} />
      </View>
    </>
  );
}

function inicializarForm() {
  return {
    email: "",
    password: "",
  };
}

function validationSchema() {
  return {
    email: Yup.string().required(true),
    password: Yup.string().required(true),
  };
}
