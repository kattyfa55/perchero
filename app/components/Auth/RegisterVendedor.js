import React, { useEffect, useState } from "react";
import { View, Image, Picker, Text } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { CheckBox } from "react-native-elements";
import { Overlay } from "react-native-elements";
import VerDocumentos from "../../screens/Account/Seguridad/VerDocumentos";

import { useFormik } from "formik";
import * as ImagePicker from "expo-image-picker";
import { format } from "date-fns";
import { FontAwesome } from "@expo/vector-icons";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Fecha from "../../utils/Fecha";
import {
  ChangeUserStyles,
  formStyles,
  mi_perfil_styles,
  register_styles,
} from "../../styles/estilos";
import {
  getListaCiudades,
  getListaPais,
  getListaProvincias,
} from "../../api/lugares";
import { ScrollView } from "react-native-gesture-handler";
import { DOC_POLITICAS, SERVIDOR } from "../../api/rutas";
import Loading from "../../components/Comunes/Loading";
import Permisos from "../../utils/permisos/Permisos";
import { TouchableOpacity } from "react-native";
import colores from "../../styles/colores";
import { EsloganPerchero, LogoPerchero } from "../Comunes/ComunComponents";
import { RegisterVendedorApi } from "../../api/user";
import { ToastAndroid } from "react-native";

export default function RegisterVendedor(props) {
  const { setShowLogin, setShowRegisterVendedor, setLogin } = props;
  const [valueFecha, setValueFecha] = useState(0);
  const [FechNacimiento, setFechNacimiento] = useState(0);

  const [loading, setLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordRepeat, setShowPasswordRepeat] = useState(false);
  const [checked, setCheked] = useState(false);

  const [nacionalidad, setNacionalidad] = useState([]);
  const [nacionalidadSelected, setNacionalidadSelected] = useState(0);
  const [nacionalidadSelect, setNacionalidadSelect] = useState([]);

  const [pais, setPais] = useState([]);
  const [paisSelected, setPaisSelected] = useState(0);

  const [provincia, setProvincia] = useState([]);
  const [provinciaSelected, setProvinciaSelected] = useState(0);

  const [ciudad, setCiudad] = useState([]);
  const [ciudadSelected, setCiudadSelected] = useState(0);

  const [genero, setGenero] = useState([]);

  const [fecha, setFecha] = useState([]);

  const regresarLogin = () => {
    setShowRegisterVendedor(false);
    setShowLogin(false);
  };

  const formik = useFormik({
    initialValues: inicializarForm(),
    onSubmit: async (formData) => {
      formData.fecha_nacimiento_users = fecha;
      setLoading(true);
      try {
        let validaciones = validarDataVendedor(await formData, fecha);
        if (validaciones == true) {
          const response = await RegisterVendedorApi(formData);
          setLoading(true);
          if (response.existeEmail == "false") {
            ToastAndroid.show("Exitoso! Tus datos han sido registrado", 3000);
            setShowRegisterVendedor(false);
            setShowLogin(false);
          } else {
            ToastAndroid.show("El correo ya se encuentra registrado", 1000);
          }
        } else {
          ToastAndroid.show(validaciones, 3000);
        }
      } catch (error) {
        setLoading(false);
        ToastAndroid.show("Ha ocurrido un error", 1000);
      }
      setLoading(false);
    },
  });

  const setValueNacionalidad = (item) => {
    setNacionalidad(item);
    formik.setFieldValue("nacionalidad_users", item);
  };

  const setPaises = (item) => {
    setPaisSelected(item);
    formik.setFieldValue("pais_users", item);
  };

  const setProvincias = (item) => {
    setProvinciaSelected(item);
    formik.setFieldValue("provincia_users", item);
  };

  const setCiudades = (item) => {
    setCiudadSelected(item);
    formik.setFieldValue("ciudad_users", item);
  };

  const setValueGenero = (item) => {
    setGenero(item);
    formik.setFieldValue("sexo_users", item);
  };

  const checkTerminos = (aceptar, setAceptar) => {
    aceptar ? setAceptar(false) : setAceptar(true);
    setCheked(aceptar);
  };

  const setFechaNacimiento = (item) => {
    setFecha(item);
    formik.setFieldValue("fecha_nacimiento_users", item);
  };

  useEffect(() => {
    try {
      (async () => {
        setIsLoading(true);
        const responsePais = await getListaPais();
        setPais(responsePais);
        setIsLoading(false);
      })();
    } catch (error) {
      setIsLoading(false);
    }
  }, []);

  return (
    <ScrollView>
      <Loading isVisible={isLoading} text="Cargando" />
      <LogoPerchero alto={150} />
      <View style={formStyles.view_titulo}>
        <View style={{ marginVertical: 20 }}></View>
        <ComboSelectorNacionalidad
          setValueNacionalidad={setValueNacionalidad}
          nacionalidad={nacionalidad}
        />
        <TextInput
          label="C.I. / Pasaporte"
          style={formStyles.input_form}
          mode="outlined"
          selectionColor={colores.color_sec}
          onChangeText={(text) => formik.setFieldValue("dni_users", text)}
          keyboardType="numeric"
          value={formik.values.dni_users}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <TextInput
          label="Nombres"
          style={formStyles.input_form}
          mode="outlined"
          selectionColor={colores.color_sec}
          onChangeText={(text) => formik.setFieldValue("nombre_users", text)}
          value={formik.values.nombre_users}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <TextInput
          label="Apellidos"
          style={formStyles.input_form}
          mode="outlined"
          selectionColor={colores.color_sec}
          onChangeText={(text) => formik.setFieldValue("apellido_users", text)}
          value={formik.values.apellido_users}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />

        <GetCalendario
          setValueFecha={setValueFecha}
          setFechaNacimiento={setFechaNacimiento}
        />
        <TextInput
          label="Whatsapp"
          style={formStyles.input_form}
          keyboardType="numeric"
          mode="outlined"
          selectionColor={colores.color_sec}
          onChangeText={(text) => formik.setFieldValue("whatsapp_users", text)}
          value={formik.values.whatsapp_users}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <TextInput
          label="Correo electrónico"
          style={formStyles.input_form}
          mode="outlined"
          selectionColor={colores.color_sec}
          onChangeText={(text) => formik.setFieldValue("email", text)}
          value={formik.values.email}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <ComboSelectorGenero setValueGenero={setValueGenero} genero={genero} />

        {pais && (
          <ComboSelectorPaises
            defecto="Seleccione un país"
            data={pais}
            seleccionado={paisSelected}
            setPaises={setPaises}
            setProvincia={setProvincia}
            setCiudad={setCiudad}
          />
        )}

        {provincia && (
          <ComboSelectorProvincia
            provincia={provincia}
            provinciaSelected={provinciaSelected}
            setProvincias={setProvincias}
            setCiudad={setCiudad}
          />
        )}

        {ciudad && (
          <ComboSelectorCiudades
            data={ciudad}
            seleccionado={ciudadSelected}
            setData={setCiudades}
          />
        )}

        <TextInput
          label="Contraseña"
          style={formStyles.input_form}
          right={
            showPasswordRepeat ? (
              <TextInput.Icon
                name="eye-off"
                color={colores.terciario}
                onPress={() => setShowPasswordRepeat(!showPasswordRepeat)}
              />
            ) : (
              <TextInput.Icon
                name="eye"
                color={colores.terciario}
                onPress={() => setShowPasswordRepeat(!showPasswordRepeat)}
              />
            )
          }
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("password", text)}
          value={formik.values.password}
          secureTextEntry={showPasswordRepeat ? false : true}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />

        <TextInput
          label="Contraseña"
          style={formStyles.input_form}
          right={
            showPassword ? (
              <TextInput.Icon
                name="eye-off"
                color={colores.terciario}
                onPress={() => setShowPassword(!showPassword)}
              />
            ) : (
              <TextInput.Icon
                name="eye"
                color={colores.terciario}
                onPress={() => setShowPassword(!showPassword)}
              />
            )
          }
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("passwordRepeat", text)}
          value={formik.values.passwordRepeat}
          secureTextEntry={showPassword ? false : true}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />

        <TerminosCondiciones checkTerminos={checkTerminos} />
        <Button
          mode="contained"
          style={formStyles.btnSucces}
          labelStyle={formStyles.btnTextLabelLogin}
          onPress={formik.handleSubmit}
          loading={loading}
        >
          Registrarse
        </Button>
        <TouchableOpacity
          style={{ marginTop: 20, alignItems: "center" }}
          onPress={regresarLogin}
        >
          <Text style={{ color: colores.primario, fontWeight: "bold" }}>
            Iniciar Sesión
          </Text>
        </TouchableOpacity>
        <EsloganPerchero alto={80} />
        <View style={{ height: 50 }}></View>
      </View>
    </ScrollView>
  );
}

function ComboSelectorPaises(props) {
  const { defecto, data, seleccionado, setPaises, setProvincia, setCiudad } =
    props;

  var changePaises = async (itemValue, index) => {
    setPaises(itemValue, index);
    const responseProvincias = await getListaProvincias(itemValue);
    setProvincia(responseProvincias.data);
    if (responseProvincias.data.length == 0) {
      setCiudad([]);
    }
  };
  var items = () => {
    return data.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_paises}
          value={element.id_paises}
          key={key}
        />
      );
    });
  };

  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={seleccionado}
        onValueChange={(itemValue, index) => changePaises(itemValue, index)}
      >
        <Picker.Item label={defecto} value={defecto} />
        {data && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorProvincia(props) {
  const { provincia, provinciaSelected, setProvincias, setCiudad } = props;
  var changeProvincia = async (itemValue, index) => {
    setProvincias(itemValue, index);
    const responseCiudad = await getListaCiudades(itemValue);
    if (responseCiudad != null) {
      setCiudad(responseCiudad.data);
    }
  };

  var items = () => {
    return provincia.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_provincias}
          value={element.id_provincias}
          key={key}
        />
      );
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={provinciaSelected}
        onValueChange={(itemValue, index) => changeProvincia(itemValue, index)}
      >
        <Picker.Item label={"Seleccione una provincia"} value={0} />
        {provincia && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorCiudades(props) {
  const { data, seleccionado, setData } = props;
  var items = () => {
    return data.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_ciudades}
          value={element.id_ciudades}
          key={key}
        />
      );
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={seleccionado}
        onValueChange={(itemValue, index) => setData(itemValue, index)}
      >
        <Picker.Item label={"Seleccione una ciudad"} value={0} />
        {data && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorGenero(props) {
  const { genero, setValueGenero } = props;

  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={genero}
        onValueChange={(itemValue, index) => setValueGenero(itemValue, index)}
      >
        <Picker.Item label={"Seleccione un género"} value={0} />
        <Picker.Item label={"Masculino"} value={"Masculino"} />
        <Picker.Item label={"Femenino"} value={"Femenino"} />
        <Picker.Item label={"Prefiero no decir"} value={"Prefiero no decir"} />
      </Picker>
    </View>
  );
}

function ImagenPerfil(props) {
  const { user } = props;
  const { path_users } = user;
  const user_img_defecto = `${SERVIDOR}` + "img/user.png";
  const user_img = `${SERVIDOR}` + path_users;
  const [imagen, setImagen] = useState(user_img_defecto);

  useEffect(() => {
    {
      path_users != "Ninguno"
        ? setImagen(user_img)
        : setImagen(user_img_defecto);
    }
  }, []);

  const seleccionarImagen = async () => {
    Permisos.permisosMedia();
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [3, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      setImagen(result.uri);
    }
  };

  return (
    <TouchableOpacity onPress={() => seleccionarImagen()}>
      <View style={mi_perfil_styles.view_perfil}>
        {path_users != "Ninguno" ? (
          <Image style={mi_perfil_styles.view_image} source={{ uri: imagen }} />
        ) : (
          <Image style={mi_perfil_styles.view_image} source={{ uri: imagen }} />
        )}
      </View>
    </TouchableOpacity>
  );
}

function ComboSelectorNacionalidad(props) {
  const { nacionalidad, setValueNacionalidad } = props;
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={nacionalidad}
        onValueChange={(itemValue, index) =>
          setValueNacionalidad(itemValue, index)
        }
      >
        <Picker.Item label={"Seleccione nacionalidad"} value={"Ninguno"} />
        <Picker.Item label={"Ecuatoriano"} value={"Ecuatoriano"} />
        <Picker.Item label={"Extranjero"} value={"Extranjero"} />
      </Picker>
    </View>
  );
}

function inicializarForm() {
  return {
    nacionalidad_users: "",
    dni_users: "",
    nombre_users: "",
    apellido_users: "",
    fecha_nacimiento_users: "1980-01-01",
    whatsapp_users: "",
    email: "",
    sexo_users: "",
    pais_users: "",
    provincia_users: "",
    ciudad_users: "",
    ciudad_users: "",
    password: "",
  };
}

function GetCalendario(props) {
  const { setValueFecha, setFechaNacimiento } = props;
  var fechita = new Date(Fecha.getMaximaFecha());
  var fechaActual = format(fechita, "yyyy-MM-dd").toString();
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [fecha, setFecha] = useState(fechaActual);
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    var date = new Date(date.toString());
    var fecha_data = format(date, "yyyy-MM-dd").toString();
    setFechaNacimiento(fecha_data);
    setFecha(fecha_data);
    setValueFecha(fecha_data);
    hideDatePicker();
  };

  return (
    <View
      style={{
        borderWidth: 1,
        borderColor: colores.terciario,
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 15,
      }}
    >
      <TouchableOpacity
        onPress={showDatePicker}
        style={{ flexDirection: "row", height: 54, width: "100%" }}
      >
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <View style={{ width: "40%" }}>
            <Text
              style={{
                marginLeft: 15,
                fontSize: 16,
                textAlignVertical: "center",
              }}
            >
              {fecha}
            </Text>
          </View>

          <View
            style={{
              width: "60%",
              alignItems: "flex-end",
              paddingRight: 16,
            }}
          >
            <FontAwesome name="calendar" size={21} color={colores.secundario} />
          </View>
        </View>
      </TouchableOpacity>
      <DateTimePickerModal
        maximumDate={Fecha.getMaximaFecha()}
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
}

function TerminosCondiciones(props) {
  const { checkTerminos } = props;
  const [aceptar, setAceptar] = useState(false);
  const [visible, setVisible] = useState(false);
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const aceptarTerminos = (item) => {
    if (aceptar == true) {
      setAceptar(false);
    } else {
      setAceptar(true);
    }
  };

  /*  () => setAceptar(!aceptar) */
  return (
    <View style={register_styles.view_general_terminos}>
      <View style={register_styles.view_checked}>
        <CheckBox
          checked={aceptar}
          onPress={(item) => {
            aceptarTerminos(item);
          }}
          checkedColor={colores.secundario}
          containerStyle={{ margin: 0, padding: 0 }}
        />
      </View>
      <View style={register_styles.view_terminos}>
        <Text style={register_styles.txt_terminos_condiciones}>
          Acepto los{" "}
          {true && (
            <Text
              style={register_styles.txt_touch_terminos}
              onPress={toggleOverlay}
            >
              Términos & Condiciones
            </Text>
          )}{" "}
          y autorizo el uso de mis datos de acuerdo a la Declaración de
          Privacidad.
        </Text>
      </View>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <VerTerminos />
      </Overlay>
    </View>
  );
}

function VerTerminos() {
  return (
    <View
      style={{
        backgroundColor: colores.secundario,
        width: 400,
        height: 600,
      }}
    >
      <View style={{ marginVertical: 6, marginLeft: 5 }}>
        <Text style={{ color: colores.color_blanco, fontWeight: "bold" }}>
          Términos y condiciones CORCE S.A
        </Text>
      </View>
      <VerDocumentos ruta={DOC_POLITICAS} />
    </View>
  );
}

function validarDataVendedor(formData) {
  if (formData.nacionalidad_users == "Ninguno") {
    return "Seleccione una nacionalidad";
  }
  if (formData.dni_users == "") {
    return "Llene los datos de su cédula";
  }
  let validacionCedula = validarCedula(formData.dni_users);

  if (validacionCedula != true) {
    return validacionCedula;
  }

  if (formData.nombre_users == "") {
    return "Llene los datos de nombre";
  }
  if (formData.apellido_users == "") {
    return "Llene los datos de apellidos";
  }
  if (formData.whatsapp_users == "") {
    return "Llene los datos de su whatsapp";
  }
  var ok = 1;
  for (let i = 0; i < formData.whatsapp_users.length && ok == 1; i++) {
    var n = parseInt(formData.whatsapp_users.charAt(i));
    if (isNaN(n)) ok = 0;
  }
  if (ok == 0) {
    return "No puede ingresar caracteres en el whatsapp";
  }

  if (formData.email == "") {
    return "Llene los datos de email";
  }
  if (
    formData.sexo_users == "Seleccione un género" ||
    formData.sexo_users == 0 ||
    formData.sexo_users == ""
  ) {
    return "Seleccione su género";
  }
  if (
    formData.pais_users === "Seleccione un país" ||
    formData.pais_users === ""
  ) {
    return "Seleccione un país";
  }
  if (
    formData.provincia_users === "Seleccione una provincia" ||
    formData.provincia_users === ""
  ) {
    return "Seleccione una provincia";
  }
  if (
    formData.ciudad_users === "Seleccione una ciudad" ||
    formData.ciudad_users === ""
  ) {
    return "Seleccione una ciudad";
  }
  if (formData.password == "") {
    return "Llene los datos de contraseñas";
  }
  if (formData.passwordRepeat == "") {
    return "Llene los datos de contraseñas";
  }
  if (formData.password != formData.passwordRepeat) {
    return "Las contraseñas no coinciden";
  }
  if (formData.password.length < 8) {
    return "La contraseña debe al menos tener 8 caracteres";
  }

  return true;
}

function validarCedula(cedula) {
  var ok = 1;
  for (let i = 0; i < cedula.length && ok == 1; i++) {
    var n = parseInt(cedula.charAt(i));
    if (isNaN(n)) ok = 0;
  }
  if (ok == 0) {
    return "No puede ingresar caracteres en la cédula";
  }

  //Preguntamos si la cedula consta de 10 digitos
  if (cedula.length == 10) {
    //Obtenemos el digito de la region que sonlos dos primeros digitos
    var digito_region = cedula.substring(0, 2);
    //Pregunto si la region existe ecuador se divide en 24 regiones
    if (digito_region >= 1 && digito_region <= 24) {
      // Extraigo el ultimo digito
      var ultimo_digito = cedula.substring(9, 10);
      //Agrupo todos los pares y los sumo
      var pares =
        parseInt(cedula.substring(1, 2)) +
        parseInt(cedula.substring(3, 4)) +
        parseInt(cedula.substring(5, 6)) +
        parseInt(cedula.substring(7, 8));

      //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
      var numero1 = cedula.substring(0, 1);
      var numero1 = numero1 * 2;
      if (numero1 > 9) {
        var numero1 = numero1 - 9;
      }

      var numero3 = cedula.substring(2, 3);
      var numero3 = numero3 * 2;
      if (numero3 > 9) {
        var numero3 = numero3 - 9;
      }

      var numero5 = cedula.substring(4, 5);
      var numero5 = numero5 * 2;
      if (numero5 > 9) {
        var numero5 = numero5 - 9;
      }

      var numero7 = cedula.substring(6, 7);
      var numero7 = numero7 * 2;
      if (numero7 > 9) {
        var numero7 = numero7 - 9;
      }

      var numero9 = cedula.substring(8, 9);
      var numero9 = numero9 * 2;
      if (numero9 > 9) {
        var numero9 = numero9 - 9;
      }

      var impares = numero1 + numero3 + numero5 + numero7 + numero9;

      //Suma total
      var suma_total = pares + impares;

      //extraemos el primero digito
      var primer_digito_suma = String(suma_total).substring(0, 1);

      //Obtenemos la decena inmediata
      var decena = (parseInt(primer_digito_suma) + 1) * 10;

      //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
      var digito_validador = decena - suma_total;

      //Si el digito validador es = a 10 toma el valor de 0
      if (digito_validador == 10) var digito_validador = 0;

      //Validamos que el digito validador sea igual al de la cedula
      if (digito_validador == ultimo_digito) {
        return true;
      } else {
        return "la cédula es incorrecta";
      }
    } else {
      // imprimimos en consola si la region no pertenece
      return "Esta cédula no pertenece a ninguna region";
    }
  } else {
    //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
    return "Esta cédula tiene menos de 10 Digitos";
  }
}
