import React, { useEffect, useState } from "react";
import { View, Text, ToastAndroid, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { TextInput, Button } from "react-native-paper";
import { CheckBox } from "react-native-elements";
import { Overlay } from "react-native-elements";
import { useFormik } from "formik";
import * as Yup from "yup";

import { EsloganPerchero, LogoPerchero } from "../Comunes/ComunComponents";
import { formStyles, register_styles } from "../../styles/estilos";

import { DOC_POLITICAS } from "../../api/rutas";
import VerDocumentos from "../../screens/Account/Seguridad/VerDocumentos";
import colores from "../../styles/colores";
import { RegisterCompradorApi } from "../../api/user";

export default function RegisterComprador(props) {
  const { setShowLogin, setLogin, setShowRegisterComprador } = props;
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordRepeat, setShowPasswordRepeat] = useState(false);
  const [checked, setCheked] = useState(false);

  useEffect(() => {}, []);
  const regresarLogin = () => {
    setShowRegisterComprador(false);
    setShowLogin(false);
  };

  const checkTerminos = (aceptar, setAceptar) => {
    aceptar ? setAceptar(false) : setAceptar(true);
    setCheked(aceptar);
  };

  const formik = useFormik({
    initialValues: inicializarForm(),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        let validaciones = validarData(await formData);
        if (validaciones == true) {
          ToastAndroid.show("Sus datos estan siendo verificados", 3000);
          const response = await RegisterCompradorApi(formData);
          setLoading(true);
          if (response.existeEmail == "false") {
            ToastAndroid.show("Exitoso! Tus datos han sido registrado", 3000);
            setShowRegisterComprador(false);
            setShowLogin(false);
          } else {
            ToastAndroid.show("El correo ya se encuentra registrado", 1000);
          }
        } else {
          ToastAndroid.show(validaciones, 3000);
        }
      } catch (error) {
        setLoading(false);
        ToastAndroid.show("Ha ocurrido un error", 1000);
      }
      setLoading(false);
    },
  });

  return (
    <>
      <ScrollView showsVerticalScrollIndicator={false}>
        <LogoPerchero alto={150} />
        <View style={formStyles.view_titulo}>
          <TextInput
            label="Nombres"
            style={formStyles.input_form}
            mode="outlined"
            selectionColor={colores.color_sec}
            onChangeText={(text) => formik.setFieldValue("nombres", text)}
            value={formik.values.nombres}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />
          <TextInput
            label="Apellidos"
            style={formStyles.input_form}
            mode="outlined"
            selectionColor={colores.color_sec}
            onChangeText={(text) => formik.setFieldValue("apellidos", text)}
            value={formik.values.apellidos}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />
          <TextInput
            label="Correo Electrónico"
            style={formStyles.input_form}
            mode="outlined"
            selectionColor={colores.color_sec}
            onChangeText={(text) => formik.setFieldValue("email", text)}
            value={formik.values.email}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />
          <TextInput
            label="Contraseña"
            style={formStyles.input_form}
            right={
              showPasswordRepeat ? (
                <TextInput.Icon
                  name="eye-off"
                  color={colores.terciario}
                  onPress={() => setShowPasswordRepeat(!showPasswordRepeat)}
                />
              ) : (
                <TextInput.Icon
                  name="eye"
                  color={colores.terciario}
                  onPress={() => setShowPasswordRepeat(!showPasswordRepeat)}
                />
              )
            }
            mode="outlined"
            onChangeText={(text) => formik.setFieldValue("password", text)}
            value={formik.values.password}
            secureTextEntry={showPasswordRepeat ? false : true}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />

          <TextInput
            label="Contraseña"
            style={formStyles.input_form}
            right={
              showPassword ? (
                <TextInput.Icon
                  name="eye-off"
                  color={colores.terciario}
                  onPress={() => setShowPassword(!showPassword)}
                />
              ) : (
                <TextInput.Icon
                  name="eye"
                  color={colores.terciario}
                  onPress={() => setShowPassword(!showPassword)}
                />
              )
            }
            mode="outlined"
            onChangeText={(text) =>
              formik.setFieldValue("passwordRepeat", text)
            }
            value={formik.values.passwordRepeat}
            secureTextEntry={showPassword ? false : true}
            theme={{
              colors: {
                primary: colores.secundario,
              },
            }}
          />
          <TerminosCondiciones checkTerminos={checkTerminos} />
          <Button
            mode="contained"
            style={formStyles.btnSucces}
            labelStyle={formStyles.btnTextLabelLogin}
            onPress={formik.handleSubmit}
            loading={loading}
          >
            Registrarse
          </Button>
          <TouchableOpacity
            style={{ marginTop: 20, alignItems: "center" }}
            onPress={regresarLogin}
          >
            <Text style={{ color: colores.primario, fontWeight: "bold" }}>
              Iniciar Sesión
            </Text>
          </TouchableOpacity>
        </View>
        <EsloganPerchero alto={80} />
        <View style={{ height: 30 }}></View>
      </ScrollView>
    </>
  );
}

function inicializarForm() {
  return {
    nombres: "",
    apellidos: "",
    email: "",
    password: "",
    passwordRepeat: "",
  };
}

function validationSchema() {
  return {
    nombres: Yup.string().required(true),
    apellidos: Yup.string().required(true),
    email: Yup.string().required(true),
    password: Yup.string().required(true),
    passwordRepeat: Yup.string()
      .required(true)
      .oneOf([Yup.ref("password")], true),
  };
}

function TerminosCondiciones(props) {
  const { checkTerminos } = props;
  const [aceptar, setAceptar] = useState(false);

  const [visible, setVisible] = useState(false);
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const aceptarTerminos = () => {
    if (aceptar == true) {
      setAceptar(false);
    } else {
      setAceptar(true);
    }
  };
  /*  () => setAceptar(!aceptar) */
  return (
    <View style={register_styles.view_general_terminos}>
      <View style={register_styles.view_checked}>
        <CheckBox
          checked={aceptar}
          onPress={aceptarTerminos}
          checkedColor={colores.secundario}
          containerStyle={{ margin: 0, padding: 0 }}
        />
      </View>
      <View style={register_styles.view_terminos}>
        <Text style={register_styles.txt_terminos_condiciones}>
          Acepto los{" "}
          {true && (
            <Text
              style={register_styles.txt_touch_terminos}
              onPress={toggleOverlay}
            >
              Términos & Condiciones
            </Text>
          )}{" "}
          y autorizo el uso de mis datos de acuerdo a la Declaración de
          Privacidad.
        </Text>
      </View>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <VerTerminos />
      </Overlay>
    </View>
  );
}

function VerTerminos() {
  return (
    <View
      style={{
        backgroundColor: colores.secundario,
        width: 400,
        height: 600,
      }}
    >
      <View style={{ marginVertical: 6, marginLeft: 5 }}>
        <Text style={{ color: colores.color_blanco, fontWeight: "bold" }}>
          Términos y condiciones CORCE S.A
        </Text>
      </View>
      <VerDocumentos ruta={DOC_POLITICAS} />
    </View>
  );
}

function validarData(formData) {
  if (formData.nombres == "") {
    return "Llene los datos de nombre";
  }
  if (formData.apellidos == "") {
    return "Llene los datos de apellidos";
  }
  if (formData.email == "") {
    return "Llene los datos de email";
  }
  if (formData.password == "") {
    return "Llene los datos de contraseñas";
  }
  if (formData.passwordRepeat == "") {
    return "Llene los datos de contraseñas";
  }
  if (formData.password != formData.passwordRepeat) {
    return "Las contraseñas no coinciden";
  }
  if (formData.password.length < 8) {
    return "La contraseña debe al menos tener 8 caracteres";
  }
  return true;
}
