import React, { useState } from "react";
import { View, Text, Alert } from "react-native";

import { TextInput, Button } from "react-native-paper";
import Toast from "react-native-root-toast";
import { useFormik } from "formik";
import * as Yup from "yup";

import colores from "../../styles/colores";
import { formStyles, register_styles } from "../../styles/estilos";
import { registerUser } from "../../api/user";
import { LogoPerchero } from "../Comunes/ComunComponents";
import { useNavigation } from "@react-navigation/native";
import { ScrollView } from "react-native-gesture-handler";
import { CheckBox } from "react-native-elements";
import { Overlay } from "react-native-elements";
import VerDocumentos from "../../screens/Account/Seguridad/VerDocumentos";
import { DOC_POLITICAS } from "../../api/rutas";

///import { registerApi } from "../../api/user";
//import { formStyle } from "../../styles";

export default function RegisterForm(props) {
  const navigation = useNavigation();
  const { setShowLogin, setLogin } = props;
  const [loading, setLoading] = useState(false);
  const showLogin = () => setShowLogin((prevState) => !prevState);
  const irEditarPerfil = (user) => {
    navigation.navigate("user-edit", { user });
  };
  const irCuentaPerfil = (user) => {
    setLogin(true);
  };
  const formik = useFormik({
    initialValues: inicializarForm(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      /*  setLoading(true); */
      /*  try {
        const datita = await registerUser(formData);
        Alert.alert(
          "Registro",
          "¿Desea completar el rgistro?",
          [
            { text: "NO", onPress: () => irCuentaPerfil(datita) },
            { text: "SI", onPress: () => irEditarPerfil(datita) },
          ],
          { cancelable: false }
        );
      } catch (error) {
        Toast.show("Error al registrar el usuario", {
          position: Toast.positions.CENTER,
        });
        setLoading(false);
      } */
    },
  });

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        backgroundColor: colores.color_blanco,
      }}
    >
      <LogoPerchero alto={140} />
      <View style={formStyles.view_titulo}>
        <TextInput
          label="Nombres"
          style={formStyles.input_form}
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("nombre", text)}
          value={formik.values.nombre}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
          /* error={formik.errors.email} */
        />
        <TextInput
          label="Apellidos"
          style={formStyles.input_form}
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("apellido", text)}
          value={formik.values.apellido}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
          /* error={formik.errors.email} */
        />
        <TextInput
          label="Email"
          style={formStyles.input_form}
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("email", text)}
          value={formik.values.email}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
          /* error={formik.errors.email} */
        />
        <TextInput
          label="Contraseña"
          style={formStyles.input_form}
          right={<TextInput.Icon name="eye" />}
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("password", text)}
          value={formik.values.password}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
          /*  error={formik.errors.password} */
          secureTextEntry
        />
        <TextInput
          label="Repetir contraseña"
          style={formStyles.input_form}
          right={<TextInput.Icon name="eye" />}
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("repeatPassword", text)}
          value={formik.values.repeatPassword}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
          secureTextEntry
        />
        <TerminosCondiciones />
        <Button
          mode="contained"
          style={formStyles.btnSucces}
          labelStyle={formStyles.btnTextLabelLogin}
          onPress={formik.handleSubmit}
          loading={loading}
        >
          Registrarse
        </Button>
        <Button
          mode="text"
          style={formStyles.btnText}
          labelStyle={formStyles.btnTextLabel}
          onPress={showLogin}
        >
          Iniciar Sesión
        </Button>
      </View>
    </ScrollView>
  );
}

function TerminosCondiciones() {
  const [aceptar, setAceptar] = useState(false);
  const [visible, setVisible] = useState(false);
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  return (
    <View style={register_styles.view_general_terminos}>
      <View style={register_styles.view_checked}>
        <CheckBox
          checked={aceptar}
          onPress={() => setAceptar(!aceptar)}
          checkedColor={colores.secundario}
          containerStyle={{ margin: 0, padding: 0 }}
        />
      </View>
      <View style={register_styles.view_terminos}>
        <Text style={register_styles.txt_terminos_condiciones}>
          Acepto los{" "}
          {true && (
            <Text
              style={register_styles.txt_touch_terminos}
              onPress={toggleOverlay}
            >
              Términos & Condiciones
            </Text>
          )}{" "}
          y autorizo el uso de mis datos de acuerdo a la Declaración de
          Privacidad.
        </Text>
      </View>
      <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <VerTerminos />
      </Overlay>
    </View>
  );
}

function VerTerminos() {
  return (
    <View
      style={{
        backgroundColor: colores.secundario,
        width: 400,
        height: 600,
      }}
    >
      <View style={{ marginVertical: 6, marginLeft: 5 }}>
        <Text style={{ color: colores.color_blanco, fontWeight: "bold" }}>
          Términos y condiciones CORCE S.A
        </Text>
      </View>
      <VerDocumentos ruta={DOC_POLITICAS} />
    </View>
  );
}

function inicializarForm() {
  return {
    nombre: "",
    apellido: "",
    email: "",
    password: "",
    repeatPassword: "",
  };
}

function validationSchema() {
  return {
    nombre: Yup.string().required(true),
    apellido: Yup.string().required(true),
    email: Yup.string().email().required(true),
    password: Yup.string().required(true),
    repeatPassword: Yup.string()
      .required(true)
      .oneOf([Yup.ref("password")], true),
  };
}
