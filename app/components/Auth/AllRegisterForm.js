import React, { useState, useCallback } from "react";
import { View, Picker, Text, Image } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { Formik, useFormik } from "formik";
import * as Yup from "yup";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FontAwesome } from "@expo/vector-icons";

import { ChangeUserStyles, formStyles } from "../../styles/estilos";
import colores from "../../styles/colores";
import Toast from "react-native-root-toast";
import { updateUser } from "../../api/user";
import { ScrollView } from "react-native";
import Fecha from "../../utils/Fecha";
import { format } from "date-fns";
import { URL_SERVIDOR } from "../../api/rutas";
import { getUserApi } from "../../api/storage";
import { getListaProvincias, getListaCiudades } from "../../api/lugares";
import Loading from "../../components/Comunes/Loading";
import { isLoading } from "expo-font";

export default function ChangeName(props) {
  const { route } = props;
  const { user } = route.params;
  const url = `${URL_SERVIDOR}`;
  const [fotoUser, setFotoUser] = useState(null);
  const [provincias, setProvincias] = useState(null);
  const [ciudades, setCiudades] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const {
    path_users,
    dni_users,
    nacionalidad_users,
    pais_users,
    provincia_users,
    ciudad_users,
    sexo_users,
    fecha_nacimiento_users,
    whatsapp_users,
    email,
    apellido_users,
    nombre_users,
  } = user;

  const paises = [
    { id: 1, valor: "Ecuador" },
    { id: 2, valor: "España" },
    { id: 3, valor: "Cuba" },
    { id: 4, valor: "Canada" },
  ];

  const sexo = [
    { id: 1, valor: "Hombre" },
    { id: 2, valor: "Mujer" },
  ];
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      (async () => {
        getSelectores();
        let url_foto = url + path_users;
        setFotoUser(url_foto);
      })();
    }, [])
  );

  const getSelectores = async () => {
    await getListaProvincias().then((result) => {
      if (result != null) {
        setProvincias(result.data);
      }
    });
    await getListaCiudades().then((result) => {
      if (result != null) {
        setCiudades(result.data);
      }
    });
  };

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        await updateUser(auth, formData);
        navigation.goBack();
      } catch (error) {
        Toast.show("Error al actualizar los datos.", {
          position: Toast.positions.CENTER,
        });
      }
      setLoading(false);
    },
  });

  return (
    <ScrollView>
      <Loading isVisible={isLoading} text="Cargando" />
      <Formik
        validateOnMount={true}
        initialValues={{
          nombres: nombre_users,
          apellidos: apellido_users,
          cedula: dni_users,
          whatsapp: whatsapp_users,
          email: email,
          pais: 1,
          provincias: 1,
          ciudades: 1,
          sexo: 1,
          url: url + path_users,
        }}
        onSubmit={(values) => console.log(values)}
      >
        {({ handleChange, values, handleSubmit, handleBlur }) => (
          <View style={formStyles.container}>
            {/*     <View
              style={{
                width: 200,
                height: 200,
                marginTop: 10,
                alignSelf: "center",
                borderRadius: 200,
              }}
            >
              <Image
                style={{
                  width: 200,
                  height: 200,
                  resizeMode: "contain",
                  borderRadius: 400,
                  paddingTop: 10,
                }}
                source={{ uri: values.url }}
              />
            </View> */}
            <TextInput
              label="Nombres"
              style={formStyles.input}
              mode="outlined"
              onChangeText={handleChange("nombres")}
              onBlur={handleBlur("nombres")}
              value={values.nombres}
            />
            <TextInput
              label="Apellidos"
              style={formStyles.input}
              mode="outlined"
              onChangeText={handleChange("apellidos")}
              onBlur={handleBlur("apellidos")}
              value={values.apellidos}
            />
            <TextInput
              label="C.I / Pasaporte"
              style={formStyles.input}
              mode="outlined"
              onChangeText={handleChange("cedula")}
              onBlur={handleBlur("cedula")}
              value={values.cedula}
            />
            {paises ? (
              <ComboSelectorPaises
                defecto="Selecione país"
                data={paises}
                seleccionado={values.pais}
              />
            ) : (
              <ComboSelectorPaises
                defecto="Selecione país"
                data={null}
                seleccionado={0}
              />
            )}
            {provincias ? (
              <ComboSelector
                defecto="Selecione provincia"
                data={provincias}
                seleccionado={values.provincias}
              />
            ) : (
              <ComboSelector
                defecto="Selecione provincia"
                data={null}
                seleccionado={0}
              />
            )}
            {ciudades ? (
              <ComboSelectorCiudades
                defecto="Selecione ciudad"
                data={ciudades}
                seleccionado={1}
              />
            ) : (
              <ComboSelectorCiudades
                defecto="Selecione ciudad"
                data={null}
                seleccionado={0}
              />
            )}
            {sexo ? (
              <ComboSelectorSexo
                defecto="Selecione sexo"
                data={sexo}
                seleccionado={values.sexo}
              />
            ) : (
              <ComboSelectorSexo
                defecto="Selecione sexo"
                data={null}
                seleccionado={0}
              />
            )}
            <GetCalendario />
            <TextInput
              label="Whatsapp"
              style={formStyles.input_form}
              mode="outlined"
              onChangeText={handleChange("whatsapp")}
              onBlur={handleBlur("whatsapp")}
              value={values.whatsapp}
            />
            <TextInput
              label="email"
              style={formStyles.input_form}
              mode="outlined"
              selectionColor={colores.color_sec}
              onChangeText={handleChange("emaiñ")}
              onBlur={handleBlur("email")}
              value={values.email}
            />
            <Button
              mode="contained"
              style={formStyles.btnSucces}
              labelStyle={formStyles.btnTextLabelLogin}
              onPress={handleSubmit}
              loading={loading}
            >
              Guardar Cambios
            </Button>
          </View>
        )}
      </Formik>
    </ScrollView>
  );
}

function ComboSelector(props) {
  const { defecto, data, seleccionado, handleChange } = props;
  const [state, setState] = useState(seleccionado);
  var items = () => {
    return data.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_provincias}
          value={element.id_provincias}
          key={key}
        />
      );
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={state}
        onValueChange={(itemValue, itemIndex) => setState(2)}
      >
        <Picker.Item label={defecto} value={defecto} />
        {data && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorCiudades(props) {
  const { defecto, data, seleccionado } = props;
  var items = () => {
    return data.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_ciudades}
          value={element.id_ciudades}
          key={key}
        />
      );
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={seleccionado}
      >
        <Picker.Item label={defecto} value={defecto} />
        {data && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorPaises(props) {
  const { defecto, data, seleccionado } = props;
  var items = () => {
    return data.map((element, key) => {
      return <Picker.Item label={element.valor} value={element.id} key={key} />;
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={seleccionado}
      >
        <Picker.Item label={defecto} value={defecto} />
        {data && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorSexo(props) {
  const { defecto, data, seleccionado } = props;
  var items = () => {
    return data.map((element, key) => {
      return <Picker.Item label={element.valor} value={element.id} key={key} />;
    });
  };
  return (
    <View style={ChangeUserStyles.View_cmb}>
      <Picker
        style={ChangeUserStyles.PickerUserStyles}
        selectedValue={seleccionado}
      >
        <Picker.Item label={defecto} value={defecto} />
        {data && items()}
      </Picker>
    </View>
  );
}
function GetCalendario() {
  var fechaActual = format(new Date(), "yyyy-MM-dd").toString();
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [fecha, setFecha] = useState(fechaActual);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    var date = new Date(date.toString());
    var fecha_data = format(date, "yyyy-MM-dd").toString();
    setFecha(fecha_data);
    hideDatePicker();
  };
  return (
    <View
      style={{
        borderWidth: 1,
        borderColor: colores.terciario,
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 15,
      }}
    >
      <TouchableOpacity
        onPress={showDatePicker}
        style={{ flexDirection: "row", height: 54, width: "100%" }}
      >
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <View style={{ width: "40%" }}>
            <Text
              style={{
                marginLeft: 15,
                fontSize: 16,
                textAlignVertical: "center",
              }}
            >
              {fecha}
            </Text>
          </View>

          <View
            style={{
              width: "60%",
              alignItems: "flex-end",
              paddingRight: 16,
            }}
          >
            <FontAwesome name="calendar" size={21} color={colores.secundario} />
          </View>
        </View>
      </TouchableOpacity>
      <DateTimePickerModal
        maximumDate={Fecha.getMaximaFecha()}
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
}
function initialValues() {
  return {
    nombre: "",
    apellido: "",
    cedula: "",
    whatsapp: "",
    email: "",
  };
}

function validationSchema() {
  return {
    nombre: Yup.string().required(true),
    apellido: Yup.string().required(true),
    cedula: Yup.string().required(true),

    whatsapp: Yup.string().required(true),
    email: Yup.string().required(true),
  };
}
