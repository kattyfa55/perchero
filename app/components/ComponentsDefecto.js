import React from "react";
import { Text, View } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import colores from "../styles/colores";
import { StyleSheet } from "react-native";

export function ProductoDefecto() {
  return (
    <View style={styles.view_defecto}>
      <FontAwesome5
        name="shopping-bag"
        size={75}
        color={colores.primario}
        style={{ marginTop: 5 }}
      />
      <Text style={styles.txt_titulo}>Aún no hay productos</Text>
      <Text style={styles.txt_context}>
        Productos no disponibles hasta publicacion de las tiendas
      </Text>
    </View>
  );
}

export function FavoriteDefecto() {
  return (
    <View style={styles.view_defecto}>
      <FontAwesome5
        name="hand-holding-heart"
        size={75}
        color={colores.primario}
        style={{ marginTop: 5 }}
      />
      <Text style={styles.txt_tit_fav}>
        Aún no has guardado productos favoritos
      </Text>
      <Text style={styles.txt_cont_fav}>
        Guarda tus productos favoritos para que accedas fácilmente a su
        información
      </Text>
    </View>
  );
}

export function StoreDefecto() {
  return (
    <View style={[styles.view_defecto]}>
      <FontAwesome5
        name="store"
        size={60}
        color={colores.primario}
        style={{ marginTop: 5 }}
      />
      <Text style={styles.txt_titulo}>No hay tiendas registradas</Text>
      <Text style={styles.txt_context}>
        Forma parte de nuestras tiendas y disfruta los beneficios
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  view_defecto: {
    width: "90%",
    borderWidth: 4,
    borderColor: "#f5f5f5",
    alignSelf: "center",
    marginVertical: 10,
    paddingVertical: 5,
    paddingHorizontal: 5,
    alignItems: "center",
    height: 180,
  },
  txt_titulo: { fontSize: 18, marginTop: 5 },
  txt_tit_fav: { fontSize: 11, marginTop: 5, color: colores.terciario },
  txt_cont_fav: {
    fontSize: 11,
    textAlign: "center",
    marginTop: 5,
    marginHorizontal: 10,
    color: colores.dark
  },
  txt_context: {
    fontSize: 12,
    textAlign: "center",
    marginTop: 5,
    marginHorizontal: 5,
  },
});
