import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
} from "react-native";
import colores from "../../styles/colores";
import { formStyles } from "../../styles/estilos";
import { TextInput, Button } from "react-native-paper";
import { getAllCategorias } from "../../api/categorias";

export default function ModalFilter(props) {
  const { modalDescription, setModalDescription } = props;
  const [categoria, setCategoria] = useState(null);

  useEffect(() => {
    (async () => {
      const categorias = await getAllCategorias();
      setCategoria(categorias.data);
    })();
  }, []);

  return (
    <View>
      <Modal
        visible={modalDescription}
        transparent={true}
        animationType={"slide"}
      >
        <TouchableWithoutFeedback
          onPress={() => setModalDescription(!modalDescription)}
        >
          <View style={styles.modalOverlay} />
        </TouchableWithoutFeedback>
        <View style={styles.modalView}>
          <View style={{ marginHorizontal: 5 }}>
            <View style={{ marginVertical: 5, width: "100%" }}>
              <View style={{ alignItems: "center", marginVertical: 2 }}>
                <Text style={{ fontSize: 16, color: colores.primario }}>
                  Filtros
                </Text>
              </View>
            </View>
            <FiltroCategorias categoria={categoria} />
            <View style={{ marginVertical: 15, width: "100%" }}>
              <Text
                style={{
                  fontSize: 14,
                  color: colores.secundario,
                  marginBottom: 10,
                }}
              >
                Precios
              </Text>
              <View style={{ flexDirection: "row", width: "50%" }}>
                <View style={{ width: "100%" }}>
                  <TextInput
                    label="Min"
                    style={{ height: 35, width: "80%" }}
                    mode="outlined"
                    selectionColor={colores.color_sec}
                  />
                </View>

                <View style={{ width: "100%" }}>
                  <TextInput
                    label="Max"
                    style={{ height: 35, width: "80%" }}
                    mode="outlined"
                    selectionColor={colores.color_sec}
                  />
                </View>
              </View>
            </View>

            <View style={{ marginVertical: 10, width: "100%" }}>
              <Text
                style={{
                  fontSize: 14,
                  color: colores.secundario,
                }}
              >
                Tamaños
              </Text>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    textAlignVertical: "center",
                    borderRadius: 40,
                    height: 40,
                    width: 40,
                    backgroundColor: colores.color_blanco,
                    margin: 10,
                    borderColor: colores.secundario,
                    borderWidth: 1,
                  }}
                >
                  <Text
                    style={{
                      color: colores.secundario,
                      fontSize: 16,
                    }}
                  >
                    S
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ marginVertical: 10, width: "100%" }}>
              <Text
                style={{
                  fontSize: 14,
                  color: colores.secundario,
                }}
              >
                Colores
              </Text>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    textAlignVertical: "center",
                    borderRadius: 30,
                    height: 30,
                    width: 30,
                    backgroundColor: colores.secundario,
                    margin: 10,
                    borderColor: colores.secundario,
                    borderWidth: 1,
                  }}
                ></View>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    textAlignVertical: "center",
                    borderRadius: 30,
                    height: 30,
                    width: 30,
                    backgroundColor: colores.primario,
                    margin: 10,
                    borderColor: colores.primario,
                    borderWidth: 1,
                  }}
                ></View>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    textAlignVertical: "center",
                    borderRadius: 30,
                    height: 30,
                    width: 30,
                    backgroundColor: colores.terciario,
                    margin: 10,
                    borderColor: colores.terciario,
                    borderWidth: 1,
                  }}
                ></View>
              </View>
            </View>
            <View style={{ width: "100%", alignItems: "center" }}>
              <View
                style={{
                  width: "95%",
                  backgroundColor: colores.secundario,
                  borderRadius: 8,
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    color: colores.color_blanco,
                    marginVertical: 10,
                    alignSelf: "center",
                  }}
                >
                  Crear Filtro
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

function FiltroCategorias(props) {
  const { categoria } = props;
  return (
    <>
      <View style={{ marginVertical: 2 }}>
        <Text style={{ fontSize: 14, color: colores.secundario }}>
          Categorias Productos
        </Text>
        <View style={{ flexDirection: "row" }}>
          <Text
            style={{
              marginTop: 10,
              marginHorizontal: 2,
              paddingHorizontal: 12,
              paddingVertical: 4,
              borderRadius: 15,
              backgroundColor: colores.secundario,
              color: colores.color_blanco,
            }}
          >
            Hombres
          </Text>
        </View>
        {categoria.map((element, key) => {
          <View key={key}>
            <Text>1</Text>
          </View>;
        })}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  counterStyle: {
    margin: 15,
    marginBottom: 28,
    marginTop: 10,
  },
  modalOverlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "transparent",
  },
  modalView: {
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    borderColor: colores.secundario,
    padding: 9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    bottom: 0,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: "100%",
    position: "absolute",
    paddingBottom: 20,
    backgroundColor: colores.secundarioLight,
  },
  colorsView: {
    borderRadius: 20,
    height: 20,
    width: 20,
    padding: 7,
    elevation: 3,
    shadowColor: "#000",
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    margin: Platform.OS === "ios" ? 8.5 : 9.5,
  },
  labelText: {
    padding: 8,
    paddingHorizontal: 15,
    alignSelf: "flex-start",
  },
  recentSearchArrayView: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 8,
    paddingTop: 0,
    paddingBottom: 0,
  },
  textAttributes: {
    borderWidth: 1,
    marginBottom: 10,
  },
  textTouchable: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15,
    padding: 7,
    margin: 5,
    paddingTop: 5,
    paddingBottom: 5,
  },
});
