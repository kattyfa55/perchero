import React, { useState } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
} from "react-native";
import colores from "../../styles/colores";

const descripciones = [
  { name: "Talla", isSelected: true, tipo: 2 },
  { name: "Color", isSelected: false, tipo: 1 },
];

const colors = [
  { name: "#34495E", isSelected: true, tipo: 1 },
  { name: "#C70039", isSelected: false, tipo: 1 },
  { name: "#138D75", isSelected: false, tipo: 1 },
];

const size = [
  { name: "L", isSelected: true, tipo: 2 },
  { name: "M", isSelected: false, tipo: 2 },
  { name: "S", isSelected: false, tipo: 2 },
  { name: "XL", isSelected: false, tipo: 2 },
];

export default function ModalDescription(props) {
  const { modalDescription, setModalDescription } = props;
  return (
    <View>
      <Modal
        visible={modalDescription}
        transparent={true}
        animationType={"slide"}
      >
        <TouchableWithoutFeedback
          onPress={() => setModalDescription(!modalDescription)}
        >
          <View style={styles.modalOverlay} />
        </TouchableWithoutFeedback>

        <View style={styles.modalView}>
          <View>
            {descripciones.map((descripcion, key) => (
              <DetalleDescripcion descripcion={descripcion} key={key} />
            ))}
            <View style={{ paddingTop: 20 }}></View>
            {/*  <View
              style={{
                backgroundColor: colores.color_qui,
                height: 40,
                width: "90%",
                alignSelf: "center",
                borderRadius: 10,
              }}
            >
              <Text
                style={{
                  color: colores.color_blanco,
                  fontWeight: "bold",
                  alignSelf: "center",
                  justifyContent: "center",
                }}
              >
                Comprar
              </Text>
            </View> */}
          </View>
        </View>
      </Modal>
    </View>
  );
}

function DetalleDescripcion(props) {
  const { descripcion } = props;
  const { name, tipo } = descripcion;

  return (
    <>
      <View>
        <Text
          style={{
            paddingLeft: 15,
            paddingVertical: 5,
            color: colores.color_sec,
            fontSize: 15,
            fontWeight: "bold",
          }}
        >
          {name}
        </Text>
      </View>
      <View
        style={{ paddingLeft: 15, flexDirection: "row", paddingVertical: 5 }}
      >
        {tipo == 1 && <InfoColores />}
        {tipo == 2 && <InfoSize />}
      </View>
    </>
  );
}

function InfoSize(props) {
  return (
    <>
      {size.map((talla, key) => (
        <View
          style={{
            width: 50,
            height: 50,
            //backgroundColor: colores.color_qui,
            borderRadius: 40,
            paddingLeft: 15,
          }}
        >
          <Text
            style={{
              color: colores.color_pri,
              fontWeight: "bold",
              fontSize: 18,
            }}
          >
            {talla.name}
          </Text>
        </View>
      ))}
    </>
  );
}

function InfoColores(props) {
  return (
    <>
      {colors.map((col, key) => (
        <>
          <View
            style={{
              width: 30,
              height: 30,
              backgroundColor: col.name,
              borderRadius: 30,
              paddinpagLeft: 15,
            }}
          ></View>
          <View style={{ paddingLeft: 5 }}></View>
        </>
      ))}
    </>
  );
}

const styles = StyleSheet.create({
  counterStyle: {
    margin: 15,
    marginBottom: 28,
    marginTop: 10,
  },
  modalOverlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "transparent",
  },
  modalView: {
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    padding: 9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    bottom: 0,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: "100%",
    position: "absolute",
    paddingBottom: 20,
    backgroundColor: "#ffff",
  },
  colorsView: {
    borderRadius: 20,
    height: 20,
    width: 20,
    padding: 7,
    elevation: 3,
    shadowColor: "#000",
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    margin: Platform.OS === "ios" ? 8.5 : 9.5,
  },
  labelText: {
    padding: 8,
    paddingHorizontal: 15,
    alignSelf: "flex-start",
  },
  recentSearchArrayView: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 8,
    paddingTop: 0,
    paddingBottom: 0,
  },
  textAttributes: {
    borderWidth: 1,
    marginBottom: 10,
  },
  textTouchable: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15,
    padding: 7,
    margin: 5,
    paddingTop: 5,
    paddingBottom: 5,
  },
});
