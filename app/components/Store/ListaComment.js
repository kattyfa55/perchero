import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Avatar, Rating } from "react-native-elements";
import { map } from "lodash";
import { getCommentStore } from "../../api/comentario";
import Loading from "../Comunes/Loading";
import { format } from "date-fns";
import { SERVIDOR_PRODUCCION } from "../../utils/constants";

export default function ListComment(props) {
  const { id_tienda, navigation } = props;
  const [userLogged, setUserLogged] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const [reviews, setReviews] = useState([]);

  const formData = {
    dato: id_tienda,
  };

  const cargarCommentarios = async () => {
    let datos = await getCommentStore(formData);
    setReviews(datos);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      (async () => {
        cargarCommentarios();
      })();
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    //setIsLoading(true);
    cargarCommentarios();

    //setIsLoading(false);
  }, []);

  return (
    <View>
      <Loading isVisible={isLoading} text="Cargando" />
      {map(reviews, (review, index) => (
        <Review key={index} review={review} />
      ))}
    </View>
  );
}

function Review(props) {
  const { review } = props;
  const {
    calificacion_comentarios_tienda,
    descripcion_comentarios_tienda,
    created_at,
    avatar_user,
  } = review;

  return (
    <View style={styles.viewReview}>
      <View style={styles.viewImageAvatar}>
        <Avatar
          size="large"
          rounded
          containerStyle={styles.imageAvatarUser}
          source={
            avatar_user != "Ninguno"
              ? { uri: SERVIDOR_PRODUCCION + avatar_user }
              : { uri: SERVIDOR_PRODUCCION + "img/user.png" }
          }
        />
      </View>
      <View style={styles.viewInfo}>
        <Text style={styles.reviewText}>{descripcion_comentarios_tienda}</Text>
        <Rating
          imageSize={15}
          startingValue={calificacion_comentarios_tienda}
          readonly
        />
        <Text style={styles.reviewDate}>{created_at}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  btnAddReview: {
    backgroundColor: "transparent",
  },
  btnTitleAddReview: {
    color: "#00a680",
  },
  viewReview: {
    flexDirection: "row",
    padding: 10,
    paddingBottom: 20,
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1,
  },
  viewImageAvatar: {
    marginRight: 15,
  },
  imageAvatarUser: {
    width: 50,
    height: 50,
  },
  viewInfo: {
    flex: 1,
    alignItems: "flex-start",
  },
  reviewTitle: {
    fontWeight: "bold",
  },
  reviewText: {
    paddingTop: 2,
    color: "grey",
    marginBottom: 5,
  },
  reviewDate: {
    marginTop: 5,
    color: "grey",
    fontSize: 12,
    position: "absolute",
    right: 0,
    bottom: 0,
  },
});
