import React from "react";
import { Provider, FAB } from "react-native-paper";
import Search from "../Search";
import StatusBarCustom from "../../components/StatusBar";
import { StyleSheet, View, Dimensions, Image } from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { mapStyle } from "../../styles/mapStyles";
import { MARKERS_DATA } from "../../utils/constantes/MARKERS_DATA";
import { CustomMarker } from "../../utils/mapas/CustomMarker";
import colores from "../../styles/colores";
//import { mapStyle } from "./mapStyle";

export default function VerMapa(props) {
  const { route } = props;
  const { latitud, longitud } = route.params;

  return (
    <Provider>
      <StatusBarCustom />
      <MapView
        customMapStyle={mapStyle}
        provider={PROVIDER_GOOGLE}
        style={{ flex: 1 }}
        region={{
          latitude: parseFloat(latitud),
          longitude: parseFloat(longitud),
          latitudeDelta: 0.0143,
          longitudeDelta: 0.0134,
        }}
      >
        <Marker
          coordinate={{
            latitude: parseFloat(latitud),
            longitude: parseFloat(longitud),
          }}
        >
          <View style={styles.markerWrapper}>
            <Image
              style={{ height: 30, width: 30, borderRadius: 30 }}
              source={require("./../../../assets/logo.png")}
            />
          </View>
        </Marker>
      </MapView>
    </Provider>
  );
}
const styles = StyleSheet.create({
  markerWrapper: {
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    height: 20,
    width: 20,
    borderRadius: 20,
    backgroundColor: colores.primario,
  },
});
