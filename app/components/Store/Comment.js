import React, { useState, useRef } from "react";
import { Button } from "react-native";
import { StyleSheet } from "react-native";
import { View, Text, Image } from "react-native";
import { AirbnbRating } from "react-native-elements";
import { Input } from "react-native-elements/dist/input/Input";
import colores from "../../styles/colores";
import { TouchableOpacity } from "react-native";
import Toast from "react-native-root-toast";
import { ScrollView } from "react-native-gesture-handler";
import { ToastAndroid } from "react-native";
import { addComment } from "../../api/comentario";
import { getUser } from "../../api/user";
import { URL_API } from "../../api/rutas";
import Loading from "../Comunes/Loading";
import { SERVIDOR_PRODUCCION } from "../../utils/constants";
import { getUserApi } from "../../api/storage";

export default function Comment(props) {
  const { route, navigation } = props;
  const { id_tienda, nombre_tienda, logo_tienda } = route.params;
  const [isLoading, setIsLoading] = useState(false);
  const [rating, setRating] = useState(null);
  const [tienda, setTienda] = useState("");
  const [review, setReview] = useState("");
  const toastRef = useRef();
  const addRevew = () => {
    if (!rating) {
      ToastAndroid.show("No has dado ninguna puntuación", 1000);
    } else if (!review) {
      ToastAndroid.show("El comentario es obligatorio", 1000);
    } else {
      (async () => {
        setIsLoading(true);
        let datos = await getUserApi();
        let users = JSON.parse(datos);
        let user_id = users.id;
        const formData = {
          users_id: user_id,
          tiendas_id: id_tienda,
          descripcion: review,
          calificacion: rating,
        };
        console.log(formData);
        await addComment(formData)
          .then((result) => {
            console.log(result);
            if (result.result == true) {
              ToastAndroid.show("Guardado Exitoso", 1000);
              navigation.goBack("option-store");
            }
          })
          .catch(() => {
            toastRef.current.show("Error al guardar la comentario");
            setIsLoading(false);
          });
        setIsLoading(false);
      })();
    }
  };

  return (
    <ScrollView>
      <Loading isVisible={isLoading} text="Enviando" />
      <Logo logo_tienda={logo_tienda} />
      <AirbnbRating
        count={5}
        reviews={["Pésimo", "Deficiente", "Normal", "Muy Bueno", "Excelente"]}
        defaultRating={0}
        size={38}
        onFinishRating={(value) => {
          setRating(value);
        }}
      />
      <View style={{ paddingHorizontal: 20, paddingVertical: 10 }}>
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <Text
            style={{
              paddingTop: 30,
              color: colores.secundario,
              fontSize: 20,
              fontWeight: "bold",
            }}
          >
            ¿Qué opinas sobre {nombre_tienda} ?
          </Text>
        </View>

        <View style={{ paddingTop: 25 }}>
          <Input
            placeholder="Comentario..."
            multiline={true}
            inputContainerStyle={styles.textArea}
            onChange={(e) => setReview(e.nativeEvent.text)}
          />
        </View>
        <View
          style={{
            backgroundColor: colores.secundario,
            paddingVertical: 10,
            borderRadius: 5,
          }}
        >
          <TouchableOpacity
            onPress={addRevew}
            style={{
              backgroundColor: colores.secundario,
              width: "100%",
              borderRadius: 5,
              alignItems: "center",
            }}
          >
            <Text
              style={{
                fontSize: 18,
                color: colores.color_blanco,
              }}
            >
              Enviar Comentario
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

function Logo(props) {
  const { logo_tienda } = props;
  let ruta = `${SERVIDOR_PRODUCCION}` + logo_tienda;
  return (
    <View style={{ alignItems: "center", paddingTop: 10 }}>
      <Image
        style={{
          width: "100%",
          height: 250,
          resizeMode: "contain",
        }}
        source={{ uri: ruta }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  btnContainer: {
    flex: 1,
    justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: colores.primario,
  },
  centeredView: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    height: "95%",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 20,
    textAlign: "center",
    fontSize: 18,
    color: colores.secundario,
    fontWeight: "bold",
  },

  btnAddReview: {
    backgroundColor: "transparent",
  },
  btnTitleAddReview: {
    color: colores.secundario,
  },
  viewReview: {
    flexDirection: "row",
    padding: 10,
    paddingBottom: 20,
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1,
  },
  viewImageAvatar: {
    marginRight: 15,
  },
  imageAvatarUser: {
    width: 50,
    height: 50,
  },
  viewInfo: {
    flex: 1,
    alignItems: "flex-start",
  },
  reviewTitle: {
    fontWeight: "bold",
  },
  reviewText: {
    paddingTop: 2,
    color: "grey",
    marginBottom: 5,
  },
  reviewDate: {
    marginTop: 5,
    color: "grey",
    fontSize: 12,
    position: "absolute",
    right: 0,
    bottom: 0,
  },
});
