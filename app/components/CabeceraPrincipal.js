import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Image } from "react-native-elements/dist/image/Image";
import { FontAwesome5 } from "@expo/vector-icons";
import colores from "../styles/colores";
import { useNavigation } from "@react-navigation/native";

export default function CabeceraPrincipal(props) {
  const navigation = useNavigation();
  const buscarProduct = () => {
    navigation.navigate("all-product-buscar", { tipo_prod: "" });
    return;
  };
  return (
    <View
      style={{
        width: "100%",
        flexDirection: "row",
        backgroundColor: colores.color_blanco,
      }}
    >
      <View
        style={{
          width: "40%",
          backgroundColor: colores.color_blanco,
          marginLeft: 15,
        }}
      >
        <Image
          style={{
            height: 70,
            resizeMode: "contain",
            flexWrap: "wrap",
          }}
          source={require("../../assets/logo33.png")}
        />
      </View>
      <View style={{ width: "60%", alignSelf: "center" }}>
        <TouchableOpacity
          style={{
            width: "100%",
            alignItems: "flex-end",
            paddingRight: 30,
          }}
          onPress={() => buscarProduct()}
        >
          <FontAwesome5 name="search" size={25} color={colores.primario} />
        </TouchableOpacity>
      </View>
    </View>
  );
}
