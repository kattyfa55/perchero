import React from "react";
import {StyleSheet, View, Text, ActivityIndicator} from "react-native";
import {Overlay} from "react-native-elements";
import colores from "../../styles/colores";

export default function Loading(props) {
    const {isVisible, text} = props;

    return(
        <Overlay 
            isVisible={isVisible}
            windowBackgroundColor="rgba(0, 0, 0, 0.5)"
            overlayBackgroundColor="transparent"
            overlayStyle={styles.overlay}
        >
            <View style={styles.view}>
                <ActivityIndicator size="large" color={colores.primario}/>
                {text ? <Text style={styles.text}>{text}</Text> : <Text> UserNull </Text>}
            </View>
        </Overlay>
    );
}

const styles = StyleSheet.create({
    overlay:{
        height:100,
        width:200,
        backgroundColor     : colores.color_blanco,
        borderColor         : colores.primario,
        borderWidth: 2,
        borderRadius:10,
    },
    view:{
        flex: 1,
        alignItems:"center",
        justifyContent:"center",
        alignSelf:'center'
    },
    text: {
        color:colores.primario,
        textTransform: "uppercase",
        marginTop:10,
        textAlign:'center'
    }
});