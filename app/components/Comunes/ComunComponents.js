import React from "react";
import { View, Text, Image } from "react-native";
import colores from "../../styles/colores";
import { SLIDERWIDTH } from "../../utils/constants";
import { TouchableOpacity } from "react-native-gesture-handler";
import { productStyle } from "../../styles/estilos";
import { Icon } from "react-native-elements";

export default function EncabezadoPortadas(props) {
  const { titulo, fontTam, goScreen } = props;
  return (
    <>
      <View>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: colores.color_cua,
            paddingLeft: 15,
          }}
        >
          <TouchableOpacity onPress={goScreen}>
            <Text
              style={{
                fontSize: fontTam,
                paddingTop: 5,
                fontWeight: "bold",
                color: colores.secundario,
              }}
            >
              {titulo}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

export function LineaSeparation() {
  return (
    <View
      style={{
        borderBottomColor: "black",
        borderBottomWidth: 1,
      }}
    ></View>
  );
}

export function LineaColores() {
  return (
    <View
      style={{
        backgroundColor: colores.secundario,
        width: SLIDERWIDTH,
        flexDirection: "row",
      }}
    >
      <LineaColor color={colores.primario} />
      <LineaColor color={colores.secundario} />
      <LineaColor color={colores.terciario} />
      <LineaColor color={colores.primario} />
      <LineaColor color={colores.secundario} />
      <LineaColor color={colores.terciario} />
    </View>
  );
}

export function LineBase() {
  return (
    <View
      style={{
        backgroundColor: colores.secundario,
        width: SLIDERWIDTH,
        flexDirection: "row",
        height: 5,
      }}
    ></View>
  );
}

function LineaColor(props) {
  const { color } = props;

  return (
    <View
      style={{
        backgroundColor: color,
        height: 5,
        width: SLIDERWIDTH / 6,
      }}
    ></View>
  );
}

export function TituloView(props) {
  const { color, txt_titulo } = props;

  return (
    <View style={{ marginTop: 10, paddingLeft: 15 }}>
      <Text style={productStyle.titleProducto}> {txt_titulo} </Text>
    </View>
  );
}

export function LogoUri(props) {
  const { ruta } = props;

  return (
    <View style={{ alignItems: "center", paddingTop: 10 }}>
      <Image
        style={{
          width: "100%",
          height: 280,
          resizeMode: "contain",
        }}
        source={{ uri: ruta }}
      />
    </View>
  );
}

export function ImagenUri(props) {
  const { ruta, size } = props;

  return (
    <View style={{ alignItems: "center", paddingTop: 20, marginBottom: 40 }}>
      <Image
        style={{
          width: "100%",
          height: size,
          resizeMode: "contain",
        }}
        source={{ uri: ruta }}
      />
    </View>
  );
}

export function LogoPerchero(props) {
  const { alto } = props;
  return (
    <View style={{ paddingTop: 25 }}>
      <Image
        style={{
          width: "100%",
          height: alto,
          resizeMode: "contain",
        }}
        source={require("../../../assets/img/logo.png")}
      />
    </View>
  );
}

export function LogoPerfilVendedor(props) {
  const { alto } = props;

  return (
    <View style={{ paddingTop: 70 }}>
      <Image
        style={{
          width: "100%",
          height: alto,
          resizeMode: "contain",
        }}
        source={{ uri: "http://elperchero.store/img/perfil-vendedor.png" }}
      />
    </View>
  );
}

export function EsloganPerchero(props) {
  const { alto } = props;

  return (
    <View style={{ paddingTop: 10 }}>
      <Image
        style={{
          width: "100%",
          height: alto,
          resizeMode: "contain",
        }}
        source={require("../../../assets/img/eslogan.png")}
      />
    </View>
  );
}

export function CarrouselTiendas(props) {
  const { ruta } = props;
  let data = [];
  data.push(ruta);

  return (
    <View style={{ alignItems: "center", paddingTop: 10 }}>
      <Image
        style={{
          width: "100%",
          height: 280,
          resizeMode: "contain",
        }}
        source={{ uri: data[0] }}
      />
    </View>
  );
}

export function IconosInfo(props) {
  const { name, color, tipo, tam } = props;

  return (
    <View
      style={{
        marginTop: 15,
        width: "15%",
      }}
    >
      <Icon name={name} color={color} type={tipo} size={tam} />
    </View>
  );
}

export function ViewInfoUserAccount(props) {
  const { titulo, dato } = props;

  return (
    <View style={{ margin: 20, flexDirection: "row" }}>
      <Text style={{ width: "35%", fontSize: 14 }}>{titulo} :</Text>
      <Text
        style={{
          width: "70%",
          marginLeft: 10,
          fontSize: 14,
          color: colores.terciario,
          textTransform: "uppercase",
        }}
      >
        {dato ? dato : "Ninguno"}
      </Text>
    </View>
  );
}

export function ViewInfoDetalles(props) {
  const { titulo, dato } = props;

  return (
    <View
      style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: "row" }}
    >
      <Text
        style={{
          width: "25%",
          fontSize: 12,
          textTransform: "uppercase",
          fontWeight: "bold",
        }}
      >
        {titulo} :
      </Text>
      <Text
        style={{
          width: "75%",
          marginLeft: 10,
          fontSize: 14,
          color: colores.terciario,
          textAlign: "justify",
          textTransform: "uppercase",
        }}
      >
        {dato}
      </Text>
    </View>
  );
}

export function ViewInfoColores(props) {
  const { titulo, dato } = props;

  return (
    <View
      style={{
        marginHorizontal: 20,
        marginVertical: 10,
        flexDirection: "row",
      }}
    >
      <Text
        style={{
          width: "25%",
          fontSize: 12,
          fontWeight: "bold",
          textTransform: "uppercase",
        }}
      >
        {titulo} :
      </Text>
      <View
        style={{
          width: "75%",
          marginLeft: 10,
          fontSize: 14,
          color: colores.terciario,
          textAlign: "justify",
          flexDirection: "row",
        }}
      >
        {/*   {color && <View></View>} */}
        {/* <InfoColores colores={color} /> */}
        <View
          style={{
            /*  backgroundColor: "'" + color + "'", */
            width: 20,
            height: 20,
            borderRadius: 20,
            marginRight: 8,
            borderRadius: 15,
          }}
        ></View>
      </View>
    </View>
  );
}

function InfoColores(props) {
  const { colores } = props;

  const colors = [
    { name: "#34495E", isSelected: true, tipo: 1 },
    { name: "#C70039", isSelected: false, tipo: 1 },
    { name: "#138D75", isSelected: false, tipo: 1 },
  ];
  return (
    <>
      {colors.map((col, key) => (
        <>
          <View
            key={key}
            style={{
              width: 20,
              height: 20,
              backgroundColor: col.name,
              borderRadius: 20,
              paddinpagLeft: 15,
            }}
          ></View>
          <View style={{ paddingLeft: 5 }}></View>
        </>
      ))}
    </>
  );
}
