import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Linking,
} from "react-native";
import { SHARE_PRODUCT } from "../../api/rutas";
import colores from "../../styles/colores";

export default function ListProduct(props) {
  const { products, navigation } = props;
  const goToProduct = (producto) => {
    navigation.navigate("product", { idProduct: producto.id_productos });
  };

  const buyByWhatsapp = (producto) => {
    let whatsapp = producto.telefono_tienda;
    let nomb_tienda = producto.nombre_tienda;
    let codigo = producto.codigo_productos;
    let share = SHARE_PRODUCT + codigo;
    let mensaje_whatsapp =
      "He visto este producto (" +
      share +
      " ) en tu tienda " +
      nomb_tienda +
      " en http://elperchero.store, me gustaría comprarlo ";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  return (
    <>
      {products.map((product, key) => (
        <View
          key={key}
          style={{ width: "50%", borderColor: colores.primario, height: 150 }}
        >
          <ViewProductos
            product={product}
            goToProduct={goToProduct}
            buyByWhatsapp={buyByWhatsapp}
          />
          <View></View>
        </View>
      ))}
    </>
  );
}

function ViewProductos(props) {
  const { product, goToProduct, buyByWhatsapp, key } = props;

  return (
    <TouchableWithoutFeedback onPress={() => goToProduct(product)}>
      <View key={key} style={styles.containerProduct}>
        <View
          style={{
            backgroundColor: colores.secundario,
            borderColor: colores.primario,
            borderBottomWidth: 5,
          }}
        >
          <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
            {product.nombre_productos}
          </Text>
          <Text
            style={{
              color: colores.primario,
              alignSelf: "center",
              marginBottom: 4,
            }}
          >
            Chami Store
          </Text>
        </View>

        <View style={styles.product}>
          <Image
            style={styles.image}
            source={{
              uri: "http://192.168.40.15/img/productos/camisa1.png",
            }}
          />
        </View>
        <View
          style={{
            backgroundColor: colores.terciario,
            width: "100%",
            flexDirection: "row",
          }}
        >
          <View style={{ width: "50%" }}>
            <Text
              style={{
                color: colores.color_blanco,
                fontSize: 20,
                padding: 5,
              }}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              $ {product.precio_productos}
            </Text>
          </View>
          <View style={{ width: "50%", padding: 5 }}>
            <TouchableOpacity
              style={{
                backgroundColor: colores.secundario,
                paddingVertical: 5,
                borderRadius: 5,
              }}
              onPress={() => buyByWhatsapp(product)}
            >
              <Text
                style={{
                  color: colores.color_blanco,
                  fontWeight: "bold",
                  alignSelf: "center",
                }}
              >
                Comprar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    //backgroundColor: "#f0f0f0",
    padding: 10,
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },
  image: {
    height: 150,
    resizeMode: "contain",
  },
  name: {
    marginTop: 5,
    fontSize: 16,
    color: colores.color_blanco,
    alignSelf: "center",
  },
});
