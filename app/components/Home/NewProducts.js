import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { getNewProductos } from "../../api/productos";
import { productStyle } from "../../styles/estilos";
import ListProduct from "./ListProduct";

export default function NewProducts() {
  const [products, setProducts] = useState(null);
  useEffect(() => {
    (async () => {
      const response = await getNewProductos();
      setProducts(response.data);
    })();
  }, []);

  return (
    <View style={productStyle.containerProducto}>
      <Text style={productStyle.titleProducto}> Productos recientes </Text>
      {products && <ListProduct products={products} />}
    </View>
  );
}
