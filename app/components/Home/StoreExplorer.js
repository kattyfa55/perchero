import React from "react";
import { View, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import colores from "../../styles/colores";
import { SLIDERWIDTH } from "../../utils/constants";
import EncabezadoPortadas from "../Comunes/ComunComponents";

export default function StoreExplorer() {
  const goStoreAll = () => {};
  return (
    <>
      <EncabezadoPortadas
        titulo="EXPLORAR TIENDAS"
        textLink="Ver tiendas"
        color={colores.color_cua}
        fontTam={14}
        goScreen={goStoreAll}
      />
    </>
  );
}
