import React from "react";
import { StatusBar, SafeAreaView } from "react-native";
import colores from "../styles/colores";

export default function StatusBarCustom(props) {
  const color = colores.secundario;
  
  const { backgroundColor=color, ...rest } = props;

  return (
    <>
      <StatusBar backgroundColor={backgroundColor} {...rest} />
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: backgroundColor,
        }}
      />
    </>
  );
}
