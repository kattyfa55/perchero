import React from 'react'
import { View, Text } from 'react-native'
import colores from '../styles/colores'

export default function Version() {
    return (
        <View style = {{alignItems:'center',paddingVertical:5}}>      
            <Text style = {{color: colores.color_qui }}> Version 1 </Text>
        </View>
    )
}
