import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { IconButton } from "react-native-paper";
import colores from "../../styles/colores";
import { userStyles } from "../../styles/estilos";

export default function UserInfo(props) {
  const { user } = props;
  return (
    <View style={userStyles.container}>
      <Text style={userStyles.titleName}>
        {user ? (
          <>
            <Text>{user.nombre_users} </Text> {user.apellido_users}
          </>
        ) : (
          <Text></Text>
        )}
      </Text>
    </View>
  );
}
