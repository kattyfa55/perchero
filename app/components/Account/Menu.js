import React, { useEffect, useState } from "react";
import { Alert, Text, View } from "react-native";
import { List } from "react-native-paper";
import { getUserApi, removeUserApi } from "../../api/storage";
import { removeTokenApi } from "../../api/token";
import AsyncStorage from "@react-native-async-storage/async-storage";
import colores from "../../styles/colores";
import { IconosInfo } from "../Comunes/ComunComponents";

export default function Menu(props) {
  const { setLogin, user, setUser, navigation } = props;
  const [usuario, setUsuario] = useState(null);

  const cerrarSesion = async () => {
    await removeUserApi();
    await removeTokenApi();
    setLogin(false);
    setUser(null);
    const keys = await AsyncStorage.getAllKeys();
  };

  const logoutAccount = () => {
    Alert.alert(
      "Cerrar sesión",
      "¿Estas seguro de que quieres salir de tu cuenta?",
      [{ text: "NO" }, { text: "SI", onPress: cerrarSesion }],
      { cancelable: false }
    );
  };

  return (
    <>
      <List.Section>
        <List.Item
          title="Mi Perfil"
          description="Información del usuario"
          left={() => (
            <IconosInfo
              name="user-alt"
              color={colores.terciario}
              tipo="font-awesome-5"
              tam={20}
            />
          )}
          onPress={() => navigation.navigate("perfil", { user })}
        />

        <List.Item
          title="Administración"
          description="Tiendas y Productos"
          left={() => (
            <IconosInfo
              name="store"
              color={colores.terciario}
              tipo="font-awesome-5"
              tam={20}
            />
          )}
          onPress={() => navigation.navigate("mis-tiendas")}
        />

        <List.Item
          title="Contraseña"
          description="Actualizar contraseña"
          left={() => (
            <IconosInfo
              name="key"
              color={colores.terciario}
              tipo="font-awesome-5"
              tam={20}
            />
          )}
          onPress={() => navigation.navigate("seguridad-options", { user })}
        />
        <List.Item
          title="Lista de deseos"
          description="Listado de los productos que te gustaron"
          left={() => (
            <IconosInfo
              name="heart"
              color={colores.terciario}
              tipo="font-awesome"
              tam={20}
            />
          )}
          onPress={() => navigation.navigate("favoritos")}
        />
        <List.Item
          title="Cerrar sesión"
          description="Cierra esta sesión e inicia con otra"
          left={() => (
            <IconosInfo
              name="exit"
              color={colores.terciario}
              tipo="ionicon"
              tam={24}
            />
          )}
          onPress={logoutAccount}
        />
      </List.Section>
    </>
  );
}
