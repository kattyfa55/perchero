import React, { useEffect, useState } from "react";
import { getUserApi } from "../../api/storage";
import Loading from "../../components/Comunes/Loading";
import ListFavorite from "./ListFavorite";
import UserInvitado from "./../Account/UserInvitado";
import StatusBarCustom from "../../components/StatusBar";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { ScrollView } from "react-native-gesture-handler";
import colores from "../../styles/colores";

export default function Favorites(props) {
  const { navigation } = props;
  const [login, setLogin] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    cargaDataInicial();
    const unsubscribe = navigation.addListener("focus", async () => {
      cargaDataInicial();
    });
    return unsubscribe;
  }, []);

  const cargaDataInicial = async () => {
    setUser(null);
    let datos = await getUserApi();
    setUser(JSON.parse(datos));
    !datos ? setLogin(false) : setLogin(true);
  };

  if (login === null) return <Loading isVisible={true} text="Cargando..." />;

  return login ? (
    <ScreenFavorito navigation={navigation} usuario={user} />
  ) : (
    <UserInvitado setLogin={setLogin} />
  );
}

function ScreenFavorito(props) {
  const { navigation, usuario } = props;

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <ScrollView style={{ backgroundColor: colores.color_blanco }}>
        <ListFavorite navigation={navigation} usuario={usuario} />
      </ScrollView>
    </>
  );
}
