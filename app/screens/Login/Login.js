import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { StatusBar } from "expo-status-bar";
import {
  blanco_color,
  color_blanco,
  principal_color,
} from "../../styles/colores";

export default function Login() {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Text
        style={{ color: principal_color, fontWeight: "bold", fontSize: 40 }}
      >
        Perchero
      </Text>
      <Logo />
      <Omitir />
      <IniciarSesion />
    </View>
  );
}

function Logo() {
  return (
    <View
      style={{
        alignSelf: "center",
        height: 220,
        width: "100%",
        alignItems: "center",
        marginTop: 25,
      }}
    >
      <Image
        source={require("../../assets/logo.png")}
        style={styles_login.image}
      />
    </View>
  );
}

function Omitir() {
  return (
    <TouchableOpacity style={styles_login.princ_view_log}>
      <Text style={styles_login.princ_text_log}>Empezar</Text>
    </TouchableOpacity>
  );
}

function IniciarSesion() {
  const iniciarSesion = () => {};

  return (
    <TouchableOpacity
      style={styles_login.princ_view_inic}
      onPress={iniciarSesion}
    >
      <Text style={styles_login.princ_text_inic}>Iniciar Sesión</Text>
    </TouchableOpacity>
  );
}

const styles_login = StyleSheet.create({
  princ_view_log: {
    backgroundColor: principal_color,
    width: "80%",
    height: 45,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    elevation: 15,
    marginTop: 20,
  },
  princ_text_log: {
    color: color_blanco,
    fontWeight: "bold",
  },
  princ_view_inic: {
    marginTop: 20,
    backgroundColor: color_blanco,
    width: "80%",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
  },
  princ_text_inic: {
    color: principal_color,
    fontWeight: "bold",
  },
  image: {
    flex: 1,
    width: 300,
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
