import React, { useState, useEffect } from "react";
import { ScrollView, Text, View, TouchableOpacity, Share } from "react-native";
import { Divider, List } from "react-native-paper";

import colores from "../../styles/colores";
import { getProducto } from "../../api/productos";
import ScreenLoading from "../../components/Comunes/ScreenLoading";
import CarouselImages from "../../components/Products/CarouselImages";
import { datita } from "../../utils/data";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { getTienda } from "../../api/tienda";
import ModalDescription from "../../components/Modals/ModalDescription";
import {
  LineaColores,
  ViewInfoDetalles,
} from "../../components/Comunes/ComunComponents";
import StatusBarCustom from "../../components/StatusBar";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { estilos_productos } from "../../styles/estilos";
import { StyleSheet } from "react-native";
import { SHARE_PRODUCT } from "../../api/rutas";
import { Linking } from "react-native";
import { getUserApi, setUserApi } from "../../api/storage";
import { ToastAndroid } from "react-native";
import { AddFavoriteUser, isFavoriteUser } from "../../api/favorite";

export default function Product(props) {
  const { route, navigation } = props;
  const { params } = route;
  const [product, setProduct] = useState(null);
  const [images, setImages] = useState([]);
  const [tienda, setTienda] = useState(null);
  const [usu, setUsu] = useState(false);
  const [favoritos, setFavoritos] = useState(false);

  useEffect(() => {
    (async () => {
      const arrayImages = [];
      const usuario = await getUserApi();
      if (usuario != null) {
        let user = JSON.parse(usuario);
        setUsu(JSON.parse(usuario));
        let data = {
          productos_id: params.idProduct,
          users_id: user.id,
        };
        let favorit = await isFavoriteUser(data);
        setFavoritos(favorit.result);
      }
      const response = await getProducto(params.idProduct);
      const id_tienda = response.data.id_tienda;
      const resp_store = await getTienda(id_tienda);
      setTienda(resp_store);
      const imagenes = await datita(params.idProduct);
      imagenes.forEach((element) => {
        arrayImages.push(element);
      });
      setProduct(response);
      setImages(response.foto);
    })();
  }, []);

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      {!product ? (
        <ScreenLoading text="Cargando producto" size="large" />
      ) : (
        <ScrollView>
          <ViewStoreShare
            tienda={tienda}
            product={product}
            usuario={usu}
            favoritos={favoritos}
            navigation={navigation}
          />
          <CarouselImages images={images} />
          <ViewPrecio
            precio={product.data.precio_productos}
            oferta={product.data.oferta_valor_productos}
            product={product}
            tienda={tienda}
            usuario={usu}
            navigation={navigation}
          />
          <ViewDetalles product={product} />
        </ScrollView>
      )}
    </>
  );
}

function ViewPrecio(props) {
  const { precio, oferta, product, tienda, usuario, navigation } = props;

  const buyByWhatsapp = (producto, tienda) => {
    let whatsapp = tienda.data.telefono_tienda;
    let nomb_tienda = producto.data.nombre_productos;
    let codigo = producto.data.codigo_productos;
    let share = SHARE_PRODUCT + codigo;
    let mensaje_whatsapp =
      "He visto este producto (" +
      share +
      " ) en tu tienda " +
      nomb_tienda +
      " en http://elperchero.store, me gustaría comprarlo ";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  const comprarLogueado = () => {
    ToastAndroid.show("Debe Loguearse para comprar", 1000);
    navigation.navigate("cuenta");
  };

  return (
    <View style={{ flexDirection: "row", marginTop: 10 }}>
      <View
        style={{
          width: "80%",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Text style={oferta ? styles.txt_precio_oferta : styles.txt_precio}>
          $ {precio}
        </Text>
        {oferta && <Text style={styles.txt_precio}>$ {oferta}</Text>}
      </View>
      <View style={{ width: "20%", paddingHorizontal: 8 }}>
        <TouchableOpacity
          style={styles.btn_comprar}
          onPress={() => {
            usuario ? buyByWhatsapp(product, tienda) : comprarLogueado();
          }}
        >
          <FontAwesome5
            name="shopping-cart"
            size={15}
            color={colores.color_blanco}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

function ViewDetalles(props) {
  const { product } = props;

  const { data, colores } = product;
  const {
    codigo_productos,
    marca_productos,
    condicion_productos,
    tallas,
    descripcion_productos,
  } = data;
  return (
    <>
      <List.Section>
        <Text style={{ fontSize: 12, marginLeft: 20, marginVertical: 8 }}>
          {descripcion_productos}
        </Text>
        <Divider />
        <ViewInfoDetalles titulo="Código" dato={codigo_productos} />
        <Divider />
        <ViewInfoDetalles titulo="Condición" dato={condicion_productos} />
        <Divider />

        <ViewInfoDetalles titulo="Talla(s)" dato={tallas} />
        <Divider />
        {colores.length > 0 && (
          <InfoColores titulo="Color(s)" colores_prod={colores} />
        )}

        <Divider />
        <ViewInfoDetalles titulo="Marca" dato={marca_productos} />
        <Divider />
      </List.Section>
    </>
  );
}

function ViewStoreShare(props) {
  const { tienda, product, usuario, favoritos, navigation } = props;
  const { nombre_tienda } = tienda.data;
  const { id_productos } = product.data;
  const { id } = usuario;
  const [stateFavorite, setStateFavorite] = useState(favoritos);
  const [colorFavorite, setColorFavorite] = useState(colores.secundario);

  useEffect(() => {
    if (favoritos) {
      setColorFavorite(colores.color_favorite);
    } else {
      setColorFavorite(colores.secundario);
    }
  }, []);

  const onShare = async (product) => {
    let share = SHARE_PRODUCT + product.data.codigo_productos;
    let mensaje =
      "He visto esta prenda en ElPerchero.store creo que te puede gustar ";
    try {
      await Share.share({ message: mensaje + share });
    } catch (error) {
      alert(error.message);
    }
  };

  const onfavorite = async (stado, id_productos, id) => {
    let data = {
      productos_id: id_productos,
      users_id: id,
    };
    let estadoFavorito = stado;
    if (estadoFavorito == false) {
      estadoFavorito = true;
      data.estado = estadoFavorito;
      setStateFavorite(estadoFavorito);
      setColorFavorite(colores.color_favorite);
      await AddFavoriteUser(data);
    } else {
      estadoFavorito = false;
      data.estado = estadoFavorito;
      setStateFavorite(estadoFavorito);
      setColorFavorite(colores.secundario);
      await AddFavoriteUser(data);
    }
  };

  const goStore = (tienda) => {
    let dato = tienda.data;
    navigation.navigate("option", {
      dato,
    });
  };

  return (
    <>
      <View style={estilos_productos.flashRow}>
        <TouchableOpacity
          style={estilos_productos.rowTag}
          onPress={() => goStore(tienda)}
        >
          <Text
            style={{
              fontSize: 14,
              color: colores.secundario,
              marginLeft: 10,
              fontWeight: "bold",
            }}
          >
            {nombre_tienda}
          </Text>
        </TouchableOpacity>

        <View style={estilos_productos.rowTag}>
          {usuario && (
            <TouchableOpacity
              style={{
                backgroundColor: colorFavorite,
                borderRadius: 3,
                width: 35,
                height: 35,
                justifyContent: "center",
                alignSelf: "center",
                marginHorizontal: 10,
              }}
              onPress={() => onfavorite(stateFavorite, id_productos, id)}
            >
              <FontAwesome
                name={"heart"}
                style={{
                  color: colores.color_blanco,
                  fontSize: 16,
                  textAlign: "center",
                }}
              />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={styles.view_share}
            onPress={() => onShare(product, id_productos, id)}
          >
            <FontAwesome
              name={"share-alt"}
              style={{
                color: "#fff",
                fontSize: 18,
                paddingHorizontal: 10,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

function DetallesProducto(props) {
  const { modalDescription, setModalDescription } = props;
  return (
    <>
      <ModalDescription
        modalDescription={modalDescription}
        setModalDescription={setModalDescription}
      />
      <View
        style={{
          backgroundColor: "#F2F2F2",
          padding: 5,
          marginBottom: 6,
          paddingVertical: 10,
          paddingLeft: 15,
        }}
      >
        <TouchableOpacity
          onPress={() => setModalDescription(!modalDescription)}
        >
          <View>
            <Text style={{ fontWeight: "bold" }}>Color y tallas 2</Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
}

function InfoColores(props) {
  const { colores_prod, titulo } = props;
  const list = () => {
    return colores_prod.map((element, key) => {
      return (
        <Text
          style={{
            backgroundColor: element.nombre_colores_productos,
            width: 25,
            height: 25,
            borderRadius: 25,
            marginRight: 5,
          }}
          key={key}
        ></Text>
      );
    });
  };
  return (
    <>
      {colores_prod ? (
        <View
          style={{
            marginHorizontal: 20,
            marginVertical: 10,
            flexDirection: "row",
          }}
        >
          <Text
            style={{
              width: "25%",
              fontSize: 12,
              textTransform: "uppercase",
              fontWeight: "bold",
            }}
          >
            {titulo}
          </Text>
          <View
            style={{
              width: "75%",
              marginLeft: 10,
              fontSize: 14,
              color: colores.terciario,
              textAlign: "justify",
              flexDirection: "row",
            }}
          >
            {list()}
          </View>
        </View>
      ) : (
        <Text>No tiene colores</Text>
      )}
    </>
  );
}

export const styles = StyleSheet.create({
  view_share: {
    backgroundColor: colores.secundario,
    borderRadius: 3,
    width: 35,
    height: 35,
    justifyContent: "center",
    alignSelf: "center",
    marginHorizontal: 10,
  },

  txt_precio: {
    color: colores.color_negro,
    fontSize: 22,
    /*  paddingVertical: 5, */
    marginLeft: 10,
    fontWeight: "bold",
  },
  txt_oferta: {
    color: colores.color_negro,
    fontSize: 22,
    marginLeft: 6,
  },
  txt_precio_oferta: {
    color: colores.secundario,
    fontSize: 12,
    marginLeft: 10,
    textDecorationLine: "line-through",
  },
  btn_comprar: {
    backgroundColor: colores.secundario,
    alignContent: "flex-end",
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginRight: 10,
    marginVertical: 5,
  },
});
