import React, { useEffect } from "react";
import { View, Text, RefreshControl, ScrollView } from "react-native";
import {
  LineaColores,
  TituloView,
} from "../../components/Comunes/ComunComponents";
import Banners from "../../components/Home/Banners";

import StatusBarCustom from "../../components/StatusBar";
import MenuCategories from "../Categories/MenuCategories";
import CarrouselStore from "../Store/CarrouselStore";
import ProductRecientes from "../Products/ProductRecientes";
import Permisos from "../../utils/permisos/Permisos";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import ListProducts from "../Products/ListProducts";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export default function Home(props) {
  const { navigation } = props;
  useEffect(() => {
    Permisos.localizacion();
  }, []);

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  return (
    <View>
      <StatusBarCustom />
      <CabeceraPrincipal navigation={navigation} />
      <LineaColores />
      <MenuCategories navigation={navigation} />
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {refreshing ? <Text></Text> : <CargarHome navigation={navigation} />}
      </ScrollView>
    </View>
  );
}

function CargarHome(props) {
  const { navigation } = props;
  return (
    <>
      <Banners />
      <TituloView txt_titulo="Productos recientes" />
      <ProductRecientes navigation={navigation} />
      <CarrouselStore navigation={navigation} />
      <View style={{ height: 120 }}></View>
    </>
  );
}
