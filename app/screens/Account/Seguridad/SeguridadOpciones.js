import React, { useCallback, useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";

import { List } from "react-native-paper";
import {
  DOC_CONSEJOS,
  DOC_TERMINOS_CONDICIONES,
  DOC_NORMATIVAS,
} from "../../../api/rutas";
import { IconosInfo } from "../../../components/Comunes/ComunComponents";
import colores from "../../../styles/colores";
import { getUserApi } from "../../../api/storage";

export default function SeguridadOpciones(props) {
  const { navigation } = props;
  const [usuario, setUsuario] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  useFocusEffect(
    useCallback(() => {
      (async () => {
        let datos = await getUserApi();
        setUsuario(JSON.parse(datos));
      })();
    }, [])
  );

  const changePass = (usuario) => {
    let id = usuario.id;
    console.log(id);
    navigation.navigate("user-edit-password", { id });
  };

  return (
    <>
      <List.Section>
        <List.Item
          title="Contraseña"
          description="Actualizar contraseña"
          left={() => (
            <IconosInfo
              name="key"
              color={colores.terciario}
              tipo="font-awesome-5"
              tam={20}
            />
          )}
          onPress={() => changePass(usuario)}
        />
        <List.Item
          title="Consejos de seguridad"
          description="Manten tu información segura"
          left={() => (
            <IconosInfo
              name="id-badge"
              color={colores.terciario}
              tipo="font-awesome"
              tam={25}
            />
          )}
          onPress={() => {
            let ruta = DOC_CONSEJOS;
            navigation.navigate("info-administracion", { ruta });
          }}
        />
        <List.Item
          title="Normativas"
          description="Consulta las normativas del uso de información de tu perfil"
          left={() => (
            <IconosInfo
              name="id-badge"
              color={colores.terciario}
              tipo="font-awesome"
              tam={25}
            />
          )}
          onPress={() => {
            let ruta = DOC_NORMATIVAS;
            navigation.navigate("info-administracion", { ruta });
          }}
        />

        <List.Item
          title="Términos y condiciones"
          description="La información de tu perfil es privada"
          left={() => (
            <IconosInfo
              name="id-badge"
              color={colores.terciario}
              tipo="font-awesome"
              tam={25}
            />
          )}
          onPress={() => {
            let ruta = DOC_TERMINOS_CONDICIONES;
            navigation.navigate("info-administracion", { ruta });
          }}
        />
      </List.Section>
    </>
  );
}
