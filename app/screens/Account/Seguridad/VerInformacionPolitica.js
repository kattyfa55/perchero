import React from "react";
import PDFReader from "rn-pdf-reader-js";
import { DOC_POLITICAS } from "../../../api/rutas";
export default function VerInformacionPoliticaVisualizador(props) {
  const { route } = props;
  const { ruta } = route.params;
  var url = ruta;
  return (
    <PDFReader source={{ uri: url }} withScroll={true} withPinchZoom={true} />
  );
}
