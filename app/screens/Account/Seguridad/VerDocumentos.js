import React from "react";
import { View } from "react-native";
import PDFReader from "rn-pdf-reader-js";
export default function VerDocumentos(props) {
  const { ruta } = props;

  return (
    <PDFReader source={{ uri: ruta }} withScroll={true} withPinchZoom={true} />
  );
}
