import React, { useCallback, useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { View, Text } from "react-native";
import { List } from "react-native-paper";
import {
  EsloganPerchero,
  LogoPerchero,
  LogoPerfilVendedor,
} from "../../../../components/Comunes/ComunComponents";
import colores from "../../../../styles/colores";
import { WebView } from "react-native-webview";
import { getUserApi } from "../../../../api/storage";

let user = "";
let rol = "";
export default function MisTiendas(props) {
  const { navigation } = props;
  const [usuario, setUsuario] = useState(null);

  useEffect(() => {
    (async () => {
      cargarData();
    })();
  }, []);

  const cargarData = async () => {
    (async () => {
      let datos = await getUserApi();
      user = JSON.parse(datos);
      Promise.all(datos);
      setUsuario(user);
      rol = user.rol_users;
    })();
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      cargarData();
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <>
      {user ? (
        rol == "Administrador_store" ? (
          <WebView
            incognito={true}
            source={{ uri: "http://elperchero.store/admin/misproductos" }}
          />
        ) : (
          <View>
            <LogoPerchero alto={150} />
            <LogoPerfilVendedor alto={150} />
            <EsloganPerchero alto={150} />
          </View>
        )
      ) : (
        <View>
          <Text></Text>
        </View>
      )}
    </>
  );

  /* <WebView source={{ uri: "http://elperchero.store/admin/misproductos" }} /> */
}
