import React, { useCallback, useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { View, Text } from "react-native";
import { List } from "react-native-paper";
import { IconosInfo } from "../../../../components/Comunes/ComunComponents";
import colores from "../../../../styles/colores";

export default function MisProductos(props) {
  const { navigation } = props;
  return (
    <>
      <List.Section>
        <List.Item
          title="Nuevo"
          description="Ingrese un nuevo producto"
          left={() => (
            <IconosInfo
              name="plus"
              color={colores.terciario}
              tipo="font-awesome-5"
              tam={20}
            />
          )}
          onPress={() => navigation.navigate("new-producto")}
        />
        <List.Item
          title="Listado"
          description="Ver sus productos"
          left={() => (
            <IconosInfo
              name="list"
              color={colores.terciario}
              tipo="font-awesome"
              tam={25}
            />
          )}
          onPress={() => navigation.navigate("listado-productos")}
        />
      </List.Section>
    </>
  );
}
