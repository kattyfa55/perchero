import React, { useState, useCallback, useFocusEffect, useEffect } from "react";
import { View, Text, ScrollView } from "react-native";
import Search from "../../components/Search/Search";
import StatusBarCustom from "../../components/StatusBar";
import Version from "../../components/Version";
import Menu from "../../components/Account/Menu";
import UserInfo from "../../components/Account/UserInfo";
import Informacion from "../../components/Informacion";
import { LineaColores } from "../../components/Comunes/ComunComponents";

export default function DatosCuenta() {
  return (
    <>
      <StatusBarCustom />
      <Search />
      <LineaColores />
      <ScrollView>
        <UserInfo user={user} />
        <Menu />
        <Informacion />
        <Version />
      </ScrollView>
    </>
  );
}
