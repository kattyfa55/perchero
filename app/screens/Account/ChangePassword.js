import React, { useState } from "react";
import { View, Text, ToastAndroid } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { useFormik } from "formik";
import * as Yup from "yup";

import colores from "../../styles/colores";
import { formStyles } from "../../styles/estilos";
import {
  EsloganPerchero,
  LogoPerchero,
} from "../../components/Comunes/ComunComponents";
import { ScrollView } from "react-native-gesture-handler";
import { actualizarPassword } from "../../api/user";

export default function ChangePassword(props) {
  const { navigation, route } = props;
  const { id } = route.params;

  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showNewPasswordRepeat, setShowNewPasswordRepeat] = useState(false);

  const formik = useFormik({
    initialValues: inicializarForm(id),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      try {
        setLoading(true);
        const response = await actualizarPassword(formData);
        if (response.result == true) {
          if (response.tipo == "password") {
            ToastAndroid.show(response.msj, 1000);
            navigation.goBack();
          }
        } else {
          if (response.tipo == "password") {
            ToastAndroid.show(response.msj, 1000);
          }
        }
        setLoading(false);
      } catch (error) {
        ToastAndroid.show("Ha ocurrido un error", 1000);
        setLoading(false);
      }
    },
  });

  return (
    <ScrollView>
      <View style={formStyles.view_titulo}>
        <LogoPerchero alto={180} />
        <View style={{ height: 30 }}></View>
        <TextInput
          label="Contraseña"
          style={formStyles.input_form}
          right={
            showPassword ? (
              <TextInput.Icon
                name="eye-off"
                color={colores.terciario}
                onPress={() => setShowPassword(!showPassword)}
              />
            ) : (
              <TextInput.Icon
                name="eye"
                color={colores.terciario}
                onPress={() => setShowPassword(!showPassword)}
              />
            )
          }
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("password", text)}
          value={formik.values.password}
          secureTextEntry={showPassword ? false : true}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <TextInput
          label="Nueva contraseña"
          style={formStyles.input_form}
          right={
            showNewPassword ? (
              <TextInput.Icon
                name="eye-off"
                color={colores.terciario}
                onPress={() => setShowNewPassword(!showNewPassword)}
              />
            ) : (
              <TextInput.Icon
                name="eye"
                color={colores.terciario}
                onPress={() => setShowNewPassword(!showNewPassword)}
              />
            )
          }
          mode="outlined"
          onChangeText={(text) => formik.setFieldValue("newPassword", text)}
          value={formik.values.newPassword}
          secureTextEntry={showNewPassword ? false : true}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <TextInput
          label="Confirmar nueva contraseña"
          style={formStyles.input_form}
          right={
            showNewPasswordRepeat ? (
              <TextInput.Icon
                name="eye-off"
                color={colores.terciario}
                onPress={() => setShowNewPasswordRepeat(!showNewPasswordRepeat)}
              />
            ) : (
              <TextInput.Icon
                name="eye"
                color={colores.terciario}
                onPress={() => setShowNewPasswordRepeat(!showNewPasswordRepeat)}
              />
            )
          }
          mode="outlined"
          onChangeText={(text) =>
            formik.setFieldValue("newPasswordRepeat", text)
          }
          value={formik.values.newPasswordRepeat}
          secureTextEntry={showNewPasswordRepeat ? false : true}
          theme={{
            colors: {
              primary: colores.secundario,
            },
          }}
        />
        <Button
          mode="contained"
          style={formStyles.btnStyleView}
          labelStyle={formStyles.btnStyleText}
          onPress={formik.handleSubmit}
          loading={loading}
        >
          Actualizar
        </Button>
        <EsloganPerchero alto={80} />
      </View>
    </ScrollView>
  );
}

function inicializarForm(id) {
  return {
    id: id,
    password: "",
    newPassword: "",
    newPasswordRepeat: "",
  };
}

function validationSchema() {
  return {
    password: Yup.string().required(true),
    newPassword: Yup.string().required(true),
    newPasswordRepeat: Yup.string()
      .required(true)
      .oneOf([Yup.ref("newPassword")], true),
  };
}
