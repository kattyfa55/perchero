import React, { useEffect, useState } from "react";
import { getUserApi } from "../../api/storage";
import Loading from "../../components/Comunes/Loading";
import UserInvitado from "./UserInvitado";
import UserLogueado from "./UserLogueado";

export default function Account(props) {
  const { navigation } = props;
  const [login, setLogin] = useState(null);
  const [user, setUser] = useState(null);
  useEffect(() => {
    (async () => {
      let datos = await getUserApi();
      setUser(JSON.parse(datos));
      !datos ? setLogin(false) : setLogin(true);
    })();
  }, []);

  if (login === null) return <Loading isVisible={true} text="Cargando..." />;

  return login ? (
    <UserLogueado
      setLogin={setLogin}
      user={user}
      setUser={setUser}
      navigation={navigation}
    />
  ) : (
    <UserInvitado setLogin={setLogin} />
  );
}
