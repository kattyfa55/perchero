import React, { useCallback, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import { View, Text, Image, RefreshControl } from "react-native";
import { Divider, List } from "react-native-paper";
import { Button } from "react-native-elements";
import { useFocusEffect } from "@react-navigation/native";

import { SERVIDOR } from "../../api/rutas";
import { ViewInfoUserAccount } from "../../components/Comunes/ComunComponents";
import { mi_perfil_styles, stylesCommentUser } from "../../styles/estilos";
import { getUserApi, setUserApi } from "../../api/storage";
import { actualizarStorage } from "../../api/user";
import colores from "../../styles/colores";
import UserInfo from "../../components/Account/UserInfo";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

export default function MiPerfil(props) {
  const { navigation } = props;
  const [usuario, setUsuario] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  useFocusEffect(
    useCallback(() => {
      (async () => {
        let datos = await getUserApi();
        setUsuario(JSON.parse(datos));
      })();
    }, [])
  );

  const actualizarDatosUser = () => {
    (async () => {
      let datos = await getUserApi();
      let user = JSON.parse(datos);
      let correo = user.email;
      let user_id = user.id;
      let formData = { email: correo, id: user_id };
      let getData = await actualizarStorage(formData);
      setUsuario(getData.data);
      await setUserApi(getData.data);
    })();
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
    actualizarDatosUser(usuario);
  }, []);

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      {refreshing ? (
        <Text></Text>
      ) : (
        <CargarPerfil
          usuario={usuario}
          setUsuario={setUsuario}
          actualizarDatosUser={actualizarDatosUser}
          navigation={navigation}
        />
      )}
    </ScrollView>
  );
}

function CargarPerfil(props) {
  const { usuario, actualizarDatosUser, navigation } = props;

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      (async () => {
        actualizarDatosUser(usuario);
      })();
    });
    return unsubscribe;
  }, []);

  return (
    <>
      {usuario && <ImagenPerfil user={usuario} />}
      {usuario && <UserInfo user={usuario} />}
      <Button
        title="Editar"
        buttonStyle={stylesCommentUser.btnAddReview}
        titleStyle={stylesCommentUser.btnTitleAddReview}
        icon={{
          type: "material-community",
          name: "square-edit-outline",
          color: colores.secundario,
        }}
        onPress={() =>
          navigation.navigate("update-datos-comprador", {
            usuario,
          })
        }
      />
      {usuario && <DatosPerfil user={usuario} />}
    </>
  );
}

function ImagenPerfil(props) {
  const { user } = props;
  const { path_users } = user;
  const user_img_defecto = `${SERVIDOR}` + "img/user.png";
  const user_img = `${SERVIDOR}` + path_users;
  return (
    <View style={mi_perfil_styles.view_perfil}>
      {path_users != "Ninguno" ? (
        <Image style={mi_perfil_styles.view_image} source={{ uri: user_img }} />
      ) : (
        <Image
          style={mi_perfil_styles.view_image}
          source={{ uri: user_img_defecto }}
        />
      )}
    </View>
  );
}

function DatosPerfil(props) {
  const { user } = props;

  const {
    dni_users,
    nacionalidad_users,
    pais_users,
    provincia_users,
    ciudad_users,
    sexo_users,
    fecha_nacimiento_users,
    whatsapp_users,
    email,
    pais,
    provincia,
    ciudad,
  } = user;

  return (
    <List.Section>
      <Divider />
      <ViewInfoUserAccount titulo="Nacionalidad" dato={nacionalidad_users} />
      <Divider />
      <ViewInfoUserAccount titulo="DNI" dato={dni_users} />
      <Divider />
      <ViewInfoUserAccount titulo="País" dato={pais} />
      <Divider />
      <ViewInfoUserAccount titulo="Provincia" dato={provincia} />
      <Divider />
      <ViewInfoUserAccount titulo="Ciudad" dato={ciudad} />
      <Divider />
      <ViewInfoUserAccount titulo="Género" dato={sexo_users} />
      <Divider />
      <ViewInfoUserAccount
        titulo="Fecha de Nacimiento"
        dato={fecha_nacimiento_users}
      />
      <Divider />
      <ViewInfoUserAccount titulo="Whatsapp" dato={whatsapp_users} />
      <Divider />
      <ViewInfoUserAccount titulo="Correo" dato={email} />
      <Divider />
    </List.Section>
  );
}
