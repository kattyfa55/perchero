import React, { useState } from "react";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Text,
  Image,
} from "react-native";
import { layoutStyle } from "../../styles";
import RegisterForm from "../../components/Auth/RegisterForm";
import LoginForm from "../../components/Auth/LoginForm";
import { LineaColores } from "../../components/Comunes/ComunComponents";

export default function Auth() {
  const [showLogin, setShowLogin] = useState(false);
  const [showMensaje, setShowMensaje] = useState(false);
  return (
    <>
      <LineaColores />
      <View style={layoutStyle.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
        >
          {showLogin ? (
            <LoginForm
              setShowLogin={setShowLogin}
              showMensaje={showMensaje}
              setShowMensaje={setShowMensaje}
            />
          ) : (
            <RegisterForm
              setShowLogin={setShowLogin}
              setShowMensaje={setShowMensaje}
            />
          )}
        </KeyboardAvoidingView>
      </View>
    </>
  );
}
