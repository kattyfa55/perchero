import React, { useState } from "react";
import { KeyboardAvoidingView } from "react-native";
import { View, Text } from "react-native";
import LoginForm from "../../components/Auth/LoginForm";
import RegisterComprador from "../../components/Auth/RegisterComprador";
import RegisterVendedor from "../../components/Auth/RegisterVendedor";
import StatusBarCustom from "../../components/StatusBar";
import { layoutStyle } from "../../styles";

export default function UserInvitado(props) {
  const { setLogin } = props;
  const [showLogin, setShowLogin] = useState(false);
  const [showRegisterVendedor, setShowRegisterVendedor] = useState(false);
  const [showRegisterComprador, setShowRegisterComprador] = useState(false);
  const [showMensaje, setShowMensaje] = useState(false);

  return (
    <>
      <StatusBarCustom />
      <View style={layoutStyle.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
        ></KeyboardAvoidingView>
        {showLogin ? (
          <>
            {showRegisterVendedor && (
              <RegisterVendedor
                setShowLogin={setShowLogin}
                setLogin={setLogin}
                setShowRegisterVendedor={setShowRegisterVendedor}
                setShowMensaje={setShowMensaje}
              />
            )}
            {showRegisterComprador && (
              <RegisterComprador
                setShowLogin={setShowLogin}
                setLogin={setLogin}
                setShowRegisterComprador={setShowRegisterComprador}
                setShowMensaje={setShowMensaje}
              />
            )}
          </>
        ) : (
          <LoginForm
            setShowLogin={setShowLogin}
            setLogin={setLogin}
            setShowRegisterComprador={setShowRegisterComprador}
            setShowRegisterVendedor={setShowRegisterVendedor}
            showMensaje={showMensaje}
          />
        )}
      </View>
    </>
  );
}
