import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, Image } from "react-native";
import Menu from "../../components/Account/Menu";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import Loading from "../../components/Comunes/Loading";

import Informacion from "../../components/Informacion";
import Search from "../../components/Search";
import StatusBarCustom from "../../components/StatusBar";
import Version from "../../components/Version";

export default function (props) {
  const { setLogin, user, navigation, setUser } = props;
  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <ScrollView>
        <Menu
          setLogin={setLogin}
          user={user}
          setUser={setUser}
          navigation={navigation}
        />
      </ScrollView>
    </>
  );
}
