import React, { useEffect } from "react";
import { useState } from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, View, Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { List } from "react-native-paper";
import { getTipoProducto } from "../../api/categorias";
import colores from "../../styles/colores";

export default function ViewCategorias(props) {
  const { subCategoria, navigation } = props;
  const { data } = subCategoria;

  return (
    <ScrollView>
      {data ? (
        <ListaCategorias data={data} navigation={navigation} />
      ) : (
        <View style={styles.view_principal_product}>
          <Text style={styles.txt_no_sub}>No hay Subcategorías</Text>
        </View>
      )}
    </ScrollView>
  );
}

function ListaCategorias(props) {
  const { data, navigation } = props;
  return (
    <>
      {data.map((element, key) => {
        return (
          <List.Accordion
            theme={{
              colors: {
                text: colores.secundario,
                primary: colores.terciario,
              },
            }}
            style={{ backgroundColor: colores.color_blanco }}
            key={key}
            title={element.nombre_subcategorias}
          >
            <View style={styles.view_principal_product}>
              <SubCategoria data={element} navigation={navigation} />
            </View>
          </List.Accordion>
        );
      })}
    </>
  );
}

function SubCategoria(props) {
  const { data, navigation } = props;
  const { id_subcategorias } = data;
  const [subCategoria, setSubCategoria] = useState();
  const irTipoProducto = (tipo) => {
    navigation.navigate("all-product-buscar", { tipo_prod: tipo });
  };

  useEffect(() => {
    (async () => {
      const sub_categ = await getTipoProducto(id_subcategorias);
      setSubCategoria(sub_categ);
    })();
  }, []);

  return (
    <>
      {subCategoria && (
        <TipoProducto
          subCategoria={subCategoria}
          irTipoProducto={irTipoProducto}
        />
      )}
    </>
  );
}

function TipoProducto(props) {
  const { subCategoria, irTipoProducto } = props;
  const { tam, data } = subCategoria;

  return (
    <>
      {tam > 0 ? (
        <>
          {data.map((element, key) => {
            return (
              <View key={key} style={styles.view_products}>
                <TouchableOpacity
                  onPress={() => irTipoProducto(element.id_tipo_productos)}
                  style={{
                    backgroundColor: "#FCFAFE",
                    height: 60,
                    justifyContent: "center",
                  }}
                >
                  <Text style={{ textAlign: "center", fontSize: 12 }}>
                    {element.nombre_tipo_productos}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </>
      ) : (
        <View style={{ width: "100%", alignItems: "center" }}>
          <Text
            style={{
              color: colores.secundario,
              paddingTop: 20,
              alignSelf: "center",
              height: 100,
              marginLeft: 20,
            }}
          >
            No hay Subcategorías
          </Text>
        </View>
      )}
    </>
  );
}

const porcentaje = 100 / 3 + "%";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    /*     flexWrap: "wrap",
    alignItems: "flex-start", */
    margin: -10,
    alignContent: "center",
  },
  containerProduct: {
    width: "35%",
    padding: 10,
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,
    padding: 10,
    width: porcentaje,
  },
  image: {
    height: 50,
    backgroundColor: "#FCFAFE",
    textAlign: "center",
  },
  name: {
    marginTop: 2,
    fontSize: 16,
    paddingLeft: 10,
    paddingVertical: 2,
  },

  view_products: {
    backgroundColor: colores.color_blanco,
    padding: 10,
    width: porcentaje,
    borderColor: colores.color_pri,
    textAlignVertical: "center",
  },
  view_principal_product: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
  },
  txt_desc_product: {
    alignSelf: "center",
    textAlign: "center",
    fontSize: 11,
  },
  txt_no_sub: {
    color: colores.secundario,
    paddingTop: 20,
    alignSelf: "center",
  },
});
