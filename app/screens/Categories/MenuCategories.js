import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { getAllCategorias } from "../../api/categorias";
import { categorias_styles } from "../../styles/estilos";

export default function MenuCategories(props) {
  const { navigation } = props;
  const [store, setStore] = useState(null);
  useEffect(() => {
    (async () => {
      const categorias = await getAllCategorias();
      if (categorias.result == false || categorias != null) {
        var array = categorias.data;
        var ofertas = { nombre_categorias: "Ofertas", id_categorias: 0 };
        var tiendas = { nombre_categorias: "Tiendas", id_categorias: -1 };
        array.push(ofertas);
        array.push(tiendas);
        setStore(categorias.data);
      }
    })();
  }, []);

  const goCategories = (id_categoria) => {
    if (id_categoria == -1) {
      navigation.navigate("store");
      return;
    }
    if (id_categoria == 0) {
      let tipo_prod = "ofertas";
      navigation.navigate("all-product-oferta", { tipo_prod });
      return;
    }
    navigation.navigate("all-product-buscar", { id_categoria });
  };

  return (
    <>
      <View style={{ marginVertical: 5 }}>
        <View style={{ marginLeft: 5 }}>
          <FlatList
            data={store}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={(categoria, key) => (
              <ButtonCategories data={categoria} goCategories={goCategories} />
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    </>
  );
}

function ButtonCategories(props) {
  const { data, goCategories } = props;
  const { id_categorias, nombre_categorias } = data.item;
  return (
    <TouchableOpacity onPress={() => goCategories(id_categorias)}>
      <View style={categorias_styles.view_categorias}>
        <View style={categorias_styles.content_categorias}>
          <Text style={categorias_styles.text_categoria}>
            {nombre_categorias}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
