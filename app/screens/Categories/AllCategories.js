import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Search from "../../components/Search/Search";
import StatusBarCustom from "../../components/StatusBar";
import colores from "../../styles/colores";
import { getAllCategorias, getSubCategories } from "../../api/categorias";
import ViewCategorias from "./ViewCategorias";
import { FlatList } from "react-native-gesture-handler";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";

export default function AllCategories(props) {
  const { navigation, route } = props;
  const { id_categoria } = route.params;
  const [isState, setState] = useState(true);
  const [categoria, setCategoria] = useState(id_categoria);
  const [subCategoria, setSubCategoria] = useState("");
  const [store, setStore] = useState(null);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      (async () => {
        cargarCategorias();
      })();
    });
    return unsubscribe;
  }, [navigation]);

  const cargarCategorias = async () => {
    const categorias = await getAllCategorias();
    if (categorias.result == false || categorias != null) {
      var array = categorias.data;
      var ofertas = { nombre_categorias: "Ofertas", id_categorias: 0 };
      var tiendas = { nombre_categorias: "Tiendas", id_categorias: -1 };
      array.push(ofertas);
      array.push(tiendas);
      setStore(categorias.data);
    }
  };

  const goCategories = async (id_categoria) => {
    if (id_categoria == -1) {
      navigation.navigate("store");
      return;
    }
    if (id_categoria == 0) {
      let tipo_prod = "ofertas";
      navigation.navigate("all-product-oferta", { tipo_prod });
      return;
    }
    setCategoria(id_categoria);
    const response = await getSubCategories(id_categoria);
    setSubCategoria(response);
    return;
  };

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <View style={styles_all_categorias.view_all}>
        <View style={styles_all_categorias.view_categoria}>
          <FlatList
            data={store}
            horizontal={false}
            showsVerticalScrollIndicator={false}
            renderItem={(categoria, key) => (
              <ButtonCategories data={categoria} irCategoria={goCategories} />
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <View style={styles_all_categorias.view_subcategoria}>
          {/*  {isState ? ( */}
          <ViewCategorias
            categoria={categoria}
            subCategoria={subCategoria}
            navigation={navigation}
          />
          {/*  ) : (
            <ViewCategorias
              categoria={categoria}
              subCategoria={subCategoria}
              navigation={navigation}
            />
          )} */}
        </View>
      </View>
    </>
  );
}

function ButtonCategories(props) {
  const { data, irCategoria } = props;
  const { id_categorias, nombre_categorias } = data.item;
  return (
    <TouchableOpacity onPress={() => irCategoria(id_categorias)}>
      <View style={styles_all_categorias.btn_categoria}>
        <View style={styles_all_categorias.btn_cat}>
          <Text style={styles_all_categorias.txt_cat}>{nombre_categorias}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles_all_categorias = StyleSheet.create({
  view_all: {
    backgroundColor: colores.secundario,
    flex: 1,
    flexDirection: "row",
    width: "100%",
  },
  view_categoria: { backgroundColor: colores.secundarioLight, width: "25%" },
  view_subcategoria: { backgroundColor: colores.color_blanco, width: "75%" },
  btn_categoria: {
    borderColor: colores.secundario,
    borderBottomWidth: 0.5,
    marginVertical: 1,
    marginHorizontal: 5,
  },
  btn_cat: { paddingVertical: 20 },
  txt_cat: {
    color: colores.primario,
    textAlign: "center",
    fontSize: 12,
  },
});
