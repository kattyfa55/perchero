import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from "@react-navigation/native";
import { Icon } from 'react-native-elements';
import { SLIDERWIDTH } from '../../utils/constants';
import { datos } from '../../utils/data';
import colores from '../../styles/colores';
import EncabezadoPortadas from '../../components/Comunes/ComunComponents';

export default function Categories() {
    const navigation = useNavigation();

    const goScreen = () => {        
        navigation.navigate("all-categorie");
      };
    const goCategories = (categoria) => {   
        (async () => {
            setCategoria(categoria);
            const response = await getCategories(categoria);
            setSubCategoria(response);
        })();
    };
    return (
        <>           
            <View>
                <EncabezadoPortadas 
                    titulo      =   'CATEGORÍAS'
                    textLink    =   'Ver todo'
                    color       =   {colores.color_cua} 
                    fontTam     =   {14}
                    goScreen    =   {goScreen}
                />
                <ScrollView 
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                >   
                    {datos.map((data,key) => 
                        <ButtonCategories 
                                name        = {data.name}
                                icon        = {data.icon}
                                tipo        = {data.tipo}
                                categoria   = {data.categoria}
                                irCategoria = {goCategories}
                                key         = {key}
                        />
                        )
                    }             
                </ScrollView>
            </View>        
        </>

    )
}



function ButtonCategories(props){
    const {name , icon, tipo ,categoria,irCategoria} = props;  
    return(
        <TouchableOpacity onPress={ ()=>irCategoria(categoria) }>
            <View style={{ width:SLIDERWIDTH/3,borderWidth:0.3,borderColor:colores.color_sec,height:100,
                justifyContent:'center'}}>  
                <View style={{paddingTop:10 }}>
                    <Icon name = {icon} type={tipo} color = {colores.color_sec} />
                    <Text style={{color:colores.color_sec,textAlign:'center' , fontSize:12 ,paddingTop:5}} > {name} </Text>    
                </View>        
            </View>
        </TouchableOpacity>
    )
}