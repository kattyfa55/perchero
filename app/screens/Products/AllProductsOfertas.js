import React, { useState, useEffect } from "react";
import { TouchableWithoutFeedback } from "react-native";
import { View, Text, Image, Picker, TextInput } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { SERVIDOR } from "../../api/rutas";
import { StoreDefecto } from "../../components/ComponentsDefecto";
import colores from "../../styles/colores";
import { getAllFitroTienda, getAllTienda } from "../../api/tienda";
import StatusBarCustom from "../../components/StatusBar";
import {
  ChangeUserStyles,
  stylesProductRecent,
  stylesSearch,
} from "../../styles/estilos";

import { Rating } from "react-native-elements";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import { getListaCityFiltro } from "../../api/lugares";
import { TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  getAllFitroProductos,
  getAllFitroProductosOfertas,
} from "../../api/productos";
import { getAllCategorias } from "../../api/categorias";
import { FontAwesome5 } from "@expo/vector-icons";

export default function AllProductsOfertas(props) {
  const { navigation, route } = props;
  const { tipo_prod } = route.params;
  const [store, setStore] = useState(null);
  const [ordenar, setOrdenar] = useState(0);
  const [ciudades, setCiudades] = useState(null);

  const [listaCategorias, setListaCategorias] = useState(0);
  const [categoria, setCategoria] = useState(0);

  const [city, setCity] = useState(0);

  const [buscar, setBuscar] = useState("");

  const [viewFiltro, setViewFiltro] = useState(false);
  const [usu, setUsu] = useState(false);

  useEffect(() => {
    (async () => {
      const productos = await getAllFitroProductosOfertas(
        formData(city, ordenar, buscar, categoria)
      );
      const resp_categoria = await getAllCategorias();
      const ciudad = await getListaCityFiltro();

      if (productos != null) {
        setStore(productos.data);
      }
      if (ciudad != null) {
        setCiudades(ciudad.data);
      }
      if (resp_categoria != null) {
        setListaCategorias(resp_categoria.data);
      }
    })();
  }, []);

  const list = () => {
    return store.map((element, key) => {
      return (
        <StoreList
          element={element}
          key={key}
          navigation={navigation}
          usu={usu}
        />
      );
    });
  };

  const setValueCategorias = async (item, index) => {
    setCategoria(item);
    let datita = await getAllFitroProductosOfertas(
      formData(city, ordenar, buscar, item)
    );
    setStore(datita.data);
  };



  const setValueOrdenar = async (item, index) => {
    setOrdenar(item);
    let datita = await getAllFitroProductosOfertas(
      formData(city, item, buscar, categoria)
    );
    setStore(datita.data);
  };

  const setValueCiudades = async (item, index) => {
    setCity(item);
    let datita = await await getAllFitroProductosOfertas(
      formData(item, ordenar, buscar, categoria)
    );
    setStore(datita.data);
  };

  const setValueBuscar = async (item, index) => {
    setBuscar(item);
    let datita = await await getAllFitroProductosOfertas(
      formData(city, ordenar, item, categoria)
    ); 
    setStore(datita.data);
  };

  const abrirFiltro = async (item) => {
    if (item == true) {
      setViewFiltro(false);
    } else {
      setViewFiltro(true);
    }
  };

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <View style={stylesSearch.search_view_store}>
        <View style={stylesSearch.search_view_busq}>
          <View style={{ width: "90%" }}>
            <View>
              <TextInput
                placeholder="Buscar"
                value={buscar}
                onChangeText={(query) => {
                  setValueBuscar(query);
                }}
                style={{ marginVertical: 6, marginLeft: 15 }}
              ></TextInput>
            </View>
          </View>
          <TouchableOpacity
            style={{ width: "10%", alignSelf: "center" }}
            onPress={() => abrirFiltro(viewFiltro)}
          >
            <MaterialCommunityIcons
              name="filter-menu"
              size={25}
              color={colores.secundario}
            />
          </TouchableOpacity>
        </View>
      </View>
      {viewFiltro && (
        <View style={{ width: "93%", alignSelf: "center" }}>
          <>
            <ComboSelectorCategorias
              categoria={categoria}
              categorias={listaCategorias}
              setValueCategorias={setValueCategorias}
            />
            <ComboSelectorCiudades
              city={city}
              ciudades={ciudades}
              setValueCiudades={setValueCiudades}
            />
            <ComboSelectorOrdenar
              ordenar={ordenar}
              setValueOrdenar={setValueOrdenar}
            />
          </>
        </View>
      )}
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colores.color_blanco }}
      >
        {store ? (
          <View style={stylesProductRecent.container}>{list()}</View>
        ) : (
          <StoreDefecto />
        )}
      </ScrollView>
    </>
  );
}

function formData(ciudad, ordenar, buscar, categoria) {
  return {
    ciudad    : ciudad,
    ordenar   : ordenar,
    buscar    : buscar,
    categoria : categoria,
  };
}

function ComboSelectorOrdenar(props) {
  const { ordenar, setValueOrdenar } = props;
  return (
    <View style={ChangeUserStyles.Ordenar_cmb}>
      <Picker
        style={ChangeUserStyles.PickerOrdenarStyles}
        selectedValue={ordenar}
        onValueChange={(itemValue, index) => setValueOrdenar(itemValue, index)}
      >
        <Picker.Item label="Ordenar por precio" value="0" />
        <Picker.Item label="Ascendente" value="ASC" />
        <Picker.Item label="Descendente" value="DESC" />
      </Picker>
    </View>
  );
}

function ComboSelectorCategorias(props) {
  const { categoria, categorias, setValueCategorias } = props;
  var items = () => {
    return categorias.map((element, key) => {
      return (
        <Picker.Item
          label={element.nombre_categorias}
          value={element.id_categorias}
          key={key}
        />
      );
    });
  };

  return (
    <View style={ChangeUserStyles.Ordenar_cmb}>
      <Picker
        style={ChangeUserStyles.PickerOrdenarStyles}
        selectedValue={categoria}
        onValueChange={(itemValue, index) =>
          setValueCategorias(itemValue, index)
        }
      >
        <Picker.Item label="Todas los productos" value={0} />
        {categorias && items()}
      </Picker>
    </View>
  );
}

function ComboSelectorCiudades(props) {
  const { city, ciudades, setValueCiudades } = props;
  var items = () => {
    return ciudades.map((element, key) => {
      return (
        <Picker.Item
          label={element.ciudades}
          value={element.id_ciudades}
          key={key}
        />
      );
    });
  };

  return (
    <View style={ChangeUserStyles.Ordenar_cmb}>
      <Picker
        style={ChangeUserStyles.PickerOrdenarStyles}
        selectedValue={city}
        onValueChange={(itemValue, index) => setValueCiudades(itemValue, index)}
      >
        <Picker.Item label="Todas las ciudades" value={0} />
        {ciudades && items()}
      </Picker>
    </View>
  );
}

function StoreList(props) {
  const { element, navigation, usu } = props;
  const {
    nombre_productos,
    precio_productos,
    nombre_tienda,
    ruta_foto,
    oferta_valor_productos,
  } = element;

  const goToProduct = (producto) => {
    navigation.navigate("product", { idProduct: producto.id_productos });
  };

  const comprarLogueado = () => {
    ToastAndroid.show("Debe Loguearse para Comprar", 1000);
    navigation.navigate("cuenta");
  };

  const buyByWhatsapp = (producto) => {
    let whatsapp = producto.telefono_tienda;
    let nomb_tienda = producto.nombre_tienda;
    let codigo = producto.codigo_productos;
    let share = SHARE_PRODUCT + codigo;
    let mensaje_whatsapp =
      "He visto este producto (" +
      share +
      " ) en tu tienda " +
      nomb_tienda +
      " en http://elperchero.store, me gustaría comprarlo ";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  return (
    <TouchableWithoutFeedback onPress={() => goToProduct(props.element)}>
      <View style={stylesProductRecent.containerProduct}>
        <View style={stylesProductRecent.view_productos_head}>
          <Text
            style={stylesProductRecent.name}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {nombre_productos}
          </Text>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={stylesProductRecent.txt_nombre_tienda}
          >
            {nombre_tienda}
          </Text>
        </View>
        <View style={stylesProductRecent.product}>
          <Image
            style={stylesProductRecent.image}
            source={{
              uri: SERVIDOR + ruta_foto,
            }}
          />
        </View>
        <View style={stylesProductRecent.view_footer}>
          <View
            style={{ width: "50%", marginVertical: 5, flexDirection: "row" }}
          >
            <Text
              style={
                oferta_valor_productos
                  ? stylesProductRecent.txt_precio_oferta
                  : stylesProductRecent.txt_precio
              }
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              $ {precio_productos}
            </Text>
            {oferta_valor_productos && (
              <Text style={stylesProductRecent.txt_oferta}>
                ${oferta_valor_productos}
              </Text>
            )}
          </View>
          <View
            style={{ width: "50%", marginVertical: 5, alignItems: "flex-end" }}
          >
            <TouchableOpacity
              style={stylesProductRecent.btn_cart}
              onPress={() => {
                usu ? buyByWhatsapp(element) : comprarLogueado();
              }}
            >
              <FontAwesome5
                name="shopping-cart"
                size={15}
                color={colores.color_blanco}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
