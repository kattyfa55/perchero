import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Linking,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import colores from "../../styles/colores";

export default function CarrouselProducts(props) {
  const { store, navigation } = props;
  const list = () => {
    return store.map((element, key) => {
      return <StoreList element={element} key={key} navigation={navigation} />;
    });
  };

  return (
    <>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        style={{ backgroundColor: colores.color_blanco }}
      >
        <View>
          <Text>hola</Text>
        </View>
        <View style={styles.container}>{store && <>{list()}</>}</View>
      </ScrollView>
    </>
  );
}
function StoreList(props) {
  const { element, navigation } = props;
  const { nombre_productos, precio_productos } = element;
  /* let ruta = URL_SERVIDOR + logo_tienda; */

  const goToProduct = (producto) => {
    navigation.navigate("product", { idProduct: producto.id_productos });
  };
  const buyByWhatsapp = (producto) => {
    let whatsapp = producto.telefono_tienda;
    let producto_nomb = producto.nombre_productos;

    let mensaje_whatsapp =
      "He visto este producto (" +
      producto_nomb +
      " ) en tu tienda en www.perchero.com" +
      " me gustaría comprarlo https://a.aliexpress.com/_msS7JAl";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  return (
    <TouchableWithoutFeedback
      /*  key={id_tienda} */
      onPress={() => goToProduct(props.element)}
    >
      <View style={styles.containerProduct}>
        <View
          style={{
            backgroundColor: colores.secundario,
            borderBottomWidth: 5,
            borderColor: colores.primario,
          }}
        >
          <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
            {nombre_productos}
          </Text>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              color: colores.primario,
              alignSelf: "center",
              marginBottom: 2,
            }}
          >
            Chami Store
          </Text>
        </View>

        <View style={styles.product}>
          <Image
            style={styles.image}
            source={{
              uri: "http://192.168.40.15/img/productos/camisa1.png",
            }}
          />
        </View>
        <View
          style={{
            backgroundColor: colores.terciario,
            width: "100%",
            flexDirection: "row",
          }}
        ></View>
        <View
          style={{
            backgroundColor: colores.terciario,
            width: "100%",
            flexDirection: "row",
          }}
        >
          <View style={{ width: "50%" }}>
            <Text
              style={{
                color: colores.color_blanco,
                fontSize: 20,
                padding: 5,
              }}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              $ {precio_productos}
            </Text>
          </View>
          <View style={{ width: "50%", padding: 5 }}>
            <TouchableOpacity
              style={{
                backgroundColor: colores.secundario,
                paddingVertical: 5,
                borderRadius: 5,
              }}
              onPress={() => buyByWhatsapp(element)}
            >
              <Text
                style={{
                  color: colores.color_blanco,
                  fontWeight: "bold",
                  alignSelf: "center",
                }}
              >
                Comprar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "center",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,

    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },
  image: {
    height: 180,
    resizeMode: "cover",
  },
  name: {
    marginVertical: 3,
    fontSize: 16,
    color: colores.color_blanco,
    alignSelf: "center",
  },
});
