import React, { useState, useEffect } from "react";
import { TouchableOpacity } from "react-native";
import { TouchableWithoutFeedback } from "react-native";
import { Linking } from "react-native";
import { View, Text, Image } from "react-native";
import { FlatList, ScrollView } from "react-native-gesture-handler";
import { getProductsRecent } from "../../api/productos";
import { SERVIDOR, SHARE_PRODUCT } from "../../api/rutas";
import { ProductoDefecto } from "../../components/ComponentsDefecto";
import colores from "../../styles/colores";
import { FontAwesome5 } from "@expo/vector-icons";
import { stylesProductRecent } from "../../styles/estilos";
import { ToastAndroid } from "react-native";
import { getUserApi } from "../../api/storage";
import { SafeAreaView } from "react-native-safe-area-context";

export default function ProductByStore(props) {
  const { navigation } = props;
  const [store, setStore] = useState(null);
  const [usu, setUsu] = useState(false);
  useEffect(() => {
    (async () => {
      const response = await getProductsRecent();
      const usuario = await getUserApi();
      setUsu(JSON.parse(usuario));
      if (response != null) {
        setStore(response);
      }
    })();
  }, []);
  return (
    <>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colores.color_blanco }}
      >
        <FlatList
          data={store}
          keyExtractor={(item) => item.id_productos + ""}
          initialNumToRender={6}
          numColumns={2}
          ListEmptyComponent={ProductoDefecto}
          renderItem={({ item }) => (
            <StoreList element={item} navigation={navigation} usu={usu} />
          )}
        ></FlatList>
      </ScrollView>
    </>
  );
}

function StoreList(props) {
  const { element, navigation, usu } = props;
  const {
    nombre_productos,
    precio_productos,
    nombre_tienda,
    ruta_foto,
    oferta_valor_productos,
  } = element;

  const goToProduct = (producto) => {
    navigation.navigate("product", { idProduct: producto.id_productos });
  };

  const comprarLogueado = () => {
    ToastAndroid.show("Debe Loguearse para Comprar", 1000);
    navigation.navigate("cuenta");
  };

  const buyByWhatsapp = (producto) => {
    let whatsapp = producto.telefono_tienda;
    let nomb_tienda = producto.nombre_tienda;
    let codigo = producto.codigo_productos;
    let share = SHARE_PRODUCT + codigo;
    let mensaje_whatsapp =
      "He visto este producto (" +
      share +
      " ) en tu tienda " +
      nomb_tienda +
      " en http://elperchero.store, me gustaría comprarlo ";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  return (
    <TouchableWithoutFeedback onPress={() => goToProduct(props.element)}>
      <View style={stylesProductRecent.containerProduct}>
        <View style={stylesProductRecent.view_productos_head}>
          <Text
            style={stylesProductRecent.name}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {nombre_productos}
          </Text>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={stylesProductRecent.txt_nombre_tienda}
          >
            {nombre_tienda}
          </Text>
        </View>
        <View style={stylesProductRecent.product}>
          <Image
            style={stylesProductRecent.image}
            source={{
              uri: SERVIDOR + ruta_foto,
            }}
          />
        </View>
        <View style={stylesProductRecent.view_footer}>
          <View
            style={{ width: "50%", marginVertical: 5, flexDirection: "row" }}
          >
            <Text
              style={
                oferta_valor_productos
                  ? stylesProductRecent.txt_precio_oferta
                  : stylesProductRecent.txt_precio
              }
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              $ {precio_productos}
            </Text>
            {oferta_valor_productos && (
              <Text style={stylesProductRecent.txt_oferta}>
                ${oferta_valor_productos}
              </Text>
            )}
          </View>
          <View
            style={{ width: "50%", marginVertical: 5, alignItems: "flex-end" }}
          >
            <TouchableOpacity
              style={stylesProductRecent.btn_cart}
              onPress={() => {
                usu ? buyByWhatsapp(element) : comprarLogueado();
              }}
            >
              <FontAwesome5
                name="shopping-cart"
                size={15}
                color={colores.color_blanco}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
