import React from 'react'
import { View, Text } from 'react-native'
import colores from '../../styles/colores';

export default function ViewProducts() {
    return (
        <View>
            <Text>VISTA DE PRODUCTOS</Text>
            <ViewFoto 
                ruta    = "http://192.168.40.15/img/subcategorias/chaqueta.png"
            />
            <View style={{backgroundColor:colores.color_cua,paddingVertical:10,paddingLeft:20}}>
                <Text>$34.00</Text>
            </View>
            <View style={{backgroundColor:colores.color_pri,paddingVertical:10,paddingLeft:20}}>
                <Text>Ver Carrito</Text>
            </View>            
        </View>
    )
}


function ViewFoto(props){
    const {ruta} = props;  
    return(
        <View style={{height:120,backgroundColor:colores.color_sec}}>
            <Text> {ruta} </Text>
        </View>
    )
}