import React, { useEffect, useState } from "react";
import { View, Text, ScrollView } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Icon } from "react-native-elements";
import { SLIDERWIDTH } from "../../utils/constants";
import colores from "../../styles/colores";
import EncabezadoPortadas from "../../components/Comunes/ComunComponents";
import { getAllTienda } from "../../api/tienda";
import { Image } from "react-native";
import { SERVIDOR } from "../../api/rutas";
import { StoreDefecto } from "../../components/ComponentsDefecto";
import { stylesLocales } from "../../styles/estilos";

export default function CarrouselStore(props) {
  const { navigation } = props;
  const [store, setStore] = useState(null);
  const [stateStore, setStateStore] = useState(false);

  useEffect(() => {
    (async () => {
      const response = await getAllTienda();
      if (response != null) {
        setStateStore(true);
        setStore(response);
      } else {
        setStateStore(false);
      }
    })();
  }, []);

  const list = () => {
    return store.map((element, key) => {
      return <ViewStore dato={element} key={key} navigation={navigation} />;
    });
  };

  const goStoreAll = () => {
    navigation.navigate("store");
  };

  return (
    <>
      <EncabezadoPortadas
        titulo="Tiendas"
        textLink=""
        color={colores.color_cua}
        fontTam={17}
        goScreen={goStoreAll}
      />
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={stateStore}
        style={{ backgroundColor: colores.color_blanco }}
      >
        {store ? (
          <View style={stylesLocales.container}>{list()}</View>
        ) : (
          <StoreDefecto />
        )}
      </ScrollView>
    </>
  );
}

function ViewStore(props) {
  const { dato, navigation } = props;
  const { id_tienda, logo_tienda } = dato;
  const goStoreId = () => {
    navigation.navigate("option", {
      dato,
    });
  };

  return (
    <>
      <TouchableOpacity onPress={() => goStoreId()}>
        <View
          style={{
            width: SLIDERWIDTH / 3,
            borderColor: colores.color_sec,
            height: 150,
            justifyContent: "center",
          }}
        >
          <View key={id_tienda} style={{ margin: 10, alignItems: "center" }}>
            <Image
              style={{ height: 120, width: 120, margin: 10 }}
              resizeMode="contain"
              source={{ uri: SERVIDOR + logo_tienda }}
            />
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
}

function ButtonCategories(props) {
  const { name, icon, tipo, categoria, irCategoria } = props;
  return (
    <TouchableOpacity onPress={() => irCategoria(categoria)}>
      <View
        style={{
          width: SLIDERWIDTH / 3,
          borderWidth: 0.3,
          borderColor: colores.color_sec,
          height: 100,
          justifyContent: "center",
        }}
      >
        <View style={{ paddingTop: 10 }}>
          <Icon name={icon} type={tipo} color={colores.color_sec} />
          <Text
            style={{
              color: colores.color_sec,
              textAlign: "center",
              fontSize: 12,
              paddingTop: 5,
            }}
          >
            {name}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
