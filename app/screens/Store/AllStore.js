import React, { useState, useEffect } from "react";
import { TouchableWithoutFeedback } from "react-native";
import { View, Text, StyleSheet, Image, Picker, TextInput } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { SERVIDOR } from "../../api/rutas";
import { StoreDefecto } from "../../components/ComponentsDefecto";
import colores from "../../styles/colores";
import { getAllFitroTienda, getAllTienda } from "../../api/tienda";
import StatusBarCustom from "../../components/StatusBar";
import {
  ChangeUserStyles,
  stylesProductRecent,
  stylesSearch,
} from "../../styles/estilos";

import { Rating } from "react-native-elements";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import { getListaCityFiltro } from "../../api/lugares";
import { TouchableOpacity } from "react-native";
import { MaterialCommunityIcons, Ionicons } from "@expo/vector-icons";

export default function AllStore(props) {
  const { navigation } = props;
  const [store, setStore] = useState(null);
  const [ordenar, setOrdenar] = useState(0);
  const [ciudades, setCiudades] = useState(null);
  const [city, setCity] = useState(0);
  const [buscar, setBuscar] = useState("");

  const [viewFiltro, setViewFiltro] = useState(false);

  useEffect(() => {
    (async () => {
      const response = await getAllTienda();
      const ciudad = await getListaCityFiltro();
      if (response != null) {
        setStore(response);
      }
      if (ciudad != null) {
        setCiudades(ciudad.data);
      }
    })();
  }, []);

  const list = () => {
    return store.map((element, key) => {
      return <StoreList dato={element} key={key} navigation={navigation} />;
    });
  };

  const setValueOrdenar = async (item, index) => {
    setOrdenar(item);
    let datita = await getAllFitroTienda(formData(city, item, buscar));
    setStore(datita.data);
  };

  const setValueCiudades = async (item, index) => {
    setCity(item);
    let datita = await getAllFitroTienda(formData(item, ordenar, buscar));
    setStore(datita.data);
  };

  const setValueBuscar = async (item, index) => {
    setBuscar(item);
    let datita = await getAllFitroTienda(formData(city, ordenar, item));
    setStore(datita.data);
  };

  const abrirFiltro = async (item) => {
    if (item == true) {
      setViewFiltro(false);
    } else {
      setViewFiltro(true);
    }
  };

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <View style={stylesSearch.search_view_store}>
        <View style={stylesSearch.search_view_busq}>
          <View style={{ width: "90%", backgroundColor: colores.color_blanco }}>
            <View>
              <TextInput
                placeholder="Buscar"
                value={buscar}
                onChangeText={(query) => {
                  setValueBuscar(query);
                }}
                style={{
                  marginVertical: 6,
                  marginLeft: 15,
                  backgroundColor: colores.color_blanco,
                }}
              ></TextInput>
            </View>
          </View>
          <TouchableOpacity
            style={{ width: "10%", alignSelf: "center" }}
            onPress={() => abrirFiltro(viewFiltro)}
          >
            <MaterialCommunityIcons
              name="filter-menu"
              size={25}
              color={colores.secundario}
            />
          </TouchableOpacity>
        </View>
      </View>
      {viewFiltro && (
        <View
          style={{
            width: "100%",
            alignSelf: "center",
            backgroundColor: colores.color_blanco,
          }}
        >
          <View
            style={{
              width: "93%",
              alignSelf: "center",
              backgroundColor: colores.color_blanco,
            }}
          >
            <>
              <ComboSelectorCiudades
                city={city}
                ciudades={ciudades}
                setValueCiudades={setValueCiudades}
              />
              <ComboSelectorOrdenar
                ordenar={ordenar}
                setValueOrdenar={setValueOrdenar}
              />
            </>
          </View>
        </View>
      )}
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colores.color_blanco }}
      >
        {store ? (
          <View style={stylesProductRecent.container}>{list()}</View>
        ) : (
          <StoreDefecto />
        )}
      </ScrollView>
    </>
  );
}

function formData(ciudad, calificaciones, buscar) {
  return {
    ciudad: ciudad,
    calificaciones: calificaciones,
    buscar: buscar,
  };
}

function ComboSelectorOrdenar(props) {
  const { ordenar, setValueOrdenar } = props;
  return (
    <View style={ChangeUserStyles.Ordenar_cmb}>
      <Picker
        style={ChangeUserStyles.PickerOrdenarStyles}
        selectedValue={ordenar}
        onValueChange={(itemValue, index) => setValueOrdenar(itemValue, index)}
      >
        <Picker.Item label="Ordenar por puntuación" value="0" />
        <Picker.Item label="Ascendente" value="ASC" />
        <Picker.Item label="Descendente" value="DESC" />
      </Picker>
    </View>
  );
}

function ComboSelectorCiudades(props) {
  const { city, ciudades, setValueCiudades } = props;
  var items = () => {
    return ciudades.map((element, key) => {
      return (
        <Picker.Item
          label={element.ciudades}
          value={element.id_ciudades}
          key={key}
        />
      );
    });
  };

  return (
    <View style={ChangeUserStyles.Ordenar_cmb}>
      <Picker
        style={ChangeUserStyles.PickerOrdenarStyles}
        selectedValue={city}
        onValueChange={(itemValue, index) => setValueCiudades(itemValue, index)}
      >
        <Picker.Item label="Todas las ciudades" value={0} />
        {ciudades && items()}
      </Picker>
    </View>
  );
}

function StoreList(props) {
  const { dato, navigation } = props;
  const { logo_tienda, nombre_tienda, id_tienda, calificaciones } = dato;

  const goStoreId = () => {
    navigation.navigate("option", {
      dato,
    });
  };

  return (
    <TouchableWithoutFeedback onPress={() => goStoreId(props.dato)}>
      <View style={stylesProductRecent.containerProduct}>
        <View style={stylesSearch.view_productos_head}>
          <Text
            style={stylesSearch.name}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {nombre_tienda}
          </Text>
          <Calificaciones calificacion={calificaciones} />
        </View>
        <View style={stylesProductRecent.store_border}>
          <Image
            style={stylesSearch.imageStore}
            source={{
              uri: SERVIDOR + logo_tienda,
            }}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

function Calificaciones(props) {
  const { calificacion } = props;
  return (
    <View
      style={{
        alignItems: "center",
        marginBottom: 4,
        backgroundColor: colores.secundario,
      }}
    >
      <Rating
        type="custom"
        imageSize={16}
        startingValue={calificacion}
        readonly
        tintColor={colores.secundario}
      />
    </View>
  );
}
