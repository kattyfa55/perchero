import React from "react";
import { Linking, StyleSheet, View, Text, Dimensions } from "react-native";

import { List } from "react-native-paper";
import { size } from "lodash";

export default function Store(props) {
  const { route } = props;
  const { dato } = route.params;

  return (
    <>
      <StoreInfo datos={dato} />
    </>
  );
}

function StoreInfo(props) {
  const { datos } = props;
  const {
    logo_tienda,
    nombre_tienda,
    nombre_dueño_tienda,
    url_facebook_tienda,
    url_instagram_tienda,
    telefono_tienda,
    descripcion_tienda,
  } = datos;

  return (
    <View style={styles.viewRestaurantInfo}>
      <Text style={styles.restaurantInfoTitle}>
        Información sobre la tienda
      </Text>
      <StoreListInfo
        logo={logo_tienda}
        tienda={nombre_tienda}
        propietario={nombre_dueño_tienda}
        whatsapp={telefono_tienda}
        facebook={url_facebook_tienda}
        instagram={url_instagram_tienda}
        descripcion={descripcion_tienda}
      />
    </View>
  );
}

function StoreListInfo(props) {
  const {
    logo,
    tienda,
    descripcion,
    propietario,
    facebook,
    instagram,
    whatsapp,
  } = props;
  const valordecadenafacebook = 25;
  const valordecadenainstagram = 26;
  const limitfacebook = size(facebook);
  const limitinstagram = size(instagram);
  const url_instagram =
    "@" + instagram.substr(valordecadenainstagram, limitinstagram);
  const url_facebook =
    "@" + facebook.substr(valordecadenafacebook, limitfacebook);

  const irWhatsapp = () => {
    let mensaje_whatsapp =
      "ME HA INTERESADO LOS PRODUCTO DE SU TIENDA, ME AYUDARIA CON MAS INFORMACIÓN";
    Linking.openURL(
      "http://api.whatsapp.com/send?phone=593" +
        whatsapp +
        "&text=" +
        mensaje_whatsapp
    );
  };

  return (
    <>
      <List.Section>
        <List.Item
          title={tienda}
          left={(props) => <List.Icon {...props} icon="store" />}
        />
        <List.Item
          title="Propietario"
          description={propietario}
          left={(props) => <List.Icon {...props} icon="account-check" />}
        />
        <List.Item
          title="Whassapp"
          description={whatsapp}
          left={(props) => <List.Icon {...props} icon="whatsapp" />}
          onPress={irWhatsapp}
        />
        <List.Item
          title="Facebook"
          description={url_facebook}
          left={(props) => <List.Icon {...props} icon="facebook" />}
          onPress={() => Linking.openURL(facebook)}
        />
        <List.Item
          title="Instagram"
          description={url_instagram}
          left={(props) => <List.Icon {...props} icon="instagram" />}
          onPress={() => Linking.openURL(instagram)}
        />
      </List.Section>
    </>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#fff",
  },
  viewRestaurantTitle: {
    padding: 15,
  },
  nameRestaurant: {
    fontSize: 20,
    fontWeight: "bold",
  },
  descriptionRestaurant: {
    marginTop: 5,
    color: "grey",
  },
  rating: {
    position: "absolute",
    right: 0,
  },
  viewRestaurantInfo: {
    margin: 15,
    marginTop: 25,
  },
  restaurantInfoTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  containerListItem: {
    borderBottomColor: "#d8d8d8",
    borderBottomWidth: 1,
  },
  viewFavorite: {
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 2,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 100,
    padding: 5,
    paddingLeft: 15,
  },
});
