import React, { useEffect } from "react";
import { Dimensions, StyleSheet } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import CabeceraPrincipal from "../../components/CabeceraPrincipal";
import { LineaColores } from "../../components/Comunes/ComunComponents";
import StatusBarCustom from "../../components/StatusBar";
import colores from "../../styles/colores";
import StoreComment from "./Tabs/StoreComment";
import StoreInfo from "./Tabs/StoreInfo";
import StoreProduct from "./Tabs/StoreProduct";

export default function StoreOptions(props) {
  const { route, navigation } = props;
  const { dato } = route.params;
  const { id_tienda } = dato;

  const [index, setIndex] = React.useState(0);
  const initialLayout = { width: Dimensions.get("window").width };

  const [routes] = React.useState([
    { key: "first", title: "Tienda" },
    { key: "second", title: "Productos" },
    { key: "third", title: "Comentarios" },
  ]);

  const renderScene = SceneMap({
    first: () => <StoreInfo dato={dato} navigation={navigation} />,
    second: () => <StoreProduct dato={id_tienda} navigation={navigation} />,
    third: () => <StoreComment navigation={navigation} dato={dato} />,
  });

  return (
    <>
      <StatusBarCustom />
      <CabeceraPrincipal />
      <LineaColores />
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        renderTabBar={(props) => (
          <TabBar
            {...props}
            activeColor={colores.primario}
            inactiveColor={colores.secundario}
            indicatorStyle={{ backgroundColor: colores.primario }}
            style={{
              backgroundColor: colores.color_blanco,
            }}
            labelStyle={{ fontSize: 11 }}
          />
        )}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {},
  scene: {
    flex: 1,
  },
});
