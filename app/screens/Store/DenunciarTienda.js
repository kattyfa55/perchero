import React, { useEffect, useState } from "react";
import { ScrollView, View, Text } from "react-native";
import { useFormik } from "formik";
import * as Yup from "yup";

import { List, TextInput, Button } from "react-native-paper";

import colores from "../../styles/colores";
import { formStyles } from "../../styles/estilos";
import { addDenuncia } from "../../api/denuncias";
import { getUserApi } from "../../api/storage";
import Toast from "react-native-root-toast";
import { ToastAndroid } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function DenunciarTienda(props) {
  const { route } = props;
  const { id_tienda } = route.params;
  const [expanded, setExpanded] = React.useState(true);
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState();
  const [descripcion, setDescripcion] = useState();
  const [datos, setDatos] = useState();
  const navigation = useNavigation();
  const handlePress = () => setExpanded(!expanded);

  useEffect(() => {
    (async () => {
      const userLogin = await getUserApi();
      setUser(userLogin);
    })();
  }, [user]);

  let data = [
    {
      titulo: "Ventas no autorizada",
      motivo:
        "Nuestras normas explican lo que permitimos y no permitimos en El Perchero. Revisamos y actualizamos estas normas periódicamente.",
    },
    {
      titulo: "Información falsa",
      motivo:
        "Existen tiendas o productos con información falsa o suplantada que infringe la comercialización de los productos existentes.",
    },
    {
      titulo: "Spam",
      motivo:
        "No permitimos cosas como:. \n ‣ Comprar, vender o regalar cuentas, roles o permisos. \n ‣ Animar a las personas a interactuar con contenido con falsos pretextos. \n ‣ Desviar a las personas de El Perchero a través del uso engañoso de enlaces.",
    },
  ];

  const listDenunciar = () => {
    return data.map((element, key) => {
      return (
        <List.Accordion key={key} title={element.titulo}>
          <Text
            style={{
              color: colores.terciario,
              margin: 15,
            }}
          >
            {element.motivo}
          </Text>
        </List.Accordion>
      );
    });
  };

  const saveDenunciar = async () => {
    let userJson = JSON.parse(user);
    let id_user = userJson.id;
    let tipo = "Otro motivo";
    let formData = getData({ descripcion, tipo, id_tienda, id_user });
    await addDenuncia(formData).then((result) => {
      if (result.result == true) {
        ToastAndroid.show("Se ha enviado la denuncia a revisión", 1000);
        navigation.goBack("store");
      } else {
        ToastAndroid.show("Ha ocurrido un error,", 1000);
      }
    });
  };

  return (
    <ScrollView
      style={{
        backgroundColor: colores.color_blanco,
        flex: 1,
      }}
    >
      {listDenunciar()}
      <List.Accordion title="Otros motivos">
        <Text
          style={{
            color: colores.terciario,
            margin: 15,
          }}
        >
          Especificar el motivo:
        </Text>

        <View style={formStyles.view_motivo}>
          <TextInput
            label="Motivo"
            style={formStyles.input_motivo}
            mode="outlined"
            multiline={true}
            selectionColor={colores.color_sec}
            value={descripcion}
            onChangeText={(text) => setDescripcion(text)}
          />
          <Button
            mode="contained"
            style={formStyles.btnSucces}
            labelStyle={formStyles.btnTextLabelLogin}
            onPress={saveDenunciar}
          >
            Enviar
          </Button>
        </View>
      </List.Accordion>
      <View style={{ height: 50 }}></View>
    </ScrollView>
  );
}

function getData(props) {
  const { descripcion, tipo, id_tienda, id_user } = props;
  return {
    descripcion: descripcion,
    tipo: tipo,
    tienda: id_tienda,
    usuario: id_user,
  };
}
