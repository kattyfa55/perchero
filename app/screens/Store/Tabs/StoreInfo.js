import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Divider, List } from "react-native-paper";
import { SERVIDOR, URL_SERVIDOR } from "../../../api/rutas";
import {
  CarrouselTiendas,
  LogoUri,
} from "../../../components/Comunes/ComunComponents";
import { ScrollView } from "react-native-gesture-handler";
import { Rating } from "react-native-elements";
import { Icon } from "react-native-elements";
import colores from "../../../styles/colores";
import { TouchableOpacity } from "react-native";

export default function StoreInfo(props) {
  const { dato, navigation } = props;
  const { calificaciones } = dato;
  let ruta = SERVIDOR + dato.logo_tienda;
  return (
    <>
      <ScrollView>
        <CarrouselTiendas ruta={ruta} />
        <Calificaciones calificacion={calificaciones} />
        <Informacion datos={dato} navigation={navigation} />
      </ScrollView>
    </>
  );
}

function Informacion(props) {
  const { datos, navigation } = props;
  const {
    nombre_tienda,
    dueno_tienda,
    descripcion_tienda,
    ciudad_nombre,
    provincia_nombre,
    ubicacion_latitud_tienda,
    ubicacion_longitud_tienda,
    direccion_tienda,
  } = datos;
  const goUbicacion = (latitud, longitud) => {
    navigation.navigate("ver-mapa", { latitud, longitud });
  };

  return (
    <View style={styles.viewRestaurantInfo}>
      <StoreListInfo
        tienda={nombre_tienda}
        propietario={dueno_tienda}
        descripcion={descripcion_tienda}
        ciudad={ciudad_nombre}
        provincia={provincia_nombre}
        direccion_tienda={direccion_tienda}
        ubicacion_latitud_tienda={ubicacion_latitud_tienda}
        ubicacion_longitud_tienda={ubicacion_longitud_tienda}
        goUbicacion={goUbicacion}
      />
    </View>
  );
}

function Calificaciones(props) {
  const { calificacion } = props;

  return (
    <View style={{ alignItems: "center", marginTop: 20 }}>
      <Rating imageSize={45} startingValue={calificacion} readonly />
    </View>
  );
}

function StoreListInfo(props) {
  const {
    tienda,
    descripcion,
    propietario,
    ciudad,
    provincia,
    direccion_tienda,
    ubicacion_longitud_tienda,
    ubicacion_latitud_tienda,
    goUbicacion,
  } = props;

  return (
    <>
      <List.Section>
        <Divider />
        <List.Item
          title={tienda}
          description={descripcion}
          left={(props) => <List.Icon {...props} icon="store" />}
        />
        <Divider />

        <List.Item
          title="Propietario"
          description={propietario}
          left={(props) => <List.Icon {...props} icon="account-check" />}
          style={{ flex: 1 }}
        />
        <Divider />
        <List.Item
          title="Ciudad - Provincia"
          description={ciudad + " - " + provincia}
          left={(props) => <List.Icon {...props} icon="city" />}
        />

        <Divider />
        <List.Item
          title="Dirección"
          description={direccion_tienda}
          left={(props) => <List.Icon {...props} icon="home-group" />}
        />
        <Divider />
        <TouchableOpacity>
          <List.Item
            title="Ubicación"
            left={(props) => <List.Icon {...props} icon="google-maps" />}
            onPress={() =>
              goUbicacion(ubicacion_latitud_tienda, ubicacion_longitud_tienda)
            }
          />
        </TouchableOpacity>

        <Divider />
      </List.Section>
    </>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#fff",
  },
  viewRestaurantTitle: {
    padding: 15,
  },
  nameRestaurant: {
    fontSize: 20,
    fontWeight: "bold",
  },
  descriptionRestaurant: {
    marginTop: 5,
    color: "grey",
  },
  rating: {
    position: "absolute",
    right: 0,
  },
  viewRestaurantInfo: {
    margin: 15,
    marginTop: 20,
  },
  restaurantInfoTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  containerListItem: {
    borderBottomColor: "#d8d8d8",
    borderBottomWidth: 1,
  },
  viewFavorite: {
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 2,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 100,
    padding: 5,
    paddingLeft: 15,
  },
});
