import React from "react";
import { View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import { StoreStyles } from "../../../styles/estilos";
import ProductByStore from "../../../components/Products/ProductByStore";

export default function StoreProduct(props) {
  const { navigation, dato } = props;

  return (
    <ScrollView>
      <View style={StoreStyles.view_product}>
        <ProductByStore navigation={navigation} dato={dato} />
      </View>
    </ScrollView>
  );
}
