import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-elements";
import { View, Text } from "react-native";
import colores from "../../../styles/colores";
import { ScrollView } from "react-native";
import ListComment from "../../../components/Store/ListaComment";
import { getUserApi } from "../../../api/storage";
import { ToastAndroid } from "react-native";

export default function StoreComment(props) {
  const { navigation, dato } = props;
  const { id_tienda, nombre_tienda, logo_tienda } = dato;
  const [userLogged, setUserLogged] = useState(false);

  const [modalDescription, setModalDescription] = useState(false);

  useEffect(() => {
    cargaDataInicial();
    const unsubscribe = navigation.addListener("focus", () => {
      (async () => {
        cargaDataInicial();
      })();
    });
    return unsubscribe;
  }, []);

  const cargaDataInicial = async () => {
    let response = await getUserApi();
    setUserLogged(JSON.parse(response));
  };

  return (
    <View>
      {userLogged ? (
        <AddComment
          navigation={navigation}
          id_tienda={id_tienda}
          nombre_tienda={nombre_tienda}
          logo_tienda={logo_tienda}
        />
      ) : (
        <AddCommentLogin navigation={navigation} />
      )}
    </View>
  );
}

function AddComment(props) {
  const { navigation, id_tienda, nombre_tienda, logo_tienda } = props;

  return (
    <ScrollView>
      <View style={{ width: "100%", flexDirection: "row" }}>
        <View style={{ width: "50%" }}>
          <Button
            title="Escribe una opinión"
            buttonStyle={styles.btnAddReview}
            titleStyle={styles.btnTitleAddReview}
            icon={{
              type: "material-community",
              name: "square-edit-outline",
              color: colores.secundario,
            }}
            onPress={() =>
              navigation.navigate("add-comment", {
                id_tienda,
                nombre_tienda,
                logo_tienda,
              })
            }
          />
        </View>
        <View style={{ width: "50%" }}>
          <Button
            title="Denunciar Tienda"
            buttonStyle={styles.btnAddReview}
            titleStyle={styles.btnTitleDenunciarReview}
            icon={{
              type: "material-community",
              name: "close-box-multiple",
              color: colores.color_close,
            }}
            onPress={() =>
              navigation.navigate("denunciar-store", {
                id_tienda,
              })
            }
          />
        </View>
      </View>

      <ListComment
        id_tienda={id_tienda}
        logo_tienda={logo_tienda}
        navigation={navigation}
      />
    </ScrollView>
  );
}

function AddCommentLogin(props) {
  const { navigation } = props;
  const iniciarSesionComment = () => {
    ToastAndroid.show("Debe Loguearse para Comprar", 1000);
    navigation.navigate("cuenta");
  };
  return (
    <View>
      <Text
        style={{ textAlign: "center", color: colores.secundario, padding: 20 }}
        onPress={iniciarSesionComment}
      >
        Para escribir un comentario es necesario estar logueado{" "}
        <Text style={{ fontWeight: "bold", color: colores.primario }}>
          pulsa AQUÍ para iniciar sesión
        </Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },

  btnAddReview: {
    backgroundColor: "transparent",
  },
  btnTitleAddReview: {
    color: colores.secundario,
  },
  btnTitleDenunciarReview: {
    color: colores.color_close,
  },
  viewReview: {
    flexDirection: "row",
    padding: 10,
    paddingBottom: 20,
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1,
  },
  viewImageAvatar: {
    marginRight: 15,
  },
  imageAvatarUser: {
    width: 50,
    height: 50,
  },
  viewInfo: {
    flex: 1,
    alignItems: "flex-start",
  },
  reviewTitle: {
    fontWeight: "bold",
  },
  reviewText: {
    paddingTop: 2,
    color: "grey",
    marginBottom: 5,
  },
  reviewDate: {
    marginTop: 5,
    color: "grey",
    fontSize: 12,
    position: "absolute",
    right: 0,
    bottom: 0,
  },
});
