import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";

import colores from "../styles/colores";
import ChangeUser from "../screens/Account/ChangeUser";
import Auth from "../screens/Account/Auth";
import { getUserApi } from "../api/storage";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Account from "../screens/Account/Account";
import UserInvitado from "../screens/Account/UserInvitado";
import UserLogueado from "../screens/Account/UserLogueado";
import MiPerfil from "../screens/Account/MiPerfil";
import ChangePassword from "../screens/Account/ChangePassword";
import SeguridadOpciones from "../screens/Account/Seguridad/SeguridadOpciones";
import VerInformacionPolitica from "../screens/Account/Seguridad/VerInformacionPolitica";
import RegisterVendedor from "../components/Auth/RegisterVendedor";
import RegisterComprador from "../components/Auth/RegisterComprador";
import ActualizarUserComprador from "../screens/Account/Actualizar/ActualizarUserComprador";

import MisTiendas from "../screens/Account/Perfil/AdminStore/MisTiendas";
import ListadoTiendas from "../screens/Account/Perfil/AdminStore/ListadoTiendas";
import NewTienda from "../screens/Account/Perfil/AdminStore/NewTienda";

import MisProductos from "../screens/Account/Perfil/AdminStore/MisProductos";
import ListadoProductos from "../screens/Account/Perfil/AdminStore/ListadoProductos";
import NewProducto from "../screens/Account/Perfil/AdminStore/NewProducto";


const Stack = createStackNavigator();

export default function AccountStack() {
  const [auth, setAuth] = useState(true);
  const [stateMenu, setStateMenu] = useState(false);

  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colores.fontLight,
        headerStyle: { backgroundColor: colores.secundario },
        cardStyle: {
          backgroundColor: "#fff",
        },
      }}
    >
      <Stack.Screen
        name="account"
        component={Account}
        options={{ title: "Cuenta", headerShown: false }}
      />
      <Stack.Screen
        name="register-vendedor"
        component={RegisterVendedor}
        options={{ title: "Cuenta", headerShown: false }}
      />
      <Stack.Screen
        name="register-comprador"
        component={RegisterComprador}
        options={{ title: "Cuenta", headerShown: false }}
      />
      <Stack.Screen
        name="update-datos-comprador"
        component={ActualizarUserComprador}
        options={{
          title: "Actualizar Datos",
        }}
      />
      <Stack.Screen
        name="perfil"
        component={MiPerfil}
        options={{
          title: "Mis Datos",
        }}
      />
      <Stack.Screen
        name="user-edit"
        component={ChangeUser}
        options={{
          title: "Editar mis datos",
        }}
      />
      <Stack.Screen
        name="seguridad-options"
        component={SeguridadOpciones}
        options={{
          title: "Seguridad",
        }}
      />
      <Stack.Screen
        name="user-edit-password"
        component={ChangePassword}
        options={{
          title: "Cambiar contraseña",
        }}
      />
      <Stack.Screen
        name="info-administracion"
        component={VerInformacionPolitica}
        options={{
          title: "Documentos",
        }}
      />
      <Stack.Screen
        name="mis-productos"
        component={MisProductos}
        options={{
          title: "Mis Productos",
        }}
      />

      <Stack.Screen
        name="new-producto"
        component={NewProducto}
        options={{
          title: "Nuevo Producto",
        }}
      />
      <Stack.Screen
        name="listado-productos"
        component={ListadoProductos}
        options={{
          title: "Listado de Producto",
        }}
      />

      <Stack.Screen
        name="mis-tiendas"
        component={MisTiendas}
        options={{
          title: "Administración",
        }}
      />

      <Stack.Screen
        name="new-tienda"
        component={NewTienda}
        options={{
          title: "Nueva Tienda",
        }}
      />
      <Stack.Screen
        name="listado-tiendas"
        component={ListadoTiendas}
        options={{
          title: "Listado de Tiendas",
        }}
      />
    </Stack.Navigator>
  );
}
