import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { NavegacionStyles } from "../styles/estilos";
import AccountStack from "./AccountStack";
import ProductStack from "./ProductStack";
import AllCategories from "../screens/Categories/AllCategories";
import AllStore from "../screens/Store/AllStore";
import Favorites from "../screens/Favorites/Favorites";
import FavoriteStack from "./FavoriteStack";

const Tab = createMaterialBottomTabNavigator();

export default function AppNavigations() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        barStyle={NavegacionStyles.navigation}
        screenOptions={({ route }) => ({
          tabBarIcon: (routeStatus) => {
            return setIcon(route, routeStatus);
          },
        })}
      >
        <Tab.Screen
          name="home"
          component={ProductStack}
          options={{
            title: "Home",
          }}
        />
        <Tab.Screen
          name="cuenta"
          component={AccountStack}
          options={{
            title: "Cuenta",
          }}
        />
        <Tab.Screen
          name="favoritos"
          component={FavoriteStack}
          options={{
            title: "Favoritos",
          }}
        />

        <Tab.Screen
          name="store"
          component={AllStore}
          options={{
            title: "Tiendas",
          }}
        />
        <Tab.Screen
          name="category"
          component={AllCategories}
          options={{
            title: "Categorias",
          }}
          initialParams={{ id_categoria: 1 }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function setIcon(route, routeStatus) {
  let iconName = "";
  switch (route.name) {
    case "home":
      iconName = "home";
      break;
    case "favoritos":
      iconName = "heart";
      break;
    case "category":
      iconName = "reorder-horizontal";
      break;
    case "cuenta":
      iconName = "account";
      break;
    case "store":
      iconName = "store";
      break;
    default:
      break;
  }
  return (
    <MaterialCommunityIcons name={iconName} style={[NavegacionStyles.icon]} />
  );
}
