import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import colores from "../styles/colores";

import AllCategories from "../screens/Categories/AllCategories";

const Stack = createStackNavigator();

export default function CategoriaStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colores.fontLight,
        headerStyle: { backgroundColor: colores.bgDark },
        cardStyle: {
          backgroundColor: "#fff",
        },
      }}
    >
      <Stack.Screen
        name="all-category"
        component={AllCategories}
        options={{ title: "Tiendas Tiendas", headerShown: false }}
      />
    </Stack.Navigator>
  );
}
