import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import colores from "../styles/colores";
import Home from "../screens/Home/Home";
import Categories from "../screens/Categories/Categories";
import AllCategories from "../screens/Categories/AllCategories";
import Product from "../screens/Home/Product";
import Store from "../screens/Store/Store";
import InfoStore from "../components/Store/InfoStore";
import AllStore from "../screens/Store/AllStore";
import StoreOptions from "../screens/Store/StoreOptions";
import Comment from "../components/Store/Comment";

const Stack = createStackNavigator();

export default function StoreStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colores.fontLight,
        headerStyle: { backgroundColor: colores.bgDark },
        cardStyle: {
          backgroundColor: "#fff",
        },
      }}
    >
      <Stack.Screen
        name="all-store"
        component={AllStore}
        options={{ title: "Tiendas Tiendas", headerShown: false }}
      />
      <Stack.Screen
        name="option-store"
        component={StoreOptions}
        options={{ title: "Options", headerShown: false }}
      />
      <Stack.Screen
        name="add-comment"
        component={Comment}
        options={{ title: "Options", headerShown: false }}
      />
      
    </Stack.Navigator>
  );
}
