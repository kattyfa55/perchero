import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import colores from "../styles/colores";
import Home from "../screens/Home/Home";
import Categories from "../screens/Categories/Categories";
import AllCategories from "../screens/Categories/AllCategories";
import Product from "../screens/Home/Product";
import Store from "../screens/Store/Store";
import InfoStore from "../components/Store/InfoStore";
import StoreOptions from "../screens/Store/StoreOptions";
import Comment from "../components/Store/Comment";
import ListFavorite from "../screens/Favorites/ListFavorite";
import Search from "../components/Search";
import SearchStore from "../screens/Products/SearchStore";
import DenunciarTienda from "../screens/Store/DenunciarTienda";
import ProductRecientes from "../screens/Products/ProductRecientes";
import AllProducts from "../screens/Products/AllProductsOfertas";
import VerMapa from "../components/Store/VerMapa";
import AllProductsOfertas from "../screens/Products/AllProductsOfertas";
import AllBuscadorProducto from "../screens/Products/AllBuscadorProducto";

const Stack = createStackNavigator();

export default function ProductStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colores.fontLight,
        headerStyle: { backgroundColor: colores.secundario },
        cardStyle: {
          backgroundColor: "#fff",
        },
      }}
    >
      <Stack.Screen
        name="home"
        component={Home}
        options={{
          title: "Home",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="product"
        component={Product}
        options={{
          title: "Productos",
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="categorie"
        component={Categories}
        options={{ title: "Categoria", headerShown: false }}
      />

      <Stack.Screen
        name="all-categorie"
        component={AllCategories}
        options={{ title: "Categoria", headerShown: false }}
      />
      <Stack.Screen
        name="view-store"
        component={Store}
        options={{ title: "Store", headerShown: false }}
      />
      <Stack.Screen
        name="info-store"
        component={InfoStore}
        options={{ title: "Store", headerShown: false }}
      />
      <Stack.Screen
        name="option"
        component={StoreOptions}
        options={{ title: "Options", headerShown: false }}
      />
      <Stack.Screen
        name="search-store"
        component={SearchStore}
        options={{ title: "Options", headerShown: false }}
      />
      <Stack.Screen
        name="add-comment"
        component={Comment}
        options={{
          title: "Comentarios",
          headerStyle: { backgroundColor: colores.color_blanco },
          headerTintColor: colores.primario,
        }}
      />
      <Stack.Screen
        name="denunciar-store"
        component={DenunciarTienda}
        options={{
          title: "Denunciar",
          headerStyle: { backgroundColor: colores.color_blanco },
          headerTintColor: colores.primario,
        }}
      />

      <Stack.Screen
        name="product-reciente"
        component={ProductRecientes}
        options={{ title: "Options", headerShown: false }}
      />

      <Stack.Screen
        name="product-by-store"
        component={ProductRecientes}
        options={{ title: "Options", headerShown: false }}
      />
      <Stack.Screen
        name="all-product"
        component={AllProducts}
        options={{ title: "Options", headerShown: false }}
      />

      <Stack.Screen
        name="all-product-oferta"
        component={AllProductsOfertas}
        options={{ title: "Options", headerShown: false }}
      />

      <Stack.Screen
        name="all-product-buscar"
        component={AllBuscadorProducto}
        options={{ title: "Options", headerShown: false }}
      />

      <Stack.Screen
        name="ver-mapa"
        component={VerMapa}
        options={{ title: "Regresar" }}
      />
    </Stack.Navigator>
  );
}
