import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import DetalleProduct from "../screens/Favorites/DetalleProduct";
import Favorites from "../screens/Favorites/Favorites";
import colores from "../styles/colores";

const Stack = createStackNavigator();
export default function FavoriteStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colores.fontLight,
        headerStyle: { backgroundColor: colores.secundario },
        cardStyle: {
          backgroundColor: "#fff",
        },
      }}
    >
      <Stack.Screen
        name="favorito"
        component={Favorites}
        options={{ title: "Cuenta", headerShown: false }}
      />
      <Stack.Screen
        name="detail-producto"
        component={DetalleProduct}
        options={{ title: "Cuenta", headerShown: false }}
      />
    </Stack.Navigator>
  );
}
