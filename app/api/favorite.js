import { USER_TOKEN } from "../utils/constants";
import { URL_API } from "./rutas";
import { getUserApi } from "./storage";

export async function getFavoriteByUser() {
  try {
    const url = `${URL_API}/get/favorite/user`;
    let user = JSON.parse(await getUserApi());
    let id_user = user.id;
    let data = {
      user_id: id_user,
      TOKEN: USER_TOKEN,
    };
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function AddFavoriteUser(formData) {
  try {
    const url = `${URL_API}/add/favorite/user`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function isFavoriteUser(formData) {
  try {
    const url = `${URL_API}/is/favorite/user`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}
