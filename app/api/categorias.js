import { PARAMETROS } from "../utils/constants";
import { URL_API } from "./rutas";

export async function getSubCategories(categoria) {
  try {
    const url = `${URL_API}/get/subcategorias`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        categoria_id: parseInt(categoria),
      }),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return null;
  }
}

export async function getTipoProducto(categoria) {
  try {
    const url = `${URL_API}/get/tipo/producto`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        sub_categoria_id: parseInt(categoria),
      }),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return null;
  }
}

export async function getAllCategorias() {
  try {
    const url = `${URL_API}/get/all/categoria`;

    const response = await fetch(url, PARAMETROS);
    const result = await response.json();
    return result;
  } catch (error) {
    return null;
  }
}
