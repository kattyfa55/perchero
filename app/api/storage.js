import AsyncStorage from "@react-native-async-storage/async-storage";

import { USER } from "../utils/constants";

export async function getUserApi() {
  try {
    const user = await AsyncStorage.getItem(USER);
    return user;
  } catch (e) {
    return null;
  }
}

export async function getUsuarioApi() {
  try {
    const user = await AsyncStorage.getItem(USER);
    return user;
  } catch (e) {
    return null;
  }
}

export async function setUserApi(user) {
  try {
    await AsyncStorage.setItem(USER, JSON.stringify (user));
    return true;
  } catch (e) {
    return null;
  }
}

export async function removeUserApi() {
  try {
    await AsyncStorage.removeItem(USER);
    return true;
  } catch (e) {
    return null;
  }
}
