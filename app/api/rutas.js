import { SERVIDOR_LOCAL, SERVIDOR_PRODUCCION } from "../utils/constants";
export const SERVIDOR = SERVIDOR_PRODUCCION;
export const URL_API = SERVIDOR + "api";
export const DOC_POLITICAS =
  "http://elperchero.store/archivos_portal/TERMINOS%20Y%20CONDICIONES%20CORCE%20S.A.pdf";

export const DOC_CONSEJOS =
  "http://elperchero.store/archivos_portal/NJkV6hgAgps8Mw0EbNJqgueD7RKXpDpYL4lLqs03.pdf";
export const DOC_TERMINOS_CONDICIONES =
  "http://elperchero.store/archivos_portal/lY956dAgob1IMWmn0HknCiud4O5l4i6KNiHQr7hz.pdf";
export const DOC_NORMATIVAS =
  "http://elperchero.store/archivos_portal/1driHuZOSj3m1vSh6owFYacDbNKJd9TJV6fVRPkj.pdf";

export const SHARE_PRODUCT = "http://elperchero.store/productos/";
