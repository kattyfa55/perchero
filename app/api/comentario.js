import { URL_API } from "./rutas";

export async function addComment(formData) {
  try {
    const url = `${URL_API}/new/comment`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return null;
  }
}

export async function getCommentStore(dato) {
  try {
    const url = `${URL_API}/get/comment/store`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dato),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result.data;
  } catch (error) {
    return null;
  }
}
