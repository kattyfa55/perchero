import { URL_API } from "./rutas";
import { PARAMETROS } from "../utils/constants";

export async function getListaProvincias(data) {
  try {
    const url = `${URL_API}/get/provincia`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_pais: data,
      }),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getListaCiudades(data) {
  try {
    const url = `${URL_API}/get/ciudad`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_provincias: data,
      }),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getListaPais(data) {
  try {
    const url = `${URL_API}/get/pais`;
    const response = await fetch(url, PARAMETROS);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}


export async function getListaCityFiltro() {
  try {
    const url = `${URL_API}/get/cmb/ciudad`;
    const response = await fetch(url, PARAMETROS);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}