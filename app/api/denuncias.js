import { URL_API } from "./rutas";

export async function addDenuncia(formData) {
  try {
    const url = `${URL_API}/save/denuncia`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    return null;
  }
}
