import { URL_API } from "./rutas";
import { PARAMETROS } from "../utils/constants";

export async function getBannersApi() {
  try {
    const url = `${URL_API}/get/banners`;
    const response = await fetch(url, PARAMETROS);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}
