import { PARAMETROS_POST } from "../utils/constants";
import { validarDatos } from "../utils/validaciones";
import { URL_API } from "./rutas";
export async function getTienda(data) {
  try {
    const url = `${URL_API}/get/tienda`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: data,
      }),
    };

    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getAllTienda() {
  try {
    const url = `${URL_API}/get/all/tienda`;
    const response = await fetch(url, PARAMETROS_POST);
    const result = await response.json();
    const resultado = await validarDatos(result);
    return resultado;
  } catch (error) {
    
    return null;
  }
}

export async function getAllFitroTienda(data) {
  try {
    const url = `${URL_API}/get/filtros/tienda`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}
