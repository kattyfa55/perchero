import { PARAMETROS, PARAMETROS_POST } from "../utils/constants";
import { validarDatos } from "../utils/validaciones";
import { URL_API } from "./rutas";

export async function getNewProductos() {
  try {
    const url = `${URL_API}/get/productos`;
    const response = await fetch(url, PARAMETROS);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getProducto(id) {
  try {
    const url = `${URL_API}/products/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getProductsByStore(id) {
  try {
    const url = `${URL_API}/get/productos/store`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: parseInt(id),
      }),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getProductsRecent() {
  try {
    const url = `${URL_API}/get/productos/recent`;
    const response = await fetch(url, PARAMETROS_POST);
    const result = await response.json();
    const resultado = await validarDatos(result);
    return resultado;
  } catch (error) {
    return null;
  }
}

export async function getAllFitroProductosOfertas(data) {
  try {
    const url = `${URL_API}/get/filtros/productos/ofertas`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}

export async function getAllFitroProductos(data) {
  try {
    const url = `${URL_API}/get/filtros/productos`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    
    return null;
  }
}
