const colores = {
  color_pri: "#2291FF",
  color_sec: "#0E3746",
  color_ter: "#DADDE1",
  color_cua: "#FFFFFF",
  color_qui: "#34495E",
  color_sex: "#F4F9FF",

  color_favorite: "#D9435F",
  color_close: "#D01834",

  color_icono: "#1F5A9D",

  primary: "#0098d3",
  dark: "#000",
  color_blanco: "#FFFFFF",
  // Fonts
  fontLight: "#fff",
  // Background,
  bgLight: "#fff",
  bgDark: "#16222b",

  primario: "#58167D",
  secundario: "#5FA199",
  terciario: "#727176",
  color_negro: "#000000",

  color_view_msj: "#D4EDDA",
  color_bord_msj: "#C3E6CB",
  color_let_msj: "#155724",

  secundarioLight: "#FBFFFF",
};

export default colores;
