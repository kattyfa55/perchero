import { StyleSheet } from "react-native";
import colores from "./colores";

export const NavegacionStyles = StyleSheet.create({
  navigation: {
    backgroundColor: colores.color_blanco,
  },
  icon: {
    fontSize: 20,
    color: colores.primario,
  },
});
export const StoreStyles = StyleSheet.create({
  view_product: {
    width: "100%",
    backgroundColor: colores.primary,
    flexDirection: "row",
    alignSelf: "center",
    margin: 20,
    flex: 1,
  },
});

export const SearchStyles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: colores.secundario,
    paddingVertical: 5,
    paddingHorizontal: 5,
    zIndex: 1,
  },
  containerInput: {
    position: "relative",
    alignItems: "flex-end",
  },
  backArrow: {
    position: "absolute",
    left: 0,
    top: 15,
    color: colores.terciario,
  },
});

export const userStyles = StyleSheet.create({
  container: {
    justifyContent: "center",
    margin: 15,
  },
  title: {
    fontSize: 20,
    color: colores.secundario,
  },
  titleName: {
    fontSize: 25,
    fontWeight: "bold",
    color: colores.terciario,
    alignSelf: "center",
  },
});

export const ChangeUserStyles = StyleSheet.create({
  View_cmb: {
    borderWidth: 1,
    borderColor: colores.terciario,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 15,
  },

  Ordenar_cmb: {
    borderWidth: 1,
    borderColor: colores.terciario,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  PickerOrdenarStyles: { marginLeft: 5, marginVertical: -4 },
  PickerUserStyles: { marginLeft: 5, marginVertical: 2 },
});

export const stylesProductRecent = StyleSheet.create({
  view_products: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "center",
  },
  view_productos_head: {
    backgroundColor: colores.secundario,
    borderBottomWidth: 4,
    borderColor: colores.primario,
  },
  txt_nombre_tienda: {
    color: colores.color_blanco,
    alignSelf: "center",
    fontSize: 11,
    marginBottom: 2,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "center",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },
  store_border: {
    backgroundColor: colores.color_blanco,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: colores.color_ter,
  },
  image: {
    height: 180,
    resizeMode: "contain",
  },
  name: {
    marginTop: 4,
    fontSize: 13,
    color: colores.color_blanco,
    alignSelf: "center",
    fontWeight: "bold",
  },
  view_footer: {
    backgroundColor: colores.terciario,
    width: "100%",
    flexDirection: "row",
  },
  txt_precio: {
    color: colores.color_blanco,
    fontSize: 16,
    paddingVertical: 5,
    marginLeft: 10,
  },
  txt_oferta: {
    color: colores.color_blanco,
    fontSize: 16,
    paddingVertical: 5,
    marginLeft: 6,
  },
  txt_precio_oferta: {
    color: colores.color_blanco,
    fontSize: 12,
    textAlignVertical: "center",
    marginLeft: 10,
    textDecorationLine: "line-through",
  },

  btn_cart: {
    backgroundColor: colores.secundario,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginRight: 10,
    marginTop: 3,
  },
});

export const estilos_productos = StyleSheet.create({
  view_products: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "center",
  },
  view_productos_head: {
    backgroundColor: colores.secundario,
    borderBottomWidth: 4,
    borderColor: colores.primario,
  },
  txt_nombre_tienda: {
    color: colores.color_blanco,
    alignSelf: "center",
    fontSize: 11,
    marginBottom: 2,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "center",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },
  image: {
    height: 180,
    resizeMode: "contain",
  },
  name: {
    marginTop: 4,
    fontSize: 13,
    color: colores.color_blanco,
    alignSelf: "center",
    fontWeight: "bold",
  },
  view_footer: {
    backgroundColor: colores.terciario,
    width: "100%",
    flexDirection: "row",
  },
  txt_precio: {
    color: colores.color_blanco,
    fontSize: 16,
    paddingVertical: 5,
    marginLeft: 10,
  },
  txt_oferta: {
    color: colores.color_blanco,
    fontSize: 16,
    paddingVertical: 5,
    marginLeft: 6,
  },
  txt_precio_oferta: {
    color: colores.color_blanco,
    fontSize: 12,
    textAlignVertical: "center",
    marginLeft: 10,
    textDecorationLine: "line-through",
  },
  btn_cart: {
    backgroundColor: colores.secundario,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginRight: 10,
    marginTop: 3,
  },
  container: {
    padding: 10,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 22,
    marginBottom: 10,
    color: colores.color_sec,
  },
  btnBuyContent: {
    backgroundColor: "#008fe9",
    paddingVertical: 5,
  },
  btnBuyLabel: {
    fontSize: 18,
  },
  btnBuy: {
    marginTop: 20,
  },
  flashRow: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    padding: 6,
    alignItems: "center",
    marginTop: 5,
  },
  rowTag: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 2,
  },
  priceRowInnerContainer: {
    flexDirection: "row",
    paddingHorizontal: 5,
    alignItems: "center",
    paddingVertical: 6,
  },
  productDescriptionText: {
    paddingHorizontal: 4,
    paddingVertical: 5,
    textAlign: "left",
    color: "#000",
    fontSize: 12,
  },
  precioDescriptionText: {
    paddingHorizontal: 4,
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "left",
  },
});
export const formStyles = StyleSheet.create({
  buscar_view: {
    borderTopColor: colores.terciario,
    borderTopWidth: 0.3,
    marginTop: 20,
  },

  view_border: {
    borderTopColor: colores.terciario,
    borderTopWidth: 0.3,
    marginTop: 20,
  },
  txt_registrarse: {
    color: colores.primario,
    fontSize: 15,
    marginTop: 15,
    fontWeight: "bold",
    alignSelf: "center",
    justifyContent: "center",
  },
  view_tipo_register: {
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  search_view: {
    marginBottom: 0,
    backgroundColor: colores.color_cua,
    width: "80%",
  },
  input: {
    marginBottom: 15,
    backgroundColor: colores.color_cua,
  },
  input_form: {
    marginBottom: 12,
    backgroundColor: colores.color_blanco,
  },
  input_motivo: {
    marginBottom: 15,
    backgroundColor: colores.color_blanco,
  },
  btnSucces: {
    padding: 5,
    backgroundColor: colores.secundario,
    color: colores.color_blanco,
    borderColor: colores.secundario,
    borderWidth: 1,
  },
  btnCancelar: {
    backgroundColor: colores.color_blanco,
    marginTop: 10,
    marginHorizontal: 5,
    borderColor: colores.secundario,
    borderWidth: 1,
  },
  btnStyleView: {
    marginVertical: 8,
    paddingVertical: 5,
    color: colores.color_blanco,
    borderWidth: 1,
    backgroundColor: colores.color_blanco,
    borderColor: colores.secundario,
  },
  btnStyleText: {
    color: colores.secundario,
    textTransform: "capitalize",
  },
  btnLogin: {
    backgroundColor: colores.secundario,
    marginTop: 10,
    marginHorizontal: 5,

    borderColor: colores.secundario,
    borderWidth: 1,
  },
  btnTextLabelCancelar: {
    color: colores.secundario,
    textTransform: "capitalize",
  },
  btnText: {
    marginTop: 0,
  },
  btnTextLabel: {
    color: colores.primario,
    textTransform: "capitalize",
    fontSize: 14,
    marginHorizontal: 10,
    marginTop: 5,
    fontWeight: "bold",
  },
  txt_separation_register: {
    color: colores.primario,
    fontSize: 15,
    marginTop: 5,
  },
  btnTextLabelLogin: {
    color: colores.color_blanco,
    textTransform: "capitalize",
  },
  container: {
    padding: "5%",
  },
  aviso_text: {
    paddingVertical: 10,
  },
  text_titulo: {
    paddingTop: "20%",
    paddingBottom: "20%",
    fontWeight: "bold",
    fontSize: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  view_titulo: {
    width: "78%",
    alignSelf: "center",
    marginTop: 20,
  },
  view_motivo: {
    width: "88%",
    alignSelf: "center",
    marginTop: 0,
  },
  view_mensaje_confirmacion: {
    alignItems: "center",
    marginTop: 45,
    backgroundColor: colores.color_view_msj,
    paddingVertical: 8,
    width: "85%",
    alignSelf: "center",
    borderRadius: 5,
    borderColor: colores.color_bord_msj,
    borderWidth: 1,
  },
  txt_mensaje_confirmacion: { color: "#155724" },
  txt_mensaje_negrita: { fontWeight: "bold" },
});

export const informacionStyles = StyleSheet.create({
  infoText: {
    width: "50%",
    textAlign: "center",
    color: colores.color_pri,
    fontWeight: "bold",
    paddingVertical: 4,
  },
  viewInfo: {
    flexDirection: "row",
    borderTopWidth: 1,
    borderColor: colores.color_ter,
    width: "95%",
    alignSelf: "center",
    paddingTop: 15,
  },
});

export const registerStyles = StyleSheet.create({
  image_princ: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
  },
});

export const logoStyle = StyleSheet.create({
  logo: {
    width: "100%",
    height: 200,
    resizeMode: "contain",
  },
});

export const productStyle = StyleSheet.create({
  containerProducto: {
    padding: 10,
    marginTop: 10,
  },
  titleProducto: {
    fontWeight: "bold",
    fontSize: 15,
    marginBottom: 5,
    color: colores.secundario,
  },
});

export const stylesCommentUser = StyleSheet.create({
  centeredView: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },

  btnAddReview: {
    backgroundColor: "transparent",
  },
  btnTitleAddReview: {
    color: colores.secundario,
  },
  btnTitleDenunciarReview: {
    color: colores.color_close,
  },
  viewReview: {
    flexDirection: "row",
    padding: 10,
    paddingBottom: 20,
    borderBottomColor: "#e3e3e3",
    borderBottomWidth: 1,
  },
  viewImageAvatar: {
    marginRight: 15,
  },
  imageAvatarUser: {
    width: 50,
    height: 50,
  },
  viewInfo: {
    flex: 1,
    alignItems: "flex-start",
  },
  reviewTitle: {
    fontWeight: "bold",
  },
  reviewText: {
    paddingTop: 2,
    color: "grey",
    marginBottom: 5,
  },
  reviewDate: {
    marginTop: 5,
    color: "grey",
    fontSize: 12,
    position: "absolute",
    right: 0,
    bottom: 0,
  },
});

export const stylesLocales = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "center",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,

    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },
  image: {
    height: 180,
    resizeMode: "cover",
  },
  name: {
    marginVertical: 3,
    fontSize: 16,
    color: colores.color_blanco,
    alignSelf: "center",
  },
});

export const categorias_styles = StyleSheet.create({
  view_categorias: {
    //borderWidth: 0.3,
    //borderColor: colores.color_sec,
    height: 30,
    //marginHorizontal: 2,
    justifyContent: "center",
    //backgroundColor: colores.color_blanco,
    //borderRadius: 5,
  },
  content_categorias: {
    flexDirection: "row",
    marginHorizontal: 5,
    alignSelf: "center",
    alignContent: "center",
    justifyContent: "center",
  },
  text_categoria: {
    color: colores.primario,
    textAlign: "center",
    fontSize: 12,
    paddingHorizontal: 5,
  },
});

export const mi_perfil_styles = StyleSheet.create({
  view_perfil: {
    width: 200,
    height: 200,
    marginTop: 10,
    alignSelf: "center",
    borderRadius: 200,
  },
  view_image: {
    width: 200,
    height: 200,
    resizeMode: "contain",
    borderRadius: 400,
  },
});

export const register_styles = StyleSheet.create({
  view_general_terminos: {
    width: "100%",
    flexDirection: "row",
  },
  view_checked: {
    width: "10%",
    alignSelf: "center",
    alignItems: "center",
  },
  view_terminos: {
    width: "90%",
    marginVertical: 20,
  },
  txt_terminos_condiciones: {
    fontSize: 9,
    marginLeft: 7,
    textAlign: "justify",
  },
  txt_touch_terminos: {
    color: colores.secundario,
    fontWeight: "bold",
    fontSize: 9,
    margin: 0,
    padding: 0,
  },
});

export const stylesSearch = StyleSheet.create({
  search_view_store: {
    width: "100%",
    flexDirection: "row",
    paddingTop: 3,
    justifyContent: "center",
    backgroundColor: colores.color_blanco,
  },
  search_view_busq: {
    width: "93%",
    flexDirection: "row",
    borderWidth: 1,
    borderColor: colores.terciario,
    borderRadius: 5,
    marginTop: 5,
    backgroundColor: colores.color_blanco,
    flexDirection: "row",
  },
  view_products: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "center",
  },
  view_productos_head: {
    backgroundColor: colores.secundario,
    borderBottomWidth: 4,
    borderColor: colores.primario,
  },
  txt_nombre_tienda: {
    color: colores.color_blanco,
    alignSelf: "center",
    fontSize: 11,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignContent: "center",
  },
  containerProduct: {
    width: "50%",
    padding: 10,
    alignSelf: "center",
    borderColor: colores.color_pri,
  },
  product: {
    backgroundColor: colores.color_blanco,
    borderLeftWidth: 0.3,
    borderRightWidth: 0.3,
    borderColor: colores.color_ter,
  },

  store: {
    backgroundColor: colores.color_blanco,

    borderColor: colores.color_ter,
  },
  image: {
    height: 180,
    resizeMode: "contain",
  },
  imageStore: {
    height: 180,
  },
  name: {
    marginTop: 4,
    fontSize: 13,
    color: colores.color_blanco,
    alignSelf: "center",
    fontWeight: "bold",
  },
  view_footer: {
    backgroundColor: colores.terciario,
    width: "100%",
    flexDirection: "row",
  },
  txt_precio: {
    color: colores.color_blanco,
    fontSize: 16,
    paddingVertical: 5,
    marginLeft: 10,
  },
  btn_cart: {
    backgroundColor: colores.secundario,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginRight: 10,
    marginTop: 3,
  },
});
