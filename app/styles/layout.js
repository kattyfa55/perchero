import { StyleSheet } from "react-native";
import colores from "./colores";

const layoutStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 0,
    backgroundColor: colores.color_blanco,
  },
});

export default layoutStyle;
