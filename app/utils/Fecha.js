import { format } from "date-fns";

const Fecha = {
  getActual_YMD: () => {
    var fecha = format(new Date(), "yyyy-MM-dd").toString();
    return fecha;
  },
  getActual_YMD_HM: () => {
    var fecha = format(new Date(), "yyyy-MM-dd HH:mm:ss").toString();
    return fecha;
  },
  getMaximaFecha: () => {
    var fecha_actual = new Date();
    var fecha_maxima = new Date();
    fecha_maxima.setMonth(fecha_actual.getMonth() - 216);
    return fecha_maxima;
  },
  getMinimaFecha: () => {
    var fecha_actual = new Date();
    return fecha_actual;
  },
};

export default Fecha;
