//Custom Marker
import React from "react";
import { Marker } from "react-native-maps";
/* import Animated from "react-native-reanimated";
 */ import { StyleSheet, View, Image, Text } from "react-native";
import colores from "../../styles/colores";
/* import { useMarkerAnimation } from "./useMarkerAnimation";
 */
export function CustomMarker({ latitude, longitude }) {
  /*   const scale = useMarkerAnimation({ id, selectedMarker });
   */
  return (
    <Marker
      coordinate={{
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
      }}
    >
      <View style={styles.markerWrapper}>
        <Image
          style={{ height: 30, width: 30, borderRadius: 30 }}
          source={require("./../../../assets/logo.png")}
        />
      </View>
    </Marker>
  );
}

const styles = StyleSheet.create({
  markerWrapper: {
    height: 50,
    width: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    height: 20,
    width: 20,
    borderRadius: 20,
    backgroundColor: colores.primario,
  },
});
