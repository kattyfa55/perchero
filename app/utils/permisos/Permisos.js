import * as Location from "expo-location";
import * as ImagePicker from 'expo-image-picker';

import { ToastAndroid } from "react-native";

const Permisos = {
  permisosMedia: async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== "granted") {
      ToastAndroid.show(
        "¡Necesita  permisos de cámara para que  funcione!",
        1000
      );
      return;
    }
  },

  localizacion: async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      ToastAndroid.show(
        "Los permisos de localización, no fueron aceptados",
        1000
      );
      return;
    }
  },

  gpsActivado: async () => {
    let gpsServiceStatus = await Location.getProviderStatusAsync();
    if (gpsServiceStatus.locationServicesEnabled) {
      return await true;
    } else {
      alert("No tiene Activado el GPS, activelo para continuar");
      return await false;
    }
  },
};
export default Permisos;
