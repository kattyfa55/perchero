export const iconInfoStore = [];

export const defecto = [
  {
    id_ciudades: 0,
    id_provincias: 0,
    nombre_ciudades: "Seleccione una ciudad",
  },
  {
    name: "Mujeres",
    icon: "shopping-bag",
    tipo: "font-awesome-5",
    categoria: "2",
  },
  { name: "Fashion", icon: "gifts", tipo: "font-awesome-5", categoria: "3" },
  {
    name: "Accesorios",
    icon: "hammer",
    tipo: "font-awesome-5",
    categoria: "4",
  },
];

export const datos = [
  { name: "Hombres", icon: "tshirt", tipo: "font-awesome-5", categoria: "1" },
  {
    name: "Mujeres",
    icon: "shopping-bag",
    tipo: "font-awesome-5",
    categoria: "2",
  },
  { name: "Fashion", icon: "gifts", tipo: "font-awesome-5", categoria: "3" },
  {
    name: "Accesorios",
    icon: "hammer",
    tipo: "font-awesome-5",
    categoria: "4",
  },
];

export const data_producto = [
  {
    id: 1,
    ruta: "http://192.168.40.15/img/productos/camisa1.png",
    id_producto: "1",
  },
  {
    id: 2,
    ruta: "http://192.168.40.15/img/productos/camisa2.png",
    id_producto: "1",
  },
  {
    id: 3,
    ruta: "http://192.168.40.15/img/productos/camisa3.png",
    id_producto: "1",
  },
  {
    id: 4,
    ruta: "http://192.168.40.15/img/productos/camisa4.png",
    id_producto: "1",
  },
];

export const productos = [
  {
    id: 1,
    ruta: "http://192.168.40.15/img/productos/camisa1.png",
    id_producto: 1,
  },
  {
    id: 2,
    ruta: "http://192.168.40.15/img/productos/camisa2.png",
    id_producto: 1,
  },
  {
    id: 3,
    ruta: "http://192.168.40.15/img/productos/camisa3.png",
    id_producto: 1,
  },
  {
    id: 4,
    ruta: "http://192.168.40.15/img/productos/camisa4.png",
    id_producto: 1,
  },
  {
    id: 1,
    ruta: "http://192.168.40.15/img/productos/polo1.png",
    id_producto: 2,
  },
  {
    id: 2,
    ruta: "http://192.168.40.15/img/productos/polo2.png",
    id_producto: 2,
  },
  {
    id: 3,
    ruta: "http://192.168.40.15/img/productos/polo3.png",
    id_producto: 2,
  },
  {
    id: 4,
    ruta: "http://192.168.40.15/img/productos/polo4.png",
    id_producto: 2,
  },
  {
    id: 1,
    ruta: "http://192.168.40.15/img/productos/chaquetas1.png",
    id_producto: 3,
  },
  {
    id: 2,
    ruta: "http://192.168.40.15/img/productos/chaquetas2.png",
    id_producto: 3,
  },
  {
    id: 3,
    ruta: "http://192.168.40.15/img/productos/chaquetas3.png",
    id_producto: 3,
  },
  {
    id: 4,
    ruta: "http://192.168.40.15/img/productos/chaquetas4.png",
    id_producto: 3,
  },
  {
    id: 1,
    ruta: "http://192.168.40.15/img/productos/camisa1.png",
    id_producto: 5,
  },
  {
    id: 2,
    ruta: "http://192.168.40.15/img/productos/camisa2.png",
    id_producto: 5,
  },
  {
    id: 3,
    ruta: "http://192.168.40.15/img/productos/camisa3.png",
    id_producto: 5,
  },
  {
    id: 4,
    ruta: "http://192.168.40.15/img/productos/camisa4.png",
    id_producto: 5,
  },
];

export var datita = (key) => {
  let imagenes = [];
  productos.forEach((element) => {
    if (element.id_producto == key) {
      imagenes.push(element);
    }
  });

  return imagenes;
};
