import { Dimensions } from "react-native";
//export const SERVIDOR_LOCAL = "http://192.168.40.15:8000/";
export const SERVIDOR_LOCAL = "http://192.168.18.128:8000/";

export const SERVIDOR_LOCAL_SIN = "http://192.168.40.15/";
export const SERVIDOR_PRODUCCION = "http://elperchero.store/";

//export const API_URL = "http://elperchero.store/api";
export const API_URL = SERVIDOR_LOCAL + "api";
// export const API_URL = "https://react-native-ecommerce-dev.herokuapp.com";
export const TOKEN = "token";
export const USER = "user";

export const USER_TOKEN =
  "$2y$10$XxgMM2t6qSQctLfo6TDQQuVKYIohEv.ugtz40XypPaLDcJyNfL3z.";

export const SEARCH_HISTORIAL = "search-history";
export const CART = "cart";
export const STRAPI_PUBLISHABLE_KEY =
  "pk_test_51IGQJXFotiOk2WnmTOM6HpcPnfjZSX98o9FqA5RJiMKB6jm62hiVu7mtk9rxgMjPSbjdIJxGmN6jwFNQ69IQ7f9O00AthydrqM";

export const SLIDERWIDTH = Dimensions.get("window").width;

export const PARAMETROS = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
  body: { data: 0 },
};

export const PARAMETROS_POST = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
};
