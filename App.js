import React, { useEffect, useState, useMemo } from "react";
import AuthContext from "./app/context/AuthContext";
import AppNavigations from "./app/navigations/AppNavigations";
import { Provider as PaperProvider } from "react-native-paper";
import { getTokenApi, setTokenApi, removeTokenApi } from "./app/api/token";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { setUserApi, getUserApi, removeUserApi } from "./app/api/storage";
import { LogBox } from "react-native";

LogBox.ignoreLogs(["VirtualizedLists should never be nested"]);

export default function App() {
  const [auth, setAuth] = useState(undefined);
  useEffect(() => {
    setAuth(null);
    /* removeUserApi(); */
  }, []);

  const login = (user) => {
    setTokenApi(user.data.telefono);
    setUserApi(user.data);

    /* setAuth({
        token: user.data.telefono,
        idUser: user.data.id,
      }); */
  };

  const logout = () => {
    if (auth) {
      setAuth(null);
    }
  };

  const authData = useMemo(
    () => ({
      auth,
      login,
      logout,
    }),
    [auth]
  );

  if (auth === undefined) return null;

  return (
    <AuthContext.Provider value={authData}>
      <PaperProvider>
        <AppNavigations />
      </PaperProvider>
    </AuthContext.Provider>
  );
}
